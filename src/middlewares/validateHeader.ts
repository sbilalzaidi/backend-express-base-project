import { Request, Response, NextFunction } from "express";
import CONSTANT from "../constant/constant";
/**
 *
 * @param {*} params
 * @description for delete variant
 * @returns
 */
const validateHeader = async (
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    try {
      if(!(req?.headers?.['content-language'])){
        req.headers['content-language']=CONSTANT.LANGUAGE_TYPE.ENGLISH;
      }
      if(req.headers?.['content-language'] && 
      Object.values(CONSTANT.LANGUAGE_TYPE).includes(req.headers?.['content-language'])){
        next();
      }else{
        return res.status(400).json({ error: `Invalid or missing content-language header(Allow ${Object.values(CONSTANT.LANGUAGE_TYPE)})`});
      }
    } catch (error) {
      next(error);
    }
  };
  export {
    validateHeader,
  }