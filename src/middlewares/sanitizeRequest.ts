import { NextFunction } from "express";
import { ADMIN_TYPE } from "../enums/enums";

const sanitizeRequest = async (req: any, res: any, next: NextFunction) => {
  try {
    const {addressType}=req.user;
    if([ADMIN_TYPE.COMPANY,ADMIN_TYPE.COMPANY_USER].includes(addressType)){
      if(req?.body?.adminId)
      delete req?.body?.adminId;
      if(req?.query?.adminId)
      delete req?.query?.adminId
      if(req?.params?.adminId)
      delete req?.params?.adminId
    }
    next();
  } catch (error) {
    next(error);
  }
};
export {sanitizeRequest}