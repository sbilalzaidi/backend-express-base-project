import {Request,Response,NextFunction} from "express";
import compression from 'compression';

function shouldCompress (req:Request, res:Response) {
    if (req.headers['x-no-compression']) {
      // don't compress responses with this request header
      return false
    }
   
    // fallback to standard filter function
    return compression.filter(req, res)
}
function compress(){
    return compression({
        filter: shouldCompress,
        level: 6, // Set the compression level (0-9)
        threshold: 512, // Minimum response size (in bytes) to compress
        // Other options as needed
      })
}
export {compress}