import { Request, Response, NextFunction } from "express";
 import rateLimit from "express-rate-limit";
import { RedisStore, RedisReply, SendCommandFn  } from "rate-limit-redis";
import connectRedisDB from "../connection/redis";
const redisInstance = connectRedisDB.getInstance();
let redisClient = redisInstance.getClient("rateLimit");
import codes from "../codes/status_codes";
const customSendCommand: any = async (...args: string[]): Promise<any> => {
  // @ts-expect-error - Known issue: the `call` function is not present in @types/ioredis
  return redisClient.call(...args)
};

const customRateLimiterIP = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let limit;
  if (req.path.startsWith("/api/v1/admins")) {
    limit = 20; // limit for route1
  } else {
    limit = 30; // default limit for other paths
  }
  const limiter = rateLimit({
    store: new RedisStore({
      sendCommand:customSendCommand
    }),
    windowMs: 1 * 60 * 1000, // 1 minutes
    max: limit, // dynamic limit based on the path
    standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
    legacyHeaders: false, // Disable the `X-RateLimit-*` headers
    keyGenerator: (req) => {
      const ip = req.ip; // Get the IP address of the client
      // Customize the key based on IP
      return `${ip}`;
    },
    handler: (req, res) => {
      return res
        .status(codes.TOO_MANY_REQUEST)
        .json({
          error: `Too many requests for ${req.path}, please try again later.`,
        });
    },
  });
  // Apply the dynamically created limiter
  limiter(req, res, next);
};
const customRateLimiterClientId = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let limit;
  if (req.path.startsWith("/api/v1/admins")) {
    limit = 20; // limit for route1
  } else {
    limit = 30; // default limit for other paths
  }
  const limiter = rateLimit({
    store: new RedisStore({
      sendCommand:customSendCommand
    }),
    windowMs: 1 * 60 * 1000, // 1 minutes
    max: limit, // dynamic limit based on the path
    standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
    legacyHeaders: false, // Disable the `X-RateLimit-*` headers
    keyGenerator: (req) => {
      const ip = req.ip; // Get the IP address of the client
      const clientId = (req.headers["client-id"] as string) || "default"; // Get client ID from headers

      // Customize the key based on IP and client ID
      return `${ip}:${clientId}`;
    },
    handler: (req, res) => {
      return res
        .status(codes.TOO_MANY_REQUEST)
        .json({
          error: `Too many requests for ${req.path}, please try again later.`,
        });
    },
  });
  // Apply the dynamically created limiter
  limiter(req, res, next);
};
const customRateLimiterClientIdWithUrl = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let limit;
  if (req.path.startsWith("/api/v1/admins")) {
    limit = 10; // limit for route1
  } else {
    limit = 5; // default limit for other paths
  }
  const limiter = rateLimit({
    store: new RedisStore({
      sendCommand:customSendCommand
    }),
    windowMs: 1 * 60 * 1000, // 1 minutes
    max: limit, // dynamic limit based on the path
    standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
    legacyHeaders: false, // Disable the `X-RateLimit-*` headers
    keyGenerator: (req) => {
      const ip = req.ip; // Get the IP address of the client
      const clientId = req.headers["client-id"] as string; // Get client ID from headers
      const url = req.originalUrl; // Get the URL

      // Customize the key based on client ID, and URL
      return `${clientId}:${url}`;
    },
    handler: (req, res) => {
      return res
        .status(codes.TOO_MANY_REQUEST)
        .json({
          error: `Too many requests for ${req.path}, please try again later.`,
        });
    },
  });
  // Apply the dynamically created limiter
  limiter(req, res, next);
};
export { customRateLimiterIP,customRateLimiterClientId, customRateLimiterClientIdWithUrl };
