import  multer from 'multer';
import {Request,Response} from 'express';
const userUpload = multer.diskStorage({
    destination: function (req:Request, file:any,cb:any) {
        cb(null, '../uploads/users')
    },
    filename: function (req:Request, file:any, cb:any) {
        cb(null, file.fieldname + '-' + Date.now() + `.${file.originalname.split('.').pop()}`)
    }
});

const adminUpload = multer.diskStorage({
    destination: function (req:Request, file:any, cb:any) {
        cb(null, '../uploads/admin')
    },
    filename: function (req:Request, file:any, cb:any) {
        cb(null, file.fieldname + '-' + Date.now() + `.${file.originalname.split('.').pop()}`)
    }
});
//8MB
let uploads = {
    user: multer({ storage: userUpload,limits: { fileSize: 8000000 } }),
    adminUpload: multer({ storage: adminUpload,limits: { fileSize: 8000000 } }),
};

//uploads.user.single('pic')
export default {uploads};