import { Server as SocketIOServer } from 'socket.io';
import customEventEmitter from '../modules/v1/common/dao/eventEmitter';
import { createAdapter } from '@socket.io/redis-adapter';
import { Server as HttpServer } from 'http';
import logger from '../utils/logger';
import connectRedisDB from "../connection/redis";
import { hGetData, hmSetData, hDeleteData, deleteData, expireKey } from "../services/redis";
import CONSTANT from "../constant/constant";
import { delay } from "../lib/universal-function";
const socketPrefix = CONSTANT.PREFIX.SOCKET;

// Create an instance of EventEmitter
const eventEmitter=customEventEmitter.getInstance()
class SocketManager {
  private socketIO: SocketIOServer;

  constructor(httpsServer: HttpServer) {
    this.socketIO = new SocketIOServer(httpsServer);
    this.initializeSocket();
  }

  private async initializeSocket() {
    const redisInstance = connectRedisDB.getInstance();
    const redisClient = redisInstance.getClient("redisServiceFn");
    const subClient = redisClient.duplicate();

    try {
      this.socketIO.adapter(createAdapter(redisClient, subClient));
      logger.info("Sockets are working now.");
    } catch (error) {
      logger.log("Server not listening to sockets.", JSON.stringify(error));
      throw error;
    }

    process.on("app",  (data:any) => {
      this.socketIO.to(data.id).emit("app", data);
    });

    process.on("dashboard", (data:any) => {
      this.socketIO.to(data.id).emit("dashboard", data);
    });
    eventEmitter.emit("voicemessage",{voiceMessage:"Bhai mongo database down hai check karlo"})
    eventEmitter.on('voicemessage', async(data) => {
      console.log('Received voiceMessage event:', data.voiceMessage);
      await delay(20000);
      this.socketIO.emit("voiceMessage", data);
    });
    this.socketIO.on("connection", async (socket: any) => {
      logger.log("connected", socket.id);

      // if (socket.handshake.query && socket.handshake.query.id) {
      //     console.log("connected", socket.handshake.query.id, socket.id);
      //     let obj = {
      //         [socket.id]: socket.handshake.query.id,
      //     };
      //     socket.join(socket.handshake.query.id);
      //     await hmSetData(socketPrefix, obj);
      //     await expireKey(socketPrefix, CONSTANT.EXPIRE_TIME.SOCKET_EXPIRATION_TIME);
      // }

      socket.on("disconnect", async function (data: any) {
        logger.log("disconnect", socket.id);
        let id = await hGetData(socketPrefix, socket.id);
        console.log("disconnect", id, socket.id);
        socket.leave(id);
        await hDeleteData(socketPrefix, socket.id);
        socket.conn.close();
      });

      // Listen for 'app' events from the client
      socket.on('app', (data: any) => {
        console.log(`Received 'app' event: ${data.message}`);

        // Broadcast the message to all connected clients
        this.socketIO.emit('app', { message: `Server says: ${data.message}` });
      });

      socket.on("ping", async function (data: any) {
        socket.emit("pong", data);
      });
    });
  }

  public async getActiveRooms() {
    // Convert map into 2D list:
    // ==> [['4ziBKG9XFS06NdtVAAAH', Set(1)], ['room1', Set(2)], ...]
    let arr = Array.from(this.socketIO.sockets.adapter.rooms);
    // Filter rooms whose name exist in set:
    // ==> [['room1', Set(2)], ['room2', Set(2)]]
    const filtered = arr.filter((room: any) => !room[1].has(room[0]))
      // Return only the room name:
      // ==> ['room1', 'room2']
    const res = filtered.map((i: any) => i[0]);
    return res;
  }

  public async activeSocket() {
    // Convert map into 2D list:
    // ==> [['4ziBKG9XFS06NdtVAAAH', Set(1)], ['room1', Set(2)], ...]
    let arr = Array.from(this.socketIO.sockets.adapter.rooms);
    // Filter rooms whose name exist in set:
    // ==> [['room1', Set(2)], ['room2', Set(2)]]
    const filtered = arr.filter((room: any) => room[1].has(room[0]))
      // Return only the room name:
      // ==> ['room1', 'room2']
    const res = filtered.map((i: any) => i[0]);
    return res;
  }
}

export default SocketManager;
