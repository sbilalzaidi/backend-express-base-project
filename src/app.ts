import express, { NextFunction } from "express";
import http from "http";
import path from "path";
import cors from "cors";
import helmet from "helmet";
import SERVER from "./config/environment";
import logger from "./utils/logger";
import connectMongoDB from "./connection/mongoose";
import graphQl from "./connection/graphql";
import connectRedisDB from "./connection/redis";
import { elasticSearch } from "./modules/v1/common/dao/elasticSearch";
import bullMQ from "./modules/v1/common/dao/bull";
import socketManager from "./middlewares/socket";
import customEventEmitter from './modules/v1/common/dao/eventEmitter';
import routes from "./modules/v1";
import { apiUrls } from "./urls/apiUrls";
import { sendErrorResponse } from "./lib/universal-function";
import { createAdminUser } from "./boostrap/initialize";
import Terminator from "./utils/terminate";
import { validateHeader } from "./middlewares/validateHeader";
import { compress } from "./middlewares/compression";
import { initFinalData } from "./script/monha";
import { imageConvert } from "./script/resizeImages";
import {
  customRateLimiterIP,
  customRateLimiterClientId,
  customRateLimiterClientIdWithUrl,
} from "./middlewares/rateLimit";
const PORT = SERVER.PORT || 3000;
const app = express();
const server = http.createServer(app);
// Creating an instance of the Terminator class
const terminator = new Terminator(server, { coredump: false, timeout: 500 });
// Middleware Setup
app.use(helmet());
app.use((req, res, next) => {
  // Replace '*' with your allowed origin if you want to restrict it to specific origins
  res.setHeader('Access-Control-Allow-Origin', 'http://13.211.101.207');
  // Set other CORS headers as needed
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization,api-key,deviceId,deviceid');
  next();
});
app.use(cors({origin: 'http://13.211.101.207'}));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(customRateLimiterIP);
// app.use(customRateLimiterClientId);
// app.use(customRateLimiterClientIdWithUrl);
app.use(compress());
//app.use(validateHeader);
app.use(express.static(path.join(__dirname, 'public')));
// Logging Middleware
app.use((req: any, res: any, next: NextFunction) => {
  if (apiUrls[req.url]) {
    logger.log(req);
  }
  next();
});
app.get("/ping", (req, res) => {
  console.log("Pong",__dirname);
  return res.send("Pong");
});
app.get('/socket-service', (req, res) => {
  res.set("Content-Security-Policy", "default-src *; style-src 'self' http://* 'unsafe-inline'; script-src 'self' http://* 'unsafe-inline' 'unsafe-eval'").sendFile(__dirname + '/public/index.html');
});
// Routes
app.use("/api/v1", routes);

// Error Handling Middleware
app.use(
  (error: ICustomErrorMessage, req: any, res: any, next: NextFunction) => {
    if (apiUrls[req.url]) {
      logger.log(JSON.stringify(error), req);
    }
    console.log(error)
    return sendErrorResponse(req, res, error);
  }
);

// Server Initialization
const startServer = async () => {
  try {
    connectMongoDB.getInstance();
    connectRedisDB.getInstance();
    // GraphQL Initialization
    graphQl.getInstance(app);
    // await elasticSearch.init();
    customEventEmitter.getInstance()
    new socketManager(server);
    bullMQ.getInstance().setupWorker(2);
    //await createAdminUser();
    // await initFinalData();
    // await imageConvert();
    logger.info("Server Listen.", process.env.NODE_ENV);
  } catch (error) {
    logger.error("Server Listener error.", error);
    throw error; // Exit with a non-zero code indicating an error
  }
};

// Start the server using async/await
server.listen(PORT, async () => {
  await startServer();
});
// Termination Handler
process.on("SIGTERM", terminator.terminate(0, "SIGTERM"));
process.on("SIGINT", terminator.terminate(0, "SIGINT"));
process.on("unhandledRejection", terminator.terminate(0, "unhandledRejection"));
