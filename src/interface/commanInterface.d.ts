declare interface ICustomError {
    code?: number;
    data?:any;
	stack?:any
  }
declare interface ICustomErrorMessage extends ICustomError{
	message:string
}
declare interface LanguageType{
	en:string;
	ar:string;
}
declare interface ILocation {
	type: string;
	coordinates: [number, number];
  }
  declare interface IShift{
	day:number;
	open:number;
	closed:number
  }
declare interface User {
	userId?: string;
	adminId?: string;
	parentId?:string;
	isPhoneNoVerified?:number;
	isEmailVerified?:number;
	adminType?: 1 | 2 | 3| 4;
}

declare interface Device extends User {
	platform?: string;
	deviceId?: string;
	excludeDeviceId?:string;
	deviceType?:string;
	deviceName?:string;
	deviceToken?: string;
	refreshToken?: string;
	accessToken?: string;
	ip?: string;
	apiKey?:string;
	timezone?: number;
}
declare interface TokenData extends Device {
	socialLoginType?: string;
	isLogoutAllDevice?:boolean;
	socialId?: string;
	permission?: string[];
	adminType?: string;
	createdAt?: number;
}

declare interface DeeplinkRequest {
	android?: string;
	ios?: string;
	fallback?: string;
	token: string;
	name: string;
	type?: string;
	accountLevel: string;
}
declare interface TerminateOptions {
    coredump?: boolean;
    timeout?: number;
}