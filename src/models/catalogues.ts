import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { AutoIncrementID } from "@typegoose/auto-increment";
import {
  CATALOGUE_TYPE,
  STATUS,
  DISCOUNT_TYPE,
  APPROVAL_STATUS,
} from "../enums/enums";
import { elasticSearch } from "../modules/v1/common/dao/elasticSearch";
import CONSTANT from "../constant/constant";
import { setData } from "../services/redis";

/**
 * Pricing logic
 * cp ,discount,sp
 * spAfterDiscount:sp-discount
 * spWithVat:sp+(spAfterDiscount of vat)
 */

export interface IVariants {
  variantNo: number;
  adminId: ObjectId;
  regionIds?: ObjectId[];
  label?: LanguageType;
  cp: number;
  sp: number;
  spAfterDiscount: number;
  spWithVat: number;
  vat?:number;
  discount?: number;
  discountType?:
    | DISCOUNT_TYPE.FLAT
    | DISCOUNT_TYPE.NO_DISCOUNT
    | DISCOUNT_TYPE.PERCENTAGE;
  variantImages?: string[];
  variantIcons?: string[];
  sku?: string;
  brand?: string;
  rating?: number;
  enableInventory?:STATUS.ACTIVE | STATUS.IN_ACTIVATED;
  inventoryOn?:STATUS.ACTIVE | STATUS.IN_ACTIVATED;
  quantity?: number;
  enableGoogleMerchantCenter:
    | STATUS.DEFAULT
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED
    | STATUS.DEFAULT;
  enableFacebookMerchantCenter:
    | STATUS.DEFAULT
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED
    | STATUS.DEFAULT;
  approvalStatus?:
    | APPROVAL_STATUS.APPROVED
    | APPROVAL_STATUS.NOT_APPROVED
    | APPROVAL_STATUS.PENDING
    | APPROVAL_STATUS.REVIEW;
  status: STATUS.ACTIVE | STATUS.IN_ACTIVATED | STATUS.DELETED;
}
export interface ICatalogues {
  _id: string;
  catalogueNo: number;
  adminId: ObjectId;
  excludeAdminIds?: Object[];
  parentId?: ObjectId;
  enableRegion?: STATUS.DEFAULT | STATUS.ACTIVE | STATUS.IN_ACTIVATED;
  regionIds?: ObjectId[];
  sku?: string;
  name?: LanguageType;
  description?: LanguageType;
  tags?: string[];
  searchTags?: string[];
  images?: string[];
  icons?: string[];
  rating?: number;
  variants: IVariants[];
  catalogueType:
    | CATALOGUE_TYPE.CATEGORY
    | CATALOGUE_TYPE.SERVICE
    | CATALOGUE_TYPE.PRODUCT;
  status: STATUS.ACTIVE | STATUS.IN_ACTIVATED | STATUS.DELETED;
}
type CataloguesType = ICatalogues & mongoose.Document;
const catalogueSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    catalogueNo: { type: Number, default: 0, index: true },
    adminId: {
      type: Schema.Types.ObjectId,
      ref: "admins",
      index: true,
      default: null,
    },
    parentId: {
      type: Schema.Types.ObjectId,
      ref: "catalogues",
      index: true,
      default: null,
    },
    excludeAdminIds: [
      {
        type: Schema.Types.ObjectId,
        ref: "admins",
        index: true,
        default: null,
      },
    ],
    enableRegion: {
      type: Number,
      enum: [STATUS.DEFAULT, STATUS.ACTIVE, STATUS.IN_ACTIVATED],
      default: STATUS.IN_ACTIVATED,
    },
    regionIds: [
      {
        type: Schema.Types.ObjectId,
        ref: "regions",
        index: true,
        default: null,
      },
    ],
    sku: { type: String, default: "", index: true },
    name: { type: Object, default: {} },
    description: { type: Object, default: {} },
    tags: [{ type: String, default: "" }],
    searchTags: [{ type: String, default: "" }],
    images: [{ type: String, default: "" }],
    icons: [{ type: String, default: "" }],
    rating: { type: Number, default: 0 },
    variants: [
      {
        variantNo: { type: Number, default: 0, index: true },
        adminId: {
          type: Schema.Types.ObjectId,
          ref: "admins",
          index: true,
          default: null,
        },
        enableRegion: {
          type: Number,
          enum: [STATUS.DEFAULT, STATUS.ACTIVE, STATUS.IN_ACTIVATED],
          default: STATUS.IN_ACTIVATED,
        },
        regionIds: [
          {
            type: Schema.Types.ObjectId,
            ref: "regions",
            index: true,
            default: null,
          },
        ],
        variantImages: [{ type: String, default: "" }],
        variantIcons: [{ type: String, default: "" }],
        sku: { type: String, default: "", index: true },
        brand: { type: String, default: "" },
        label: { type: Object, default: {} },
        cp: { type: Number, default: 0 }, //200
        sp: { type: Number, default: 0 },
        spAfterDiscount: { type: Number, default: 0 }, //vat+mrp-discount
        spWithVat: { type: Number, default: 0 }, //sp+(spAfterDiscount of vat)
        vat: { type: Number, default: 0 },
        discount: { type: Number, default: 0 },
        discountType: {
          type: Number,
          enum: [
            DISCOUNT_TYPE.FLAT,
            DISCOUNT_TYPE.NO_DISCOUNT,
            DISCOUNT_TYPE.PERCENTAGE,
          ],
          default: DISCOUNT_TYPE.NO_DISCOUNT,
        },
        rating: { type: Number, default: 0 },
        on: { type: Number, default: 1 },
        ranking: { type: Number, default: 0 },
        quantity: { type: Number, default: 0 },
        enableInventory:{
          type: Number,
          enum: [
            STATUS.ACTIVE,
            STATUS.IN_ACTIVATED
          ],
          default: STATUS.IN_ACTIVATED,
        },
        inventoryOn:{
          type: Number,
          enum: [
            STATUS.ACTIVE,
            STATUS.IN_ACTIVATED
          ],
          default: STATUS.IN_ACTIVATED,
        },
        approvalStatus: {
          type: Number,
          enum: [
            APPROVAL_STATUS.APPROVED,
            APPROVAL_STATUS.NOT_APPROVED,
            APPROVAL_STATUS.PENDING,
            APPROVAL_STATUS.REVIEW,
          ],
          default: APPROVAL_STATUS.PENDING,
        },
        status: {
          type: Number,
          enum: [
            STATUS.ACTIVE,
            STATUS.IN_ACTIVATED,
            STATUS.DELETED,
          ],
          default: STATUS.IN_ACTIVATED,
        },
      },
    ],
    catalogueType: {
      type: Number,
      enum: [
        CATALOGUE_TYPE.CATEGORY,
        CATALOGUE_TYPE.SERVICE,
        CATALOGUE_TYPE.PRODUCT,
      ],
      default: CATALOGUE_TYPE.PRODUCT,
    },
    status: {
      type: Number,
      enum: [
        STATUS.ACTIVE,
        STATUS.IN_ACTIVATED,
        STATUS.DELETED,
      ],
      default: STATUS.IN_ACTIVATED,
    },
  },
  {
    timestamps: true,
  }
);
catalogueSchema.set("toJSON", {
  transform: function (doc, ret, option) {
    ret.id = doc._id.toString();
    delete ret._id;
  },
});
catalogueSchema.plugin(AutoIncrementID, {
  field: "catalogueNo",
  incrementBy: 1,
  startAt: 1,
  trackerCollection: "catalogueCounters",
  trackerModelName: "catalogues",
});
catalogueSchema.methods.setSku = function (prefix: string, cb: any) {
  const promise = new Promise(async (resolve, reject) => {
    this.sku = prefix + this.catalogueNo; //await generateSKU(prefix+'-', 30);
    resolve(this);
  });
  if (!cb) return promise;
  promise.then((result) => cb(null, result)).catch((err) => cb(err));
};
// catalogueSchema.post('save', doc => console.log('[create] says:', doc));

// catalogueSchema.post('updateOne', doc => console.log('[update] says:', doc));
// catalogueSchema.post('update', doc => console.log('[update] says:', doc));
// catalogueSchema.post('updateMany', doc => console.log('[update] says:', doc));
// catalogueSchema.post('findOneAndUpdate', doc => console.log('[findOneAndUpdate] says:', doc));

// catalogueSchema.post('deleteOne', doc => console.log('[deleteOne] says:', doc));
// catalogueSchema.post('deleteMany', docs => console.log('[partial:x] says:', docs));
// catalogueSchema.post('findOneAndDelete', doc => console.log('[findOneAndDelete] says:', doc));
// catalogueSchema.post('findOneAndRemove', doc => console.log('[findOneAndRemove] says:', doc));
// catalogueSchema.post('remove', doc => console.log('[remove] says:', doc));
// catalogueSchema.post('findOneAndReplace', doc => console.log('[findOneAndReplace] says:', doc));
// catalogueSchema.post('replaceOne', doc => console.log('[replaceOne] says:', doc));
const Catalogues: Model<CataloguesType> = mongoose.model<CataloguesType>(
  "catalogues",
  catalogueSchema
);
// Catalogues.watch([], { fullDocument: "updateLookup" }).on(
//   "change",
//   (docs: any) => {
//     console.log(docs?.fullDocument);
//     if (docs?.fullDocument) {
//       let doc = docs?.fullDocument;
//       if (doc?._id) {
//         doc.id = String(doc._id);
//         delete doc._id;
//       }
//       let searchTags = [
//         ...Object.values(doc.name),
//         ...Object.values(doc.description),
//         doc.sku,
//       ];
//       searchTags = searchTags.filter((item) => item.length < 200);
//       doc["suggestSearchTags"] = {
//         input: searchTags,
//       };
//       setData(
//         CONSTANT.ELASTIC_SEARCH_INDEXES.CATALOGUES,
//         new Date().getTime().toString()
//       );
//       // elasticSearch.addDocument(
//       //   CONSTANT.ELASTIC_SEARCH_INDEXES.CATALOGUES,
//       //   doc.id,
//       //   doc
//       // );
//     }
//   }
// );
export default Catalogues;
