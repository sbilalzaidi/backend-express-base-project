import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import {
  ORDER_STATUS
} from "../enums/enums";


export interface IOrderActivity {
  _id: string;
  userId: ObjectId;
  storeId: ObjectId;
  finalOrderStatus:
    | ORDER_STATUS.PENDING
    | ORDER_STATUS.CANCEL
    | ORDER_STATUS.CANCEL_BY_ADMIN 
    | ORDER_STATUS.PROGRESS
    | ORDER_STATUS.COMPLETED;
}
type OrderActivityType = IOrderActivity & mongoose.Document;
const orderActivitySchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    orderNo:{ type: Number, default: 0, index: true },
    trackOrderNo:{type: String, default: 0, index: true },
    storeId: {
      type: Schema.Types.ObjectId,
      ref: "admins",
      index: true,
      default: null,
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: "users",
      index: true,
      default: null,
    },
    finalOrderStatus: {
      type: Number,
      enum: [
        ORDER_STATUS.DEFAULT,
        ORDER_STATUS.PENDING,
        ORDER_STATUS.ACCEPT,
        ORDER_STATUS.CANCEL,
        ORDER_STATUS.CANCEL_BY_ADMIN,
        ORDER_STATUS.PAKING,
        ORDER_STATUS.PICKED_UP,
        ORDER_STATUS.ON_THE_WAY,
        ORDER_STATUS.PROGRESS
      ],
      default: ORDER_STATUS.PENDING,
    },
  },
  {
    timestamps: true,
  }
);
const OrderActivities: Model<OrderActivityType> = mongoose.model<OrderActivityType>(
  "orderActivities",
  orderActivitySchema
);
export default OrderActivities;
