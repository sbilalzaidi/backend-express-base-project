import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { STATUS } from "../enums/enums";

export interface IModule {
  moduleId: ObjectId;
  add: Boolean;
  edit: Boolean;
  view: Boolean;
  delete: Boolean;
}
export interface IRoles {
  _id: string;
  adminId:ObjectId;
  role: string;
  modules: IModule[];
  status:
     STATUS.DEFAULT
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED
    | STATUS.DELETED;
}
type RoleType = IRoles & mongoose.Document;
const roleSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    adminId: {
      type: Schema.Types.ObjectId,
      ref: "admins",
      index: true,
      default: null,
    },
    role: { type: String, default: "" },
    description: { type: String, default: "" },
    modules: [
      {
        moduleId: {
          type: Schema.Types.ObjectId,
          ref: "modules",
          default: null,
        },
        add: { type: Boolean, default: null },
        edit: { type: Boolean, default: null  },
        view: { type: Boolean, default: null  },
        delete: { type: Boolean, default: null  },
      },
    ],
    status: {
      type: Number,
      enum: [STATUS.DEFAULT, STATUS.ACTIVE,STATUS.IN_ACTIVATED, STATUS.DELETED],
      default: STATUS.DEFAULT,
    },
  },
  {
    timestamps: true,
  }
);
roleSchema.set("toJSON", {
  transform: function (doc, ret, option) {
    ret.roleId = doc._id.toString();
    delete ret.__v;
  },
});

const Roles: Model<RoleType> = mongoose.model<RoleType>(
  "roles",
  roleSchema
);
export default Roles;

