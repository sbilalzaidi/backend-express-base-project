import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { STATUS, MESSAGE_TYPE } from "../enums/enums";

export interface IAdminNotifications {
  _id: string;
  receiverId: ObjectId;
  senderId: ObjectId;
  messageType:
    MESSAGE_TYPE.DEFAULT
    | MESSAGE_TYPE.ADMIN
    | MESSAGE_TYPE.CHAT
  message: string;
  extraKey: any;
  isAdminNotification: Boolean;
  isUserNotification: Boolean;
  isRead: Boolean;
  status: STATUS.DELETED | STATUS.ACTIVE | STATUS.IN_ACTIVATED;
}
type AdminNotificationType = IAdminNotifications & mongoose.Document;
const adminNotificationSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    receiverId: { type: Schema.Types.ObjectId },
    senderId: { type: Schema.Types.ObjectId , ref: "admins" }, // _id of users or admins
    userId: { type: Schema.Types.ObjectId, ref: "users" },
    adminId: { type: Schema.Types.ObjectId, ref: "admins" },
    messageType: {
      type: Number,
      enum: [
        MESSAGE_TYPE.DEFAULT,
        MESSAGE_TYPE.ADMIN,
        MESSAGE_TYPE.CHAT
      ],
      default: MESSAGE_TYPE.DEFAULT,
    },
    message: { type: String, default: "" },
    extraKey: { type: Object, default: {} },
    isAdminNotification: { type: Boolean, default: false },
    isUserNotification: { type: Boolean, default: false },
    isRead: { type: Boolean, default: false },
    status: {
      type: Number,
      enum: [STATUS.DELETED, STATUS.ACTIVE, STATUS.IN_ACTIVATED],
      default: STATUS.ACTIVE,
    },
  },
  {
    timestamps: true,
  }
);

const AdminNotifications: Model<AdminNotificationType> =
  mongoose.model<AdminNotificationType>(
    "adminNotifications",
    adminNotificationSchema
  );
export default AdminNotifications;
