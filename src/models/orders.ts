import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { AutoIncrementID } from "@typegoose/auto-increment";
import {
  DISCOUNT_TYPE,
  ORDER_STATUS,
  DELIVERY_MODE,
  PAYMENT_MODE,
  PAYMENT_STATUS,
  CASHBACK_TYPE,
  PROMO_TYPE,
} from "../enums/enums";

export interface IProduct {
  productId: ObjectId;
  variantId: ObjectId;
  name: LanguageType;
  description?: LanguageType;
  brand?: string;
  label?: LanguageType;
  images?: string[];
  icons?: string[];
  variantImages?: string[];
  variantIcons?: string[];
  cp: number;
  sp: number;
  spAfterDiscount: number;
  spWithVat: number;
  discount?: number;
  discountType?:
    | DISCOUNT_TYPE.FLAT
    | DISCOUNT_TYPE.NO_DISCOUNT
    | DISCOUNT_TYPE.PERCENTAGE;
  quantity: number;
  removedQuantity?: number;
  isQuantityRemoved?: number;
  isRemoved?: number;
}
export interface IStore {
  storeId: ObjectId;
  shopName: LanguageType;
  amount: number;
  products: IProduct[];
  cancelReason?: string;
  cancelOrderDate?: Date;
  orderStatus:
    | ORDER_STATUS.DEFAULT
    | ORDER_STATUS.PENDING
    | ORDER_STATUS.ACCEPT
    | ORDER_STATUS.CANCEL
    | ORDER_STATUS.CANCEL_BY_ADMIN
    | ORDER_STATUS.CANCEL_BY_STORE
    | ORDER_STATUS.PAKING
    | ORDER_STATUS.PICKED_UP
    | ORDER_STATUS.ON_THE_WAY
    | ORDER_STATUS.PROGRESS;
}
export interface IOrder {
  _id: string;
  orderNo: number;
  trackOrderNo: string;
  userId: ObjectId;
  cashBackId?:ObjectId;
  promoCodeId?:ObjectId;
  address: string;
  latitude: number;
  longitude: number;
  promoAmount?:number;
  promoValue?:number;
  promoType?:PROMO_TYPE.FLAT |PROMO_TYPE.PERCENTAGE |PROMO_TYPE.DEFAULT;
  cashBackAmount?:number;
  cashBackValue?:number;
  cashBackType?:CASHBACK_TYPE.FLAT |CASHBACK_TYPE.PERCENTAGE |CASHBACK_TYPE.DEFAULT;
  discountedAmount:number;
  amount: number;
  paidAmount: number;
  cancelAmount:number;
  refundedAmount:number;
  paymentMode:
    | PAYMENT_MODE.APPLE_PAY
    | PAYMENT_MODE.CARD
    | PAYMENT_MODE.CASH
    | PAYMENT_MODE.NO_PAYMENT;
  paymentStatus:
    | PAYMENT_STATUS.PENDING
    | PAYMENT_STATUS.REFUND
    | PAYMENT_STATUS.COMPLETED
    | PAYMENT_STATUS.CANCEL
    | PAYMENT_STATUS.DELETED;
  deliveryMode: DELIVERY_MODE.PICK_UP | DELIVERY_MODE.DELIVERY;
  stores: IStore[];
  cancelOrderReason: string;
  cancelOrderDate?: Date;
  deliveryOrderDate?: Date;
  finalOrderStatus:
    | ORDER_STATUS.PENDING
    | ORDER_STATUS.CANCEL
    | ORDER_STATUS.CANCEL_BY_ADMIN
    | ORDER_STATUS.PROGRESS
    | ORDER_STATUS.COMPLETED;
}
type OrderType = IOrder & mongoose.Document;
const orderSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    orderNo: { type: Number, default: 0, index: true },
    trackOrderNo: { type: String, default: 0, index: true },
    userId: {
      type: Schema.Types.ObjectId,
      ref: "users",
      index: true,
      default: null,
    },
    cashBackId: {
      type: Schema.Types.ObjectId,
      ref: "cashbacks",
      index: true,
      default: null,
    },
    promoCodeId: {
      type: Schema.Types.ObjectId,
      ref: "promocodes",
      index: true,
      default: null,
    },
    address: { type: String, default: "" },
    latitude: { type: Number, default: 0 },
    longitude: { type: Number, default: 0 },
    promoAmount: { type: Number, default: 0 },
    promoValue: { type: Number, default: 0 },
    promoType: {
          type: Number,
          enum: [
            PROMO_TYPE.FLAT,
            PROMO_TYPE.PERCENTAGE,
            PROMO_TYPE.DEFAULT
          ],
          default: PROMO_TYPE.DEFAULT,
    },
    cashBackAmount: { type: Number, default: 0 },
    cashBackValue: { type: Number, default: 0 },
    cashBackType: {
          type: Number,
          enum: [
            CASHBACK_TYPE.FLAT,
            CASHBACK_TYPE.PERCENTAGE,
            CASHBACK_TYPE.DEFAULT,
          ],
          default: CASHBACK_TYPE.DEFAULT,
    },
    amount: { type: Number, default: 0 },
    discountedAmount:{ type: Number, default: 0 },
    paidAmount: { type: Number, default: 0 },//paidAmount=amount-cancelAmount-promoAmount and cashBackAmount go to wallet
    cancelAmount:{ type: Number, default: 0 },
    refundedAmount:{ type: Number, default: 0 },
    stores: [
      {
        storeId: {
          type: Schema.Types.ObjectId,
          ref: "admins",
          index: true,
          default: null,
        },
        shopName: { type: Object, default: {} },
        products: [
          {
            productId: {
              type: Schema.Types.ObjectId,
              ref: "catalogues",
              index: true,
              default: null,
            },
            variantId: {
              type: Schema.Types.ObjectId,
              ref: "catalogues",
              index: true,
              default: null,
            },
            name: { type: Object, default: {} },
            description: { type: Object, default: {} },
            brand: { type: String, default: "" },
            label: { type: Object, default: {} },
            images: [{ type: String, default: "" }],
            icons: [{ type: String, default: "" }],
            variantImages: [{ type: String, default: "" }],
            variantIcons: [{ type: String, default: "" }],
            cp: { type: Number, default: 0 }, //200
            sp: { type: Number, default: 0 },
            spAfterDiscount: { type: Number, default: 0 }, //vat+mrp-discount
            spWithVat: { type: Number, default: 0 }, //sp+(spAfterDiscount of vat)
            vat: { type: Number, default: 0 },
            discount: { type: Number, default: 0 },
            discountType: {
              type: Number,
              enum: [
                DISCOUNT_TYPE.FLAT,
                DISCOUNT_TYPE.NO_DISCOUNT,
                DISCOUNT_TYPE.PERCENTAGE,
              ],
              default: DISCOUNT_TYPE.NO_DISCOUNT,
            },
            quantity: { type: Number, default: 0 },
            removedQuantity:{ type: Number, default: 0 },
            isQuantityRemoved:{ type: Number, default: 0 },
            isRemoved:{ type: Number, default: 0 },
          },
        ],
        amount: { type: Number, default: 0 },
        cancelReason: { type: String, default: "" },
        cancelOrderDate: { type: Date, default: "" },
        orderStatus: {
          type: Number,
          enum: [
            ORDER_STATUS.DEFAULT,
            ORDER_STATUS.PENDING,
            ORDER_STATUS.ACCEPT,
            ORDER_STATUS.CANCEL,
            ORDER_STATUS.CANCEL_BY_ADMIN,
            ORDER_STATUS.CANCEL_BY_STORE,
            ORDER_STATUS.PAKING,
            ORDER_STATUS.PICKED_UP,
            ORDER_STATUS.ON_THE_WAY,
            ORDER_STATUS.COMPLETED,
          ],
          default: ORDER_STATUS.PENDING,
        },
      },
    ],
    cancelReason: { type: String, default: "" },
    cancelOrderDate: { type: Date, default: "" },
    deliveryOrderDate: { type: Date, default: "" },
    paymentMode: {
      type: Number,
      enum: [
        PAYMENT_MODE.APPLE_PAY,
        PAYMENT_MODE.CARD,
        PAYMENT_MODE.CASH,
        PAYMENT_MODE.NO_PAYMENT,
      ],
      default: PAYMENT_MODE.CASH,
    },
    paymentStatus: {
      type: Number,
      enum: [
        PAYMENT_STATUS.PENDING,
        PAYMENT_STATUS.REFUND,
        PAYMENT_STATUS.COMPLETED,
        PAYMENT_STATUS.CANCEL,
        PAYMENT_STATUS.DELETED,
      ],
      default: PAYMENT_STATUS.PENDING,
    },
    deliveryMode: {
      type: Number,
      enum: [DELIVERY_MODE.PICK_UP, DELIVERY_MODE.DELIVERY],
      default: DELIVERY_MODE.DELIVERY,
    },
    finalOrderStatus: {
      type: Number,
      enum: [
        ORDER_STATUS.PENDING,
        ORDER_STATUS.CANCEL,
        ORDER_STATUS.CANCEL_BY_ADMIN,
        ORDER_STATUS.PROGRESS,
        ORDER_STATUS.COMPLETED,
      ],
      default: ORDER_STATUS.PENDING,
    },
  },
  {
    timestamps: true,
  }
);
orderSchema.plugin(AutoIncrementID, {
  field: "orderNo",
  incrementBy: 1,
  startAt: 1000,
  trackerCollection: "orderCounters",
  trackerModelName: "orders",
});
const Orders: Model<OrderType> = mongoose.model<OrderType>(
  "orders",
  orderSchema
);
export default Orders;
