import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { STATUS } from "../enums/enums";
export interface IPaymentCards {
  _id: string;
  userId: ObjectId;
  paymentBrand: string;
  currency: string;
  customerId: string;
  cardNumber: string;
  cardHolderName: string;
  expirationDate: Date;
  cvv: string;
  expiryMonth: string;
  expiryYear: string;
  card: any;
  status: STATUS.ACTIVE | STATUS.DELETED;
}
type PaymentCardType = IPaymentCards & mongoose.Document;
const paymentCardSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    userId: { type: Schema.Types.ObjectId, ref: "users" },
    paymentBrand: {
      type: String,
      default: "",
    },
    currency: {
      type: String,
      default: "",
    },
    customerId: {
      type: String,
      default: "",
    },
    cardNumber: {
      type: String,
      required: true,
    },
    cardHolderName: {
      type: String,
    },
    expiryMonth: {
      type: String,
    },
    expiryYear: {
      type: String,
    },
    expirationDate: {
      type: String,
      required: true,
    },
    cvv: {
      type: String,
      required: true,
    },
    card: {
      type: Object,
      required: true,
    },
    status: {
      type: Number,
      enum: [STATUS.ACTIVE, STATUS.DELETED],
      default: STATUS.ACTIVE,
    },
  },
  {
    timestamps: true,
  }
);

const PaymentCards: Model<PaymentCardType> = mongoose.model<PaymentCardType>(
  "paymentCards",
  paymentCardSchema
);
export default PaymentCards;
