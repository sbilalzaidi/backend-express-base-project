import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import bcrypt from "bcryptjs";
import { LOGIN_TYPE, STATUS } from "../enums/enums";
import messages from "../messages/messages";
export interface IAddresses {
  addressType: string;
  country: string;
  state: string;
  city: string;
  address: string;
  latitude: Number;
  longitude: Number;
  isDefault: Boolean;
}
export interface IUser {
  _id: string;
  userName: string;
  firstName?: string;
  lastName?: string;
  email: string;
  phoneNo: string;
  countryCode: string;
  password: string;
  walletAmount: Number;
  provider:
   LOGIN_TYPE.DEFAULT
    | LOGIN_TYPE.NORMAL
    | LOGIN_TYPE.GOOGLE
    | LOGIN_TYPE.FACEBOOK;
  providerId: string;
  isSocialLogin: Boolean;
  isEmailVerified: Boolean;
  isPhoneNoVerified: Boolean;
  addresses: IAddresses[];
  status:
    STATUS.DEFAULT
    | STATUS.PENDING
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED
    | STATUS.DELETED;
}

type UserType = IUser & mongoose.Document;
const userSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    userName: { type: String, default: "" },
    firstName: { type: String, default: "" },
    lastName: { type: String, default: "" },
    email: { type: String, index: true, default: "" },
    phoneNo: { type: String, index: true, default: "" },
    countryCode: { type: String, index: true, default: "" },
    password: { type: String, index: true, default: "" },
    walletAmount: { type: Number, default: 0 },
    provider: {
      type: Number,
      enum: [
        LOGIN_TYPE.DEFAULT,
        LOGIN_TYPE.NORMAL,
        LOGIN_TYPE.GOOGLE,
        LOGIN_TYPE.FACEBOOK,
      ],
      default: LOGIN_TYPE.NORMAL,
    },
    providerId: { type: String, default: "" },
    isSocialLogin: { type: Boolean, default: false },
    isEmailVerified: { type: Boolean, default: false },
    isPhoneNoVerified: { type: Boolean, default: false },
    addresses: [
      {
        addressType: { type: String, default: "" },
        country: { type: String, default: "" },
        state: { type: String, default: "" },
        city: { type: String, default: "" },
        address: { type: String, default: "" },
        latitude: { type: Number, default: 0 },
        longitude: { type: Number, default: 0 },
        isDefault: { type: Boolean, default: true },
      },
    ],
    status: {
      type: String,
      enum: [
        STATUS.DEFAULT,
        STATUS.PENDING,
        STATUS.ACTIVE,
        STATUS.IN_ACTIVATED,
        STATUS.DELETED,
      ],
      default: STATUS.DEFAULT,
    },
  },
  {
    timestamps: true,
  }
);

userSchema.set("toJSON", {
  transform: function (doc, ret, option) {
    ret.userId = doc._id.toString();
    delete ret._id;
    delete ret.password;
    delete ret.__v;
  },
});

userSchema.methods.authenticate = function (password: string, cb: any) {
  const promise = new Promise((resolve, reject) => {
    if (!password) reject(new Error(messages.MISSING_PASSWORD));

    bcrypt.compare(password, this.password, (error, result) => {
      if (!result) reject(new Error(messages.WRONG_PASSWORD));
      resolve(this);
    });
  });

  if (!cb) return promise;
  promise.then((result) => cb(null, result)).catch((err) => cb(err));
};

userSchema.methods.setPassword = function (password: string, cb: any) {
  const promise = new Promise((resolve, reject) => {
    if (!password) reject(new Error(messages.MISSING_PASSWORD));

    bcrypt.hash(password, 10, (err, hash) => {
      if (err) reject(err);
      this.password = hash;
      resolve(this);
    });
  });

  if (!cb) return promise;
  promise.then((result) => cb(null, result)).catch((err) => cb(err));
};
const Users: Model<UserType> = mongoose.model<UserType>("users", userSchema);
export default Users;
