import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { AutoIncrementID } from "@typegoose/auto-increment";
import bcrypt from "bcryptjs";
import { ADMIN_TYPE, STATUS,APPROVAL_STATUS } from "../enums/enums";
import messages from "../messages/messages";

export interface IShift {
  day: number;
  open: number;
  closed: number;
}

interface IAddress {
  addressType: string;
  country?: string;
  state?: string;
  city?: string;
  address: string;
  latitude: number;
  longitude: number;
  isDefault: boolean;
}
interface IStore {
  shopName: LanguageType;
  country?: string;
  state?: string;
  city?: string;
  address: string;
  latitude: number;
  longitude: number;
  icons?: string[];
  images?: string[];
  banners?: string[];
  regionIds?: ObjectId[];
  enableRegions:STATUS.ACTIVE | STATUS.IN_ACTIVATED;
  enableServiceRadius?:STATUS.ACTIVE | STATUS.IN_ACTIVATED;
  serviceRadius?: number;
  location?: ILocation;
  enableShift?: STATUS.ACTIVE | STATUS.IN_ACTIVATED;
  shift?: IShift[];
  enableGoogleMerchantCenter:
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED;
  enableFacebookMerchantCenter:
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED;
  autoApprovedEnabled?: STATUS.ACTIVE | STATUS.IN_ACTIVATED;
  enableInventory?: STATUS.ACTIVE | STATUS.IN_ACTIVATED;
  ranking?: number;
  rating?: 0;
}
export interface IAdmin {
  _id: string;
  adminNo: number;
  roleId?: ObjectId[];
  parentId?: ObjectId;
  firstName?: string;
  lastName?: string;
  email: string;
  phoneNo: string;
  countryCode: string;
  password: string;
  profilePic?: {
    original?: string;
    thumbNail?: string;
  };
  addresses?: IAddress[];
  shopInfo?: IStore;
  adminType:
    | ADMIN_TYPE.ADMIN
    | ADMIN_TYPE.ADMIN_USER
    | ADMIN_TYPE.COMPANY
    | ADMIN_TYPE.COMPANY_USER;
  isEmailVerified : APPROVAL_STATUS.APPROVED | APPROVAL_STATUS.PENDING;
  isPhoneNoVerified : APPROVAL_STATUS.APPROVED | APPROVAL_STATUS.PENDING;
  status:
    | STATUS.DEFAULT
    | STATUS.PENDING
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED
    | STATUS.DELETED;
}

type AdminType = IAdmin & mongoose.Document;
const adminSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    adminNo: { type: Number, default: 0, index: true },
    roleId: [
      {
        type: Schema.Types.ObjectId,
        ref: "roles",
        index: true,
        default: null,
      },
    ],
    parentId: {
      type: Schema.Types.ObjectId,
      ref: "admins",
      index: true,
      default: null,
    },
    firstName: { type: String, default: "" },
    lastName: { type: String, default: "" },
    email: { type: String, index: true, default: "" },
    phoneNo: { type: String, index: true, default: "" },
    countryCode: { type: String, index: true, default: "" },
    password: { type: String, index: true, default: "" },
    profilePic: {
      original: { type: String, default: "" },
      thumbNail: { type: String, default: "" },
    },
    addresses: [
      {
        addressType: { type: String, default: "" },
        country: { type: String, default: "" },
        state: { type: String, default: "" },
        city: { type: String, default: "" },
        address: { type: String, default: "" },
        latitude: { type: Number, default: 0 },
        longitude: { type: Number, default: 0 },
        isDefault: { type: Boolean, default: true },
      },
    ],
    store: {
      shopName: { type: Object, default: {} },
      country: { type: String, default: "" },
      state: { type: String, default: "" },
      city: { type: String, default: "" },
      address: { type: String, default: "" },
      latitude: { type: Number, default: 0 },
      longitude: { type: Number, default: 0 },
      icons: [{ type: String, default: "" }],
      images: [{ type: String, default: "" }],
      banners: [{ type: String, default: "" }],
      regionIds: [
        {
          type: Schema.Types.ObjectId,
          ref: "regions",
          index: true,
          default: null,
        },
      ],
      enableRegions: {
        type: Number,
        enum: [STATUS.ACTIVE, STATUS.IN_ACTIVATED],
        default: STATUS.IN_ACTIVATED,
      },
      serviceRadius: { type: Number, default: 0 },
      enableServiceRadius: {
        type: Number,
        enum: [STATUS.ACTIVE, STATUS.IN_ACTIVATED],
        default: STATUS.IN_ACTIVATED,
      },
      location: {
        type: { type: String, default: "Point" },
        coordinates: { type: [Number], default: [0, 0] },//[<longitude>, <latitude>] opposite to gogoole map
      },
      autoApprovedEnabled: {
        type: Number,
        enum: [STATUS.ACTIVE, STATUS.IN_ACTIVATED],
        default: STATUS.IN_ACTIVATED,
      },
      enableInventory:{
        type: Number,
        enum: [
          STATUS.ACTIVE,
          STATUS.IN_ACTIVATED
        ],
        default: STATUS.IN_ACTIVATED,
      },
      enableGoogleMerchantCenter: {
        type: Number,
        enum: [STATUS.ACTIVE, STATUS.IN_ACTIVATED],
        default: STATUS.IN_ACTIVATED,
      },
      enableFacebookMerchantCenter: {
        type: Number,
        enum: [STATUS.ACTIVE, STATUS.IN_ACTIVATED],
        default: STATUS.IN_ACTIVATED,
      },
      enableShift: {
        type: Number,
        enum: [STATUS.ACTIVE, STATUS.IN_ACTIVATED],
        default: STATUS.IN_ACTIVATED,
      },
      shift: [
        {
          day: { type: Number, default: 0 },
          open: { type: Number, default: 0 },
          closed: { type: Number, default: 0 },
        },
      ],
      ranking: { type: Number, default: 0 },
      rating: { type: Number, default: 0 },
    },
    notification: { type: Object, default: "" },
    adminType: {
      type: Number,
      enum: [
        ADMIN_TYPE.ADMIN,
        ADMIN_TYPE.ADMIN_USER,
        ADMIN_TYPE.COMPANY,
        ADMIN_TYPE.COMPANY_USER,
      ],
      default: ADMIN_TYPE.ADMIN_USER,
    },
    isEmailVerified:{
      type: Number,
      enum: [APPROVAL_STATUS.APPROVED, APPROVAL_STATUS.PENDING],
      default: APPROVAL_STATUS.PENDING,
    },
    isPhoneNoVerified:{
      type: Number,
      enum: [APPROVAL_STATUS.APPROVED, APPROVAL_STATUS.PENDING],
      default: APPROVAL_STATUS.PENDING,
    },
    status: {
      type: Number,
      enum: [
        STATUS.DEFAULT,
        STATUS.PENDING,
        STATUS.ACTIVE,
        STATUS.IN_ACTIVATED,
        STATUS.DELETED,
      ],
      default: STATUS.DEFAULT,
    },
  },
  {
    timestamps: true,
  }
);
adminSchema.index({ 'store.location': "2dsphere" });
adminSchema.set("toJSON", {
  transform: function (doc, ret, option) {
    ret.adminId = doc._id.toString();
    delete ret._id;
    delete ret.password;
    delete ret.__v;
  },
});
adminSchema.plugin(AutoIncrementID, {
  field: "adminNo",
  incrementBy: 1,
  startAt: 1,
  trackerCollection: "adminCounters",
  trackerModelName: "admins",
});
adminSchema.methods.authenticate = function (password: string, cb: any) {
  const promise = new Promise((resolve, reject) => {
    if (!password) reject(new Error(messages.MISSING_PASSWORD));
    bcrypt.compare(password, this.password, (error, result) => {
      if (!result) reject(new Error(messages.WRONG_PASSWORD));
      resolve(this);
    });
  });

  if (!cb) return promise;
  promise.then((result) => cb(null, result)).catch((err) => cb(err));
};

adminSchema.methods.setPassword = function (password: string, cb: any) {
  const promise = new Promise((resolve, reject) => {
    if (!password) reject(new Error(messages.MISSING_PASSWORD));

    bcrypt.hash(password, 10, (err, hash) => {
      if (err) reject(err);
      this.password = hash;
      resolve(this);
    });
  });

  if (!cb) return promise;
  promise.then((result) => cb(null, result)).catch((err) => cb(err));
};
const Admins: Model<AdminType> = mongoose.model<AdminType>(
  "admins",
  adminSchema
);
export default Admins;
