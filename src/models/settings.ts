import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { STATUS } from "../enums/enums";

export interface ISetting {
  _id: ObjectId;
  adminId: ObjectId;
  paymentConfig?: any;
  smsConfig?: any;
  emailConfig?: any;
  webhookConfig?: any;
  vat?: number;
  deliveryCharge?:number;
  status: STATUS.ACTIVE | STATUS.IN_ACTIVATED | STATUS.DELETED;
}
type SettingType = ISetting & mongoose.Document;
const settingSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    adminId: {
      type: Schema.Types.ObjectId,
      ref: "admins",
      index: true,
      default: null,
    },
    paymentConfig: { type: Object, default: "" },
    smsConfig: { type: Object, default: "" },
    emailConfig: { type: Object, default: "" },
    webhookConfig: { type: Object, default: "" },
    vat: { type: Number, default: 0 },
    deliveryCharge: { type: Number, default: 0 },
    status: {
      type: Number,
      enum: [STATUS.ACTIVE, STATUS.IN_ACTIVATED, STATUS.DELETED],
      default: STATUS.ACTIVE,
    },
  },
  {
    timestamps: true,
  }
);

const Settings: Model<SettingType> = mongoose.model<SettingType>(
  "settings",
  settingSchema
);
export default Settings;
