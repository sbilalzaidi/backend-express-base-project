import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { STATUS } from "../enums/enums";


export interface IRegion {
  _id: ObjectId;
  name:string;
  description:string;
  status:
     STATUS.DEFAULT
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED
    | STATUS.DELETED;
}
type RegionType = IRegion & mongoose.Document;
const regionSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    name: { type:String, default: '',index:true },
    description: { type:String, default: '' },
    boundaries: {
        type: {
          type: String,
          enum: ['Polygon'], // We're using a simple polygon in this example
          required: true,
        },
        coordinates: {
          type: [[[Number]]], // Nested array of coordinates for the polygon
          required: true,
        },
    },
    status: {
      type: Number,
      enum: [STATUS.DEFAULT, STATUS.ACTIVE,STATUS.IN_ACTIVATED, STATUS.DELETED],
      default: STATUS.DEFAULT,
    },
  },
  {
    timestamps: true,
  }
);
regionSchema.index({ geometry: '2dsphere' });
regionSchema.set("toJSON", {
  transform: function (doc, ret, option) {
    ret.regionId = doc._id.toString();
    delete ret.__v;
  },
});

const Regions: Model<RegionType> = mongoose.model<RegionType>(
  "regions",
  regionSchema
);
export default Regions;

