import * as mongoose from "mongoose";
import { Model, Schema } from "mongoose";
import { STATUS,ADMIN_TYPE } from "../enums/enums";

export interface IModules {
  _id: string;
  name: string;
  add: Boolean;
  edit: Boolean;
  view: Boolean;
  delete: Boolean;
  visibilityFor:ADMIN_TYPE.ADMIN | ADMIN_TYPE.COMPANY;
  status:
     STATUS.DEFAULT
    | STATUS.ACTIVE
    | STATUS.IN_ACTIVATED
    | STATUS.DELETED;
}
type ModuleType = IModules & mongoose.Document;
const moduleSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    name: { type: String, default: "" },
    add: { type: Boolean, default: null },
    edit: { type: Boolean, default: null  },
    view: { type: Boolean, default: null  },
    delete: { type: Boolean, default: null  },
    visibilityFor:{
      type: Number,
      enum: [ADMIN_TYPE.ADMIN, ADMIN_TYPE.COMPANY],
      default: ADMIN_TYPE.COMPANY,
    },
    status: {
      type: Number,
      enum: [STATUS.DEFAULT, STATUS.ACTIVE,STATUS.IN_ACTIVATED, STATUS.DELETED],
      default: STATUS.DEFAULT,
    },
  },
  {
    timestamps: true,
  }
);
moduleSchema.set("toJSON", {
  transform: function (doc, ret, option) {
    ret.moduleId = doc._id.toString();
    delete ret.__v;
  },
});

const Modules: Model<ModuleType> = mongoose.model<ModuleType>(
  "modules",
  moduleSchema
);
export default Modules;
