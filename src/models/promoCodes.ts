import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { STATUS,PROMO_TYPE } from "../enums/enums";

export interface IPromoCode {
  _id: ObjectId;
  adminId: ObjectId;
  userIds?: ObjectId[];
  usedByUsers?:ObjectId[];
  promoCode:string;
  promoValue:number;
  promoType:PROMO_TYPE.FLAT |PROMO_TYPE.PERCENTAGE;
  minAmount?:number;
  perUserFrequency?: number;
  totalFrequency?: number;
  startDate?: Date;
  expiryDate?: Date;
  status: STATUS.ACTIVE | STATUS.IN_ACTIVATED | STATUS.DELETED;
}
type PromoCodeType = IPromoCode & mongoose.Document;
const promoCodeSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    adminId: {
      type: Schema.Types.ObjectId,
      ref: "admins",
      index: true,
      default: null,
    },
    userIds: [
      {
        type: Schema.Types.ObjectId,
        ref: "users",
        index: true,
        default: null,
      },
    ],
    usedByUsers: [
      {
        type: Schema.Types.ObjectId,
        ref: "users",
        index: true,
        default: null,
      },
    ],
    promoCode:{ type: String, default: '',index:true},
    promoValue: { type: Number, default: 0 },
    promoType: {
          type: Number,
          enum: [
            PROMO_TYPE.FLAT,
            PROMO_TYPE.PERCENTAGE,
          ],
          default: PROMO_TYPE.PERCENTAGE,
    },
    minAmount: { type: Number, default: 0 },
    perUserFrequency: { type: Number, default: 0 },
    totalFrequency: { type: Number, default: 0 },
    startDate: { type: Date, default: "" },
    expiryDate: { type: Date, default: "" },
    status: {
      type: Number,
      enum: [STATUS.ACTIVE, STATUS.IN_ACTIVATED, STATUS.DELETED],
      default: STATUS.ACTIVE,
    },
  },
  {
    timestamps: true,
  }
);

const PromoCodes: Model<PromoCodeType> = mongoose.model<PromoCodeType>(
  "promoCodes",
  promoCodeSchema
);
export default PromoCodes;
