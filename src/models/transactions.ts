import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { PAYMENT_MODE, PAYMENT_STATUS } from "../enums/enums";
export interface ITransactions {
  _id: string;
  userId: ObjectId;
  orderId: ObjectId;
  transactionId: ObjectId;
  amount: number;
  paidAmount: number;
  refundAmount:number;
  cancelAmount:number;
  refundReason:string;
  cancelReason:string;
  card: any;
  paymentMode:
    | PAYMENT_MODE.APPLE_PAY
    | PAYMENT_MODE.CARD
    | PAYMENT_MODE.CASH
    | PAYMENT_MODE.NO_PAYMENT;
  paymentStatus:
    | PAYMENT_STATUS.PENDING
    | PAYMENT_STATUS.REFUND
    | PAYMENT_STATUS.COMPLETED
    | PAYMENT_STATUS.CANCEL
    | PAYMENT_STATUS.DELETED;
}
type TransactionType = ITransactions & mongoose.Document;
const transactionSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    userId: { type: Schema.Types.ObjectId, ref: "users" },
    orderId: { type: Schema.Types.ObjectId, ref: "orders" },
    transactionId: { type: String, default: "" },
    amount: { type: Number, default: 0 },
    paidAmount: { type: Number, default: 0 },
    refundAmount: { type: Number, default: 0 },
    cancelAmount: { type: Number, default: 0 },
    refundReason: { type: String, default: "" },
    cancelReason: { type: String, default: "" },
    card: { type: Object, default: null },
    paymentMode: {
      type: Number,
      enum: [
        PAYMENT_MODE.APPLE_PAY,
        PAYMENT_MODE.CARD,
        PAYMENT_MODE.CASH,
        PAYMENT_MODE.NO_PAYMENT,
      ],
      default: PAYMENT_MODE.NO_PAYMENT,
    },
    paymentStatus: {
      type: Number,
      enum: [
        PAYMENT_STATUS.PENDING,
        PAYMENT_STATUS.REFUND,
        PAYMENT_STATUS.COMPLETED,
        PAYMENT_STATUS.CANCEL,
        PAYMENT_STATUS.DELETED,
      ],
      default: PAYMENT_STATUS.PENDING,
    },
  },
  {
    timestamps: true,
  }
);

const Transactions: Model<TransactionType> = mongoose.model<TransactionType>(
  "transactions",
  transactionSchema
);
export default Transactions;
