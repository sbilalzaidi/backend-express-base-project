import * as mongoose from "mongoose";
import { Model, Schema, ObjectId } from "mongoose";
import { STATUS,CASHBACK_TYPE } from "../enums/enums";

export interface ICashBack {
  _id: ObjectId;
  adminId: ObjectId;
  userIds?: ObjectId[];
  usedByUsers?:ObjectId[];
  cashBackValue:number;
  cashBackType:CASHBACK_TYPE.FLAT |CASHBACK_TYPE.PERCENTAGE;
  minAmount?:number;
  perUserFrequency?: number;
  totalFrequency?: number;
  startDate?: Date;
  expiryDate?: Date;
  status: STATUS.ACTIVE | STATUS.IN_ACTIVATED | STATUS.DELETED;
}
type CashBackType = ICashBack & mongoose.Document;
const cashBackSchema = new Schema(
  {
    _id: { type: Schema.Types.ObjectId, required: true, auto: true },
    adminId: {
      type: Schema.Types.ObjectId,
      ref: "admins",
      index: true,
      default: null,
    },
    userIds: [
      {
        type: Schema.Types.ObjectId,
        ref: "users",
        index: true,
        default: null,
      },
    ],
    usedByUsers: [
      {
        type: Schema.Types.ObjectId,
        ref: "users",
        index: true,
        default: null,
      },
    ],
    cashBackValue: { type: Number, default: 0 },
    cashBackType: {
          type: Number,
          enum: [
            CASHBACK_TYPE.FLAT,
            CASHBACK_TYPE.PERCENTAGE,
          ],
          default: CASHBACK_TYPE.PERCENTAGE,
    },
    minAmount: { type: Number, default: 0 },
    perUserFrequency: { type: Number, default: 0 },
    totalFrequency: { type: Number, default: 0 },
    startDate: { type: Date, default: "" },
    expiryDate: { type: Date, default: "" },
    status: {
      type: Number,
      enum: [STATUS.ACTIVE, STATUS.IN_ACTIVATED, STATUS.DELETED],
      default: STATUS.ACTIVE,
    },
  },
  {
    timestamps: true,
  }
);

const CashBacks: Model<CashBackType> = mongoose.model<CashBackType>(
  "cashBacks",
  cashBackSchema
);
export default CashBacks;
