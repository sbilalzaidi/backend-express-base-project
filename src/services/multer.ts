import multer from 'multer';
import {randomString} from "../lib/universal-function"
const userUpload = multer.diskStorage({
    destination: function (req:any, file:any, cb) {
        cb(null, './uploads/users')
    },
    filename: async function (req:any, file:any, cb) {
        cb(null, file.fieldname + '-'+await randomString(10)+"-" + Date.now() + `.${file.originalname.split('.').pop()}`)
    }
});

const adminUpload = multer.diskStorage({
    destination: function (req:any, file:any, cb) {
        cb(null, './uploads/admins')
    },
    filename: async function (req:any, file:any, cb) {
        cb(null, file.fieldname + '-' +await randomString(10)+"-"+ Date.now() + `.${file.originalname.split('.').pop()}`)
    }
});
//8MB
let uploads = {
    user: multer({ storage: userUpload,limits: { fileSize: 8000000 } }),
    adminUpload: multer({ storage: adminUpload,limits: { fileSize: 8000000 } }),
};

//uploads.user.single('pic')
export {
    uploads
}
