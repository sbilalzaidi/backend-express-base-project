
import fs from "fs";
import AWSSDK from "aws-sdk";
import gm from "gm";
import SERVER from "../config/environment";
const accessKeyId = SERVER.S3.ACCESS_KEY_ID;
const secretAccessKey = SERVER.S3.SECRET_ACCESS_KEY;
const bucketName = SERVER.S3.BUCKET
const Url = SERVER.S3.URL
const deleteFile=async(filePath:string)=>{
    if (fs.existsSync(filePath)) 
        fs.unlinkSync(filePath);
    return true;
}
const uploadOriginalImageToS3Bucket = async (file:any, folder:string) => {
    const filename = file.filename;
    const path = file.path;
    const mimeType = file.mimetype;

    try {
        const file_buffer = await fs.promises.readFile(path);
        AWSSDK.config.update({
            accessKeyId: accessKeyId,
            secretAccessKey: secretAccessKey,
        });
        const s3bucket = new AWSSDK.S3();
        const params:any = {
            Bucket: bucketName,
            Key: `${folder}/${filename}`,
            Body: file_buffer,
            ACL: "public-read",
            ContentType: mimeType,
        };
        await s3bucket.putObject(params).promise();
        deleteFile(path);
        return `${Url}/${folder}/${filename}`;
    } catch (error) {
        return 0;
    }
};

const uploadImageToS3Bucket = async (file:any, folder:string) => {
    let dataToSend:any = {
        original: "",
        thumbNail: "",
    };
    dataToSend.original = await uploadOriginalImageToS3Bucket(file, folder);
    dataToSend.thumbNail = dataToSend.original ;
    return dataToSend;
};
export  {
    uploadImageToS3Bucket,
    uploadOriginalImageToS3Bucket
};
