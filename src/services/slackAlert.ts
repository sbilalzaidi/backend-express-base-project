import SERVER from "../config/environment";
import logger from "../utils/logger";
import axios from "./axios";
const sendAlert = async (text: string) => {
  try {
    const headers = {
      "content-type": "application/json",
    };
    let url = `${SERVER.SLACK.ALERT_URL}/${SERVER.SLACK.ALERT_TOKEN}`;
    await axios.sendPOST(url, null, { text: text }, headers);
    logger.info("Send alert to slack.");
    return true;
  } catch (error: any) {
    logger.error("Error in send alert to slack", {
      text,
      error: error.message || error,
    });
    return false;
  }
};
export { sendAlert };
