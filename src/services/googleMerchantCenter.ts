import SERVER from "../config/environment";
import logger from "../utils/logger";
import {google} from "googleapis"

interface IPRODUCT_PRICE{
    value:number;
    currency:string; //eg. "SAR"
}
interface IPRODUCT{
    batchId?:number;// productId
    channel?:"online";
    title:string;  //best 40 chars
    targetCountry:string;// eg. "SA"
    contentLanguage:string; //eg. "ar"
    description?:string //best 2 words max 500 chars
    link:string;
    imageLink:string;
    availability:"in stock" | "out stock";
    price:IPRODUCT_PRICE,
    method?:'insert' | 'update' | 'delete';
    merchantId?:string;
}

const upsertProductsInGoogleMerchant = async (
    adminId: string,
    merchantId: string,
    products:IPRODUCT[]
  ) => {
    try {
        products=products.map((product:IPRODUCT)=>{
            return {
                ...product,
                merchantId:merchantId,
                method:"insert",

            }
        })
        const auth = new google.auth.GoogleAuth({
            keyFile: SERVER.GOOGLE.MERCHANT_CONTENT_KEY_PATH,
            scopes: [SERVER.GOOGLE.APIS_AUTH_URL]
        });
        const client = await auth.getClient();
        const content = await google.content({
            'version': 'v2',
            'auth': client as any
        });
        await content.products.custombatch({
            requestBody: {"entries": products},
        });
        logger.info("Products successfully upserted in Google Merchant.");
        return true
    } catch (error:any) {
      logger.error("Error in upserting products in Google Merchant", {
        adminId,
        merchantId,
        error: error.message || error,
      });
      return false
    }
}
export { upsertProductsInGoogleMerchant }
