import connectRedisDB from "../connection/redis";
const redisInstance = connectRedisDB.getInstance();
let redisClient = redisInstance.getClient("redisServiceFn");
import {
  isJson,
} from "../lib/universal-function";
const setData = async (key: string, value: string): Promise<string> => {
  try {
    await redisClient.set(key, value);
    return 'OK'; // Assuming the set operation was successful
  } catch (error) {
    throw error;
  }
};
const setDataWithExpire = async (key: string, value: string,seconds: number ): Promise<string> => {
  try {
    await redisClient.set(key, value,'EX',seconds);
    return 'OK'; // Assuming the set operation was successful
  } catch (error) {
    throw error;
  }
};
const getData = async (key: string): Promise<any> => {
  try {
    let data = await redisClient.get(key);
    return await isJson(data);
  } catch (error) {
    throw error;
  }
};

const deleteData = async (key: string): Promise<number> => {
  try {
    const result = await redisClient.del(key);
    return result;
  } catch (error) {
    throw error;
  }
};

const hlen = async (key: string): Promise<number> => {
  try {
    return await redisClient.hlen(key);
  } catch (error) {
    throw error;
  }
};

const hSetData = async (hkey: any, key: string, value: any): Promise<number> => {
  try {
    return await redisClient.hset(hkey, key, value);
  } catch (error) {
    throw error;
  }
};

const hGetData = async (hkey: any, key: string): Promise<string | null> => {
  try {
    return await redisClient.hget(hkey, key);
  } catch (error) {
    throw error;
  }
};

const hDeleteData = async (hkey: any, field: string): Promise<number> => {
  try {
    return await redisClient.hdel(hkey, field);
  } catch (error) {
    throw error;
  }
};

const hExistsData = async (hkey: any, key: string): Promise<number> => {
  try {
    return await redisClient.hexists(hkey, key);
  } catch (error) {
    throw error;
  }
};

const hmSetData = async (hkey: any, obj: any): Promise<string> => {
  try {
    await redisClient.hmset(hkey, obj);
    return 'OK'; // Assuming the hmset operation was successful
  } catch (error) {
    throw error;
  }
};

const hmGetData = async (hmkey: any, arr: any[]): Promise<any> => {
  try {
    return await redisClient.hmget(hmkey, ...arr);
  } catch (error) {
    throw error;
  }
};

const hgetallData = async (hmkey: any): Promise<{ [key: string]: string }> => {
  try {
    return await redisClient.hgetall(hmkey);
  } catch (error) {
    throw error;
  }
};

const hmDeleteData = async (hmkey: any): Promise<number> => {
  try {
    return await redisClient.hdel(hmkey);
  } catch (error) {
    throw error;
  }
};

const hmExistsData = async (hkey: any, key: string): Promise<number> => {
  try {
    return await redisClient.hexists(hkey, key);
  } catch (error) {
    throw error;
  }
};

const keys = async (keyPattern: any): Promise<string[]> => {
  try {
    return await redisClient.keys(keyPattern);
  } catch (error) {
    throw error;
  }
};

const expireKey = async (key: string, expiryTime: number): Promise<number> => {
  try {
    return await redisClient.expire(key, expiryTime);
  } catch (error) {
    throw error;
  }
};

export {
  getData,
  setData,
  setDataWithExpire,
  deleteData,
  hSetData,
  hGetData,
  hDeleteData,
  hExistsData,
  hmSetData,
  hmGetData,
  hmDeleteData,
  hmExistsData,
  hlen,
  keys,
  hgetallData,
  expireKey
};
