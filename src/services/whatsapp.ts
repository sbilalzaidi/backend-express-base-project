import axios from "./axios";
import SERVER from "../config/environment"
import logger from "../utils/logger";
//https://developers.facebook.com/blog/post/2022/10/31/sending-messages-with-whatsapp-in-your-nodejs-application/
//https://www.youtube.com/watch?v=gqiBzFlF44c&list=PLi9cSkiBrnxa58GtrU8OdNiBErd20I0JY&index=5
/**
 * @param data {
      to: 'whatsapp:your_channel_id', // Replace with the WhatsApp channel ID or group chat ID
      text: 'Hello, this is a WhatsApp message to a channel!',
    }
    OR template send object
 */
const sendMessage=async (data:any)=> {
    try {
        const url=`${SERVER.WHATSAPP.HOST}/${SERVER.WHATSAPP.VERSION}/${SERVER.WHATSAPP.PHONE_NUMBER_ID}/${SERVER.WHATSAPP.MESSAGE_URL}`
        const headers= {
            'Authorization': `Bearer ${SERVER.WHATSAPP.ACCESS_TOKEN}`,
            'Content-Type': 'application/json'
          };
        await axios.sendPOST(url,null,data,headers);
        logger.info("Send message to whatsapp user.");
        return true
    } catch (error:any) {
        logger.error("Error in send message to whatsapp user", {
            data,
            error: error.message || error,
          });
          return false
    }
}
  

export { sendMessage }