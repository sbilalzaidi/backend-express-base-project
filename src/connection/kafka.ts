import {
  Admin,
  AdminConfig,
  Consumer,
  ConsumerConfig,
  Kafka,
  Logger,
  Producer,
  ProducerConfig,
  logLevel,
} from "kafkajs";
import SERVER from "../config/environment";
import logger from "../utils/logger";

class KafkaConnection {
  private static kafkaInstance: KafkaConnection;
  private kafkaClient!: Kafka;

  private constructor() {
    this.connect();
  }

  public static getInstance(): KafkaConnection {
    if (!this.kafkaInstance) {
      this.kafkaInstance = new KafkaConnection();
    }
    return this.kafkaInstance;
  }

  private connect(): void {
    logger.info("Kafka: Connecting...", SERVER.KAFKA.KAFKA_CLIENT_ID);
    try {
      this.kafkaClient = new Kafka({
        clientId: SERVER.KAFKA.KAFKA_CLIENT_ID,
        brokers: SERVER.KAFKA.KAFKA_BROKER_URLS, // replace with your Kafka broker addresses
        logLevel: logLevel.ERROR, // adjust log level as needed (ERROR, WARN, INFO, DEBUG)
        retry: {
          initialRetryTime: 3000,
          retries: 5,
        },
      });
    } catch (error: any) {
      logger.error(`Kafka error: ${error.message}`, {
        error: error?.message || "",
        stack: error?.stack || "",
      });
    }
  }

  public getClient(groupId?: string): {
    producer: (config?: ProducerConfig) => Producer;
    consumer: (config?: ConsumerConfig) => Consumer;
    admin: () => Admin;
    logger: () => Logger;
  } {
    if (!this.kafkaClient) {
      throw new Error("Kafka: Not connected. Call connect method first.");
    }
    return {
      producer: (config?: ProducerConfig) => this.kafkaClient.producer(config),
      consumer: (config?: ConsumerConfig) =>
        this.kafkaClient.consumer({
          groupId: groupId || "gptest1",
          sessionTimeout: 10000, // Adjust as needed (in milliseconds)
          ...config,
        }),
      admin: () => this.kafkaClient.admin(),
      logger: () => this.kafkaClient.logger(),
    };
  }
}

export default KafkaConnection;
