import mongoose from "mongoose";
import SERVER from "../config/environment";
import logger from "../utils/logger";

class MongoDBConnection {
  private static mongoInstance: MongoDBConnection;
  private db: mongoose.Connection | undefined;
  private retryCount: number = 0;
  private readonly maxRetries: number = 1;

  private constructor() {
    this.connect();
  }

  public static getInstance(): MongoDBConnection {
    if (!this.mongoInstance) {
      this.mongoInstance = new MongoDBConnection();
    }
    return this.mongoInstance;
  }

  private async connect(): Promise<void> {
    try {
      mongoose.set("strictQuery", false);
      await mongoose.connect(SERVER.MONGO.DB_URL || "", SERVER.MONGO.OPTIONS);
      mongoose.Promise = global.Promise;
      this.db = mongoose.connection;
      logger.info("Mongo database connected...");
      this.retryCount = 0; // Reset retry count on successful connection
    } catch (error: any) {
      this.retryCount++;
      logger.error(
        `Mongo database connection error. Retry ${this.retryCount} of ${this.maxRetries}.`,
        {
          error: error?.message || "",
          stack: error?.stack || "",
          voiceMessage: "Bhai mongo database down hai check karlo",
        }
      );

      if (this.retryCount < this.maxRetries) {
        // Retry after 5 seconds
        setTimeout(() => this.connect(), 2000);
      } else {
        logger.error(
          `Maximum retry limit reached (${this.maxRetries}). Exiting.`,
          { voiceMessage: "Bhai mongo database down hai check karlo" }
        );
        error.data = {
          voiceMessage: "Bhai mongo database down hai check karlo",
        };
        throw error; // Exit with a non-zero code indicating an error
      }
    }
  }

  public getDb(): mongoose.Connection {
    if (!this.db) {
      logger.error("MongoDB not connected. Call connect method first.");
      throw new Error("MongoDB not connected. Call connect method first.");
    }
    return this.db;
  }

  public async closeConnection(): Promise<void> {
    if (this.db) {
      await this.db.close();
      logger.info("MongoDB connection closed.");
    }
  }
}

export default MongoDBConnection;
