import { graphqlHTTP } from "express-graphql";
import depthLimit from "graphql-depth-limit";
import { schema } from "../graphql/shema";
import logger from "../utils/logger";
class GraphQl {
  private static graphQlInstance: any;
  private constructor(app: any) {
    this.connect(app);
  }
  public static getInstance(app: any) {
    if (!this.graphQlInstance) {
      this.graphQlInstance = new GraphQl(app);
    }
    return this.graphQlInstance;
  }
  private async connect(app: any): Promise<void> {
    try {
      app.use(
        "/graphql",
        graphqlHTTP({
          schema: schema,
          validationRules: [depthLimit(7)],
          graphiql: true,
        })
      );

      logger.info("GraphQl server started...");
    } catch (error: any) {
      logger.error("GraphQl server not started:", {
        error: error?.message || "",
        stack: error?.stack || "",
      });
      setTimeout(() => this.connect(app), 5000);
    }
  }
}
export default GraphQl;
