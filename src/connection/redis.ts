import { Redis } from "ioredis";
import SERVER from "../config/environment";
import logger from "../utils/logger";

class RedisConnection {
  private static redisInstance: RedisConnection;
  private client!: Redis;
  private retryCount: number = 0;
  private readonly maxRetries: number = 5;

  private constructor() {
    this.connect();
  }

  public static getInstance(): RedisConnection {
    if (!this.redisInstance) {
      this.redisInstance = new RedisConnection();
    }
    return this.redisInstance;
  }

  private connect(): void {
    logger.info("Redis connecting...");
    // Set maxRetriesPerRequest to null
    const options: any = {
      maxRetriesPerRequest: null,
    };
    this.client = new Redis(SERVER.REDIS.DB_URL, options);

    this.client.on("connect", () => {
      logger.info("Redis connected...");
      this.retryCount = 0; // Reset retry count on successful connection
    });

    this.client.on("error", (error: Error) => {
      this.retryCount++;
      logger.error(
        `Redis Error: ${error.message}. Retry ${this.retryCount} of ${this.maxRetries}.`,
        { error: error?.message || "", stack: error?.stack || "" }
      );

      if (this.retryCount < this.maxRetries) {
        // Retry after 5 seconds
        setTimeout(() => this.connect(), 5000);
      } else {
        logger.error(
          `Maximum retry limit reached (${this.maxRetries}). Exiting.`
        );
        throw error; // Exit with a non-zero code indicating an error
      }
    });
  }

  public getClient(serviceName: string): Redis {
    if (!this.client) {
      throw new Error(
        "Redis client not connected. Call connect method first." + serviceName
      );
    }
    return this.client;
  }

  public async closeConnection(): Promise<void> {
    if (this.client) {
      await this.client.quit();
      logger.info("Redis connection closed.");
    }
  }
}

export default RedisConnection;
