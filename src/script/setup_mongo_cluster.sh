#!/bin/bash

BASE_DIR="/Users/bilalzaidi/Desktop/sharding"
SHARD1_DIR="$BASE_DIR/shard1"
SHARD2_DIR="$BASE_DIR/shard2"
CONFIG_DIR="$BASE_DIR/config"

startMongoReplicaSet() {
  # Create directories for the replica set data
  mkdir -p "$SHARD1_DIR/db11" "$SHARD1_DIR/db12" "$SHARD1_DIR/db13"
  mkdir -p "$SHARD2_DIR/db21" "$SHARD2_DIR/db22" "$SHARD2_DIR/db23"
  mkdir -p "$CONFIG_DIR/config1" "$CONFIG_DIR/config2" "$CONFIG_DIR/config3"

  # Start the mongod instances for shard1
  sudo mongod --shardsvr --replSet shard1 --dbpath "$SHARD1_DIR/db11" --port 27127 &
  sudo mongod --shardsvr --replSet shard1 --dbpath "$SHARD1_DIR/db12" --port 27128 &
  sudo mongod --shardsvr --replSet shard1 --dbpath "$SHARD1_DIR/db13" --port 27129 &

  # Connect to one of the shard1 servers
  mongosh --port 27127

  # Inside the MongoDB shell, initiate the replica set for shard1
  mongosh --port 27127 <<EOF
rs.initiate({
  _id: "shard1",
  members: [
    { _id: 0, host: "localhost:27127" },
    { _id: 1, host: "localhost:27128" },
    { _id: 2, host: "localhost:27129" }
  ]
})
EOF

  # Start the mongod instances for shard2
  sudo mongod --shardsvr --replSet shard2 --dbpath "$SHARD2_DIR/db21" --port 27227 &
  sudo mongod --shardsvr --replSet shard2 --dbpath "$SHARD2_DIR/db22" --port 27228 &
  sudo mongod --shardsvr --replSet shard2 --dbpath "$SHARD2_DIR/db23" --port 27229 &

  # Connect to one of the shard2 servers
  mongosh --port 27227

  # Inside the MongoDB shell, initiate the replica set for shard2
  mongosh --port 27227 <<EOF
rs.initiate({
  _id: "shard2",
  members: [
    { _id: 0, host: "localhost:27227" },
    { _id: 1, host: "localhost:27228" },
    { _id: 2, host: "localhost:27229" }
  ]
})
EOF

  # Start the mongod instances for the config servers
  sudo mongod --dbpath "$CONFIG_DIR/config1" --replSet configReplica --port 30117 --configsvr &
  sudo mongod --dbpath "$CONFIG_DIR/config2" --replSet configReplica --port 30127 --configsvr &
  sudo mongod --dbpath "$CONFIG_DIR/config3" --replSet configReplica --port 30137 --configsvr &

  # Connect to one of the config servers
  mongosh --port 30117

  # Inside the MongoDB shell, initiate the replica set for config servers
  mongosh --port 30117 <<EOF
rs.initiate({
  _id: "configReplica",
  configsvr:true,
  members: [
    { _id: 0, host: "localhost:30117" },
    { _id: 1, host: "localhost:30127" },
    { _id: 2, host: "localhost:30137" }
  ]
})
EOF

  # Start mongos instance
  mongos --configdb configReplica/localhost:30117,localhost:30127,localhost:30137 &

  # Commands for connecting to MongoDB and adding shards
  echo "Connect to Studio 3T and execute the following commands:"

  mongo <<EOF
  sh.addShard('shard1/localhost:27127')
  sh.addShard('shard2/localhost:27227')
EOF
  

  # Connection strings
  echo "Connection strings:"
  echo "mongodb://localhost:30117,localhost:30127,localhost:30137/?replicaSet=configReplica&readPreference=primary"
  echo "mongodb://localhost:27127,localhost:27128,localhost:27129/?replicaSet=shard1&readPreference=primary"
  echo "mongodb://localhost:27227,localhost:27228,localhost:27229/?replicaSet=shard2&readPreference=primary"

  # Check running mongod processes for shard1
  ps -ef | grep mongod | grep shard1
}

stopMongoReplicaSet() {
  # Stop the mongod instances
  echo "Stopping MongoDB instances..."
  sudo pkill -f mongod
  sudo pkill -f mongos
  echo "MongoDB instances stopped."
}

case "$1" in
  "start") startMongoReplicaSet ;;
  "stop") stopMongoReplicaSet ;;
  *) echo "Usage: $0 {start|stop}" && exit 1 ;;
esac
