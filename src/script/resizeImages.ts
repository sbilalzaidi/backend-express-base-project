import * as fs from 'fs';
import * as https from 'https';
import gm from 'gm';
import * as pathUtil from 'path';
import * as util from 'util';
const resizeImage = util.promisify((width: number, height: number, originalPath: string, outputPath: string,callback: (err: Error | null) => void) => {
    gm(originalPath)
      .resize(width, height)
      .write(outputPath, callback);
  });
const downloadImage = (url: string, destPath: string,path:string): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      const file = fs.createWriteStream(`images/${path}/original/${destPath}.jpg`);
  
      https.get(url, (response) => {
        response.pipe(file);
        file.on('finish', () => {
          file.close();
          resolve();
        });
      }).on('error', (err) => {
        fs.unlink(`images/${path}/original/${destPath}.jpg`, () => reject(err));
      });
    });
  };

  const processImages = async (imageUrl:string,originalImagePath:string,path:string) => {
    try {
      // Download the original image
      await downloadImage(imageUrl, originalImagePath,path);
      // Resize images asynchronously using Promise.all
      await Promise.all([
        //resize('original.jpg','original2.jpg')
        resizeImage(250, 250, `./images/${path}/original/${originalImagePath}.jpg`, `./images/${path}/250/${originalImagePath}.jpg`),
        resizeImage(400, 400, `./images/${path}/original/${originalImagePath}.jpg`, `./images/${path}/400/${originalImagePath}.jpg`),
      ]);
      console.log(`Images resized and saved successfully.`);
    } catch (error) {
      console.error('Error:', error);
    }
  };
/**
 * images/catagories/original
 * images/catagories/250
 * images/catagories/400
 * images/sub-catagories/original
 * images/sub-catagories/250
 * images/sub-catagories/400
 * images/products/original
 * images/products/250
 * images/products/400
 */
const mkdirRecursive = async(dirPath:string) => {
  const parts = dirPath.split(pathUtil.sep);

  for (let i = 1; i <= parts.length; i++) {
    const currentPath = pathUtil.join(...parts.slice(0, i));
    if (!fs.existsSync(currentPath)) {
      fs.mkdirSync(currentPath);
    }
  }
};
const imageConvert = async () => {
    try {
        let directories:any=[
          'images/catagories/original',
          'images/catagories/250',
          'images/catagories/400',
          'images/sub-catagories/original',
          'images/sub-catagories/250',
          'images/sub-catagories/400',
          'images/products/original',
          'images/products/250',
          'images/products/400'
        ]
        for(let directory of directories){
          await mkdirRecursive(directory)
        }
        // Read the content of the file synchronously
        const filePath = 'data1.json';
        const data = fs.readFileSync(filePath, 'utf-8');
      
        // Parse the JSON data
        let categories:any = JSON.parse(data);
        const categoryIdsToFilter:any=[1,19094,19148,19118,19087,19107,
            19095,19169,19100,19103,19122,19112,19145,19184,19186,19192
        ];
        categories=categories.filter((category:any) => categoryIdsToFilter.includes(category.catalogue_id));
        // Now you can work with the loaded JSON data
        console.log('Loaded JSON data:', categories.length);
        if(Array.isArray(categories) && categories.length){
            for(let category of categories){
                if(category?.image_url)
                await processImages(category?.image_url,category?.catalogue_id,"catagories")
                let subCategories=category?.subCategory ||[];
                if(Array.isArray(subCategories) && subCategories.length){
                    for(let subCategory of subCategories){
                        if(subCategory?.image_url)
                        await processImages(subCategory?.image_url,subCategory?.catalogue_id,"sub-catagories")
                        let products=subCategory?.products ||[];
                        if(Array.isArray(products) && products.length){
                            for(let product of products){
                                if(product?.image_url)
                                await processImages(product?.image_url,product?.product_id,"products")
                            }
                        }
                    }
                }
            }
        }
        
      } catch (error) {
        console.error('Error:', error);
    }
};
export { imageConvert };
