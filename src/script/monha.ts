import logger from "../utils/logger";
import axios from "../services/axios";
import * as fs from 'fs';
import {delay} from "../lib/universal-function"
const getCategory = async () => {
  try {
    let body: any = {
      access_token: "6e6b6da7dd1816850f75998b97168426",
      user_id: 110986,
      user_type: 6,
      marketplace_user_id: 30074,
      subadmin_user_id: 110986,
    };
    const headers = {
      "content-type": "application/json",
    };
    let url = `https://api.monhapp.com/AdminCatalog/getMerchantCatalogueFourLevel`;
    let output= await axios.sendPOST(url, null, body, headers);
    return output?.data?.data || [];
  } catch (error: any) {
    logger.error("Error in get category", {
      error: error.message || error,
    });
    return false;
  }
};
const getSubCategory = async (parent_category_id: number) => {
  try {
    let body: any = {
      access_token: "6e6b6da7dd1816850f75998b97168426",
      user_id: 110986,
      user_type: 6,
      marketplace_user_id: 30074,
      parent_category_id: parent_category_id,
      subadmin_user_id: 110986,
    };
    const headers = {
      "content-type": "application/json",
    };
    let url = `https://api.monhapp.com/AdminCatalog/getMerchantCatalogueLevelWiseDashboard`;
    let output= await axios.sendPOST(url, null, body, headers);
    return output?.data?.data || [];
  } catch (error: any) {
    logger.error(`Error in get sub-category",${parent_category_id}`);
    return false;
  }
};
const getAdminProducts = async (catalogue_id: number) => {
  try {
    let body: any = {
      access_token: "6e6b6da7dd1816850f75998b97168426",
      user_id: 110986,
      catalogue_id: catalogue_id,
      marketplace_user_id: 30074,
      user_type: 6,
      subadmin_user_id: 110986,
    };
    const headers = {
      "content-type": "application/json",
    };
    let url = `https://api.monhapp.com/v1/AdminCatalog/getAdminProducts`;
    let output= await axios.sendPOST(url, null, body, headers);
    return output?.data?.data || [];
  } catch (error: any) {
    logger.error(`Error in get products",${catalogue_id}`);
    return false;
  }
};
const getMainCatalogueData = async () => {
  let finalData: any = [];
  let count=0
  let categories = await getCategory();
  if (Array.isArray(categories) && categories.length) {
    for (let category of categories) {
      let productsCategoriesData: any = [];
      let subCategoriesData: any = [];
      if (category?.has_products) {
        productsCategoriesData = await getAdminProducts(category?.catalogue_id);
        productsCategoriesData = productsCategoriesData.map((product: any) => {
          return {
            product_id: product?.product_id,
            name: product?.name,
            description: {
              ar: product?.long_description || "",
              en: product?.description,
            },
            cp: product?.cost_price || 0,
            sp: product?.price || 0,
            image_url: product?.image_url || "",
          };
        });
      } else {
        let subCategories = await getSubCategory(category?.catalogue_id);
        if (Array.isArray(subCategories) && subCategories.length) {
          for (let subCategory of subCategories) {
            let productsSubCategory: any = [];
            if (subCategory?.has_products) {
              productsSubCategory = await getAdminProducts(
                subCategory?.catalogue_id
              );
              productsSubCategory = productsSubCategory.map((product: any) => {
                return {
                  product_id: product?.product_id,
                  name: product?.name,
                  description: {
                    ar: product?.long_description || "",
                    en: product?.description,
                  },
                  cp: product?.cost_price || 0,
                  sp: product?.price || 0,
                  image_url: product?.image_url || "",
                };
              });
            } else {
              console.log("No subCategory", category?.name, subCategory?.name);
            }
            subCategoriesData.push({
              catalogue_id: category?.catalogue_id,
              name: category?.name || "",
              description: category?.description || "",
              image_url: category?.image_url || "",
              products: productsSubCategory || [],
            });
            delay(count);
            count=count+10;
          }
        }
      }
      finalData.push({
        catalogue_id: category?.catalogue_id,
        name: category?.name || "",
        description: category?.description || "",
        image_url: category?.image_url || "",
        products: productsCategoriesData,
        subCategory: subCategoriesData,
      });
      delay(count);
      count=count+10;
    }
  }
  return finalData;
};
const getCatalogueData = async () => {
    let finalData: any = [];
    let categories:any = await getCategory();
    const categoryIdsToFilter:any=[1,19094,19148,19118,19087,19107,
      19095,19169,19100,19103,19122,19112,19145,19184,19186,19192
  ];
  categories=categories.filter((category:any) => categoryIdsToFilter.includes(category.catalogue_id));
  
    let count=0
    if (Array.isArray(categories) && categories.length) {
      for (let category of categories) {
        let productsCategoriesData: any = [];
        let subCategoriesData: any = [];
        if (category?.has_products) {
          productsCategoriesData = await getAdminProducts(category?.catalogue_id);
          productsCategoriesData = productsCategoriesData.map((product: any) => {
            return {
              product_id: product?.product_id,
              name: product?.name,
              description: {
                ar: product?.long_description || "",
                en: product?.description,
              },
              cp: product?.cost_price || 0,
              sp: product?.price || 0,
              image_url: product?.image_url || "",
              is_enabled:product?.is_enabled
            };
          });
          subCategoriesData.push(JSON.parse(JSON.stringify({
            catalogue_id: category?.catalogue_id,
            isNoSubCategory:1,
            name: category?.name || "",
            description: category?.description || "",
            image_url: category?.image_url || "",
            is_enabled:category?.is_enabled,
            products: productsCategoriesData || [],
          })));
          delay(count);
          count=count+10;
        } else {
          let subCategories = await getSubCategory(category?.catalogue_id);
          if (Array.isArray(subCategories) && subCategories.length) {
            for (let subCategory of subCategories) {
              let productsSubCategory: any = [];
              if (subCategory?.has_products) {
                productsSubCategory = await getAdminProducts(
                  subCategory?.catalogue_id
                );
                productsSubCategory = productsSubCategory.map((product: any) => {
                  return {
                    product_id: product?.product_id,
                    name: product?.name,
                    description: {
                      ar: product?.long_description || "",
                      en: product?.description,
                    },
                    cp: product?.cost_price || 0,
                    sp: product?.price || 0,
                    image_url: product?.image_url || "",
                    is_enabled:product?.is_enabled
                  };
                });
              } else {
                console.log("No subCategory", category?.name, subCategory?.name);
              }
              subCategoriesData.push(JSON.parse(JSON.stringify({
                catalogue_id: subCategory?.catalogue_id,
                isNoSubCategory:0,
                name: subCategory?.name || "",
                description: subCategory?.description || "",
                image_url: subCategory?.image_url || "",
                is_enabled:subCategory?.is_enabled,
                products: productsSubCategory || [],
              })));
              delay(count);
              count=count+10;
            }
          }
        }
        finalData.push(JSON.parse(JSON.stringify({
          catalogue_id: category?.catalogue_id,
          name: category?.name || "",
          description: category?.description || "",
          image_url: category?.image_url || "",
          is_enabled:category?.is_enabled,
          subCategory: subCategoriesData,
        })));
      }
    }
    return finalData;
};
const initFinalData=async()=>{
    let catalogues=await getCatalogueData();
    console.log("Write data data1 start")
    const jsonString = JSON.stringify(catalogues, null, 2); // The '2' argument adds indentation for better readability
    // Specify the file path
    const filePath = 'data1.json';
    // Write the JSON data to the file
    fs.writeFileSync(filePath, jsonString, 'utf-8');
    console.log("Write data data1")
    // let cataloguesMain=await getMainCatalogueData();
    // const jsonMainString = JSON.stringify(cataloguesMain, null, 2); // The '2' argument adds indentation for better readability
    // // Specify the file path
    // const filePath2 = 'data2.json';
    // // Write the JSON data to the file
    // fs.writeFileSync(filePath2, jsonMainString, 'utf-8');
}
export { initFinalData };
