#!/bin/bash

# Start ZooKeeper servers
start_servers() {
  echo 1 >> /tmp/zookeeper-1/myid  
  cd /Users/bilalzaidi/Desktop/apache-zookeeper/zkNode1
  bin/zkServer.sh start

  echo 2 >> /tmp/zookeeper-2/myid  
  cd /Users/bilalzaidi/Desktop/apache-zookeeper/zkNode2
  bin/zkServer.sh start

  echo 3 >> /tmp/zookeeper-3/myid  
  cd /Users/bilalzaidi/Desktop/apache-zookeeper/zkNode3
  bin/zkServer.sh start
}

# Stop ZooKeeper servers
stop_servers() {
  cd /Users/bilalzaidi/Desktop/apache-zookeeper/zkNode1
  bin/zkServer.sh stop

  cd /Users/bilalzaidi/Desktop/apache-zookeeper/zkNode2
  bin/zkServer.sh stop

  cd /Users/bilalzaidi/Desktop/apache-zookeeper/zkNode3
  bin/zkServer.sh stop
}

# Check the argument provided and call the corresponding function
if [ "$1" == "start" ]; then
  start_servers
elif [ "$1" == "stop" ]; then
  stop_servers
else
  echo "Usage: $0 {start|stop}"
  exit 1
fi
