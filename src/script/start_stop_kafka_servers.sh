#!/bin/bash

# Start Kafka servers
start_servers() {
  cd /Users/bilalzaidi/Desktop/kafka/kfNode1
  bin/kafka-server-start.sh -daemon config/server.properties 

  cd /Users/bilalzaidi/Desktop/kafka/kfNode2
  bin/kafka-server-start.sh -daemon config/server.properties 

  cd /Users/bilalzaidi/Desktop/kafka/kfNode3
  bin/kafka-server-start.sh -daemon config/server.properties 
}

# Stop Kafka servers
stop_servers() {
  cd /Users/bilalzaidi/Desktop/kafka/kfNode1
  bin/kafka-server-stop.sh config/server.properties 

  cd /Users/bilalzaidi/Desktop/kafka/kfNode2
  bin/kafka-server-stop.sh config/server.properties 

  cd /Users/bilalzaidi/Desktop/kafka/kfNode3
  bin/kafka-server-stop.sh config/server.properties 
}

# Check the argument provided and call the corresponding function
if [ "$1" == "start" ]; then
  start_servers
elif [ "$1" == "stop" ]; then
  stop_servers
else
  echo "Usage: $0 {start|stop}"
  exit 1
fi
