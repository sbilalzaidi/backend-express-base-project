/** Example 
 * interface Person {name: string;age: number;}
 */

/**
 * Readonly Properties
 * type ReadonlyPerson = Readonly<Person>;
 * ReadonlyPerson is { readonly name: string; readonly age: number; }
 */
type Readonly<T> = {
    readonly [P in keyof T]: T[P];
};

/**
 * Optional Properties
 * type PartialPersion = Partial<Person>;
 * PartialPersion is { name?: string; age?: string; }
 */

type Partial<T> = {
    [P in keyof T]?: T[P];
};

/**
 * Nullable Properties
 * type NullablePersion = Nullable<Person>;
 * NullablePersion is { name: string | null; age: number | null;}
 */

type Nullable<T> = {
    [P in keyof T]: T[P] | null;
};
/**
 * Mapped Type with Conditional Types
 * type ConditionalPersion = ConditionalProps<Person>;
 * ConditionalPersion is { name: 'other'; age: 'number';}
 */

type ConditionalProps<T> = {
    [P in keyof T]: T[P] extends number ? 'number' : 'other';
};