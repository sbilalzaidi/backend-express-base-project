
// Model Type For DAO manager
declare type ModelNames =
"AdminNotifications" | 
"Admins" | 
"AdminSessions" | 
"CashBacks" | 
"PaymentCards" | 
"Catalogues" | 
"Users" |
"Sessions" | 
"Roles" | 
"Permissions" | 
"PromoCodes" | 
"Regions" | 
"Modules" | 
"Orders" | 
"Settings" |
"Transactions" |
"UserNotifications"