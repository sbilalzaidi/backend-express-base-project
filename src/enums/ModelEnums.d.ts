const enum Models {
	Admin_NOTIFICATIONS="AdminNotifications",
	ADMIN="Admins",
	ADMIN_SESSIONS="AdminSessions",
	CASHBACK="CashBacks",
	PAYMENT_CARDS="PaymentCards",
	CATALOGUES="Catalogues",
	USERS="Users",
	USER_SESSIONS="Sessions",
	ROLES="Roles",
	PERMISSIONS="Permissions",
	PROMO_CODES="PromoCodes",
	REGIONS="Regions",
	MODULES="Modules",
	ORDERS="Orders",
	SETTINGS="Settings",
	TRANSACTIONS="Transactions",
	USER_NOTIFICATIONS="UserNotifications"
}
