import axios, { AxiosResponse } from 'axios';
import {CIRCUIT_BREAKER_STATES} from "../enums/enums"
import {setDataWithExpire,getData} from "../services/redis"
import SERVER from '../config/environment';
import logger from '../utils/logger';
import codes from '../codes/status_codes'
/**
 * // sample request to fetch data asynchronously
const request = axios fetchDataFromExternalVendor();
// wrap the request within a circuit breaker object
const circuitBreakerObject = new CircuitBreaker(request, { failureThreshold: 4, timeout: 4000 });
// fire the request
circuitBreakerObject.fire()
  .then((data) => console.log(data))
  .catch((err) => console.log(some error occurred = ${err.message}`);
 */


class CircuitBreaker {
  private request: any;
  private state: string;
  private failureCount: number;
  private failureThreshold: number;
  private resetAfter: number;
  private timeout: number;

  constructor(request: any, options?: { failureThreshold?: number; timeout?: number }) {
    this.request = request;
    this.state = CIRCUIT_BREAKER_STATES.CLOSED;
    this.failureCount = 0;
    this.resetAfter = Date.now();
    this.timeout = 5000;

    if (options) {
      this.failureThreshold = options.failureThreshold || 5;
      this.timeout = options.timeout || 5000;
    } else {
      this.failureThreshold = 5;
    }
  }

  private async getStateFromRedis(): Promise<string | null> {
    return await getData(SERVER.CIRCUIT_BREAKER_KEY);
  }

  private async setStateInRedis(state: string): Promise<void> {
     await setDataWithExpire(SERVER.CIRCUIT_BREAKER_KEY,state,this.timeout / 1000);
  }

  async fire(): Promise<any> {
    const currentState = await this.getStateFromRedis();

    if (currentState === CIRCUIT_BREAKER_STATES.OPENED) {
      if (this.resetAfter <= Date.now()) {
        this.state = CIRCUIT_BREAKER_STATES.HALF;
        await this.setStateInRedis(CIRCUIT_BREAKER_STATES.HALF);
      } else {
        logger.error('Circuit is in open state right now. Please try again later.');
        throw new Error('Circuit is in open state right now. Please try again later.');
      }
    }

    try {
      const response = await axios(this.request);
      if (response.status === codes.SUCCESS) return this.success(response.data);
      return this.failure(response.data);
    } catch (err:any) {
      return this.failure(err.message);
    }
  }

  private async success(data: any): Promise<any> {
    this.failureCount = 0;

    if (this.state === CIRCUIT_BREAKER_STATES.HALF) {
      this.state = CIRCUIT_BREAKER_STATES.CLOSED;
      await this.setStateInRedis(CIRCUIT_BREAKER_STATES.CLOSED);
    }

    return data;
  }

  private async failure(data: any): Promise<any> {
    this.failureCount += 1;

    if (this.state === CIRCUIT_BREAKER_STATES.HALF || this.failureCount >= this.failureThreshold) {
      this.state = CIRCUIT_BREAKER_STATES.OPENED;
      this.resetAfter = Date.now() + this.timeout;

      await this.setStateInRedis(CIRCUIT_BREAKER_STATES.OPENED);
    }

    return data;
  }
}

export default CircuitBreaker;
