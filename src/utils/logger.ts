import { createLogger, format, transports ,Logger} from "winston";
import  *  as  winston  from  'winston';
let { combine, timestamp, label, printf } = format;
import  'winston-daily-rotate-file';
import CONSTANT from "../constant/constant";

class CustomLogger {
  private logger: Logger;

  constructor() {
    const myFormat = printf(({ level, message, label, timestamp,...data }) => {
      if(data && Object.keys(data).length)
      return `${label}:${level}:${timestamp}:${message}:`+JSON.stringify(data);
      else
      return `${timestamp} [${label}] ${level}:${message}`;
    });

    const dailyTransportOpts = {
      filename: "./logs/application-%DATE%.log",
      datePattern: "YYYY-MM-DD-HH",
      zippedArchive: true,
      maxSize: "1k", //20m
      maxFiles: "14d",
    };

    let transportArray = [];

    if (CONSTANT.LOGGER.ADD_ERROR_FILE) {
      transportArray.push(
        new transports.File({
          filename: "./logs/error.log",
          level: "error",
          format: format.combine(
            format.json()
          ),
        })
      );
    }

    if (CONSTANT.LOGGER.ADD_INFO_FILE) {
      transportArray.push(
        new transports.File({
          filename: "./logs/info.log",
          format: format.combine(
            format.json()
          ),
        })
      );
    }

    if (CONSTANT.LOGGER.CONSOLE_ON) {
      transportArray.push(new transports.Console());
    }

    if (CONSTANT.LOGGER.DAILY_ROTATE_LOG_ON) {
      transportArray.push(new winston.transports.DailyRotateFile(dailyTransportOpts));
    }

    this.logger = createLogger({
      format: combine(
        label({ label: "Grocery Backend:" + process.env.NODE_ENV }),
        timestamp(),
        myFormat,
      ),
      transports: transportArray,
    });
  }

  public getLogger(): Logger {
    return this.logger;
  }
}

const customLogger = new CustomLogger();
const logger = customLogger.getLogger();

export default logger;
