import codes from "../codes/status_codes";
export class CustomError extends Error {
  code: number;
  data: any;
  message:string;
  constructor(message: string, metaData: ICustomError = {}) {
    super();
    // Use the provided code or default to 500 (Internal Server Error)
    this.code = metaData?.code || codes.BAD_REQUEST;
    // Attach additional data to the error (if any)
    this.data = metaData.data;
    this.message=message || '';
    // Preserve the stack trace
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, CustomError);
    }
  }

  toJSON() {
    // Convert the error to a JSON-friendly object
    return {
      code: this.code,
      message: this.message,
      data: this.data,
    };
  }
}
