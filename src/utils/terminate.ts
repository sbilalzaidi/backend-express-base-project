import { Server } from "http";
import customEventEmitter from '../modules/v1/common/dao/eventEmitter';
import logger from "../utils/logger";
import connectMongoDB from "../connection/mongoose";
import connectRedisDB from "../connection/redis";
import bullMQ from "../modules/v1/common/dao/bull";
import { delay } from "../lib/universal-function";
const eventEmitter=customEventEmitter.getInstance()
type ExitCallback = (code?: number) => void;
class Terminator {
  private server: Server;
  private options: TerminateOptions;

  constructor(
    server: Server,
    options: TerminateOptions = { coredump: false, timeout: 500 }
  ) {
    this.server = server;
    this.options = options;
  }

  private exit: ExitCallback = (code) => {
    connectMongoDB.getInstance().closeConnection();
    connectRedisDB.getInstance().closeConnection();
    bullMQ.getInstance().handleGracefulTermination();
    this.options.coredump ? process.abort() : process.exit(code);
  };

  public terminate(code: number, reason: string) {
    return async(err: ICustomErrorMessage, promise: Promise<any>) => {
      if (err) {
        // Log error information, use a proper logging library here :)
        logger.error("Terminator catch error:", {
          message: err.message,
          stack: err.stack,
          reason: reason,
        });
      }
      //  voice handling
      if (err?.data?.voiceMessage) {
        eventEmitter.emit("voicemessage",{voiceMessage:err?.data?.voiceMessage})
      }
       await delay(20000);
      // Attempt a graceful shutdown
     this.server.close(() => this.exit(code));
     setTimeout(() => this.exit(code), this.options.timeout).unref();
    };
  }
}

export default Terminator;
