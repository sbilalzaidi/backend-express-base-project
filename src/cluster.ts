import cluster from 'cluster';
import os from 'os';
import logger from "./utils/logger";
/**
 * 
 * @param workerFunction startCluster(()=>{ exclude express app instance all are inside it})
 */
export function startCluster(workerFunction:any) {
  if (cluster.isMaster) {
    // If it's the master process, fork workers
    const numCPUs = os.cpus().length;
    logger.info("numCPUs:",numCPUs)
    for (let i = 0; i < numCPUs; i++) {
      cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        logger.info(`Worker ${worker.process.pid} died`);
      // You can fork a new worker here if needed
      cluster.fork();
    });
  } else {
    // Worker processes run the provided worker function
    workerFunction();
  }
}
