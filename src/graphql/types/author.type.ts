import {
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLString,
    GraphQLInt
} from 'graphql';


class Author {

    public name = "Author";
    public description = "This represent an author";

    public fields = function () {

        return {

            id: {
                type: new GraphQLNonNull(GraphQLInt),
            },
            name: {
                type: GraphQLString,
                resolve: function (author: any) {
                    return author.firstName + " " + author.lastName
                }
            },
            firstName: {
                type: GraphQLString
            },
            lastName: {
                type: GraphQLString
            }
        }

    }

}

export const AuthorType = new GraphQLObjectType(new Author());