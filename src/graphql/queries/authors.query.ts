import {
    GraphQLType,
    GraphQLList,
} from 'graphql';

import { GraphQLQuery } from './abstract.query'
import { AuthorType } from '../types'


export class AuthorsQuery implements GraphQLQuery {

    public type = new GraphQLList(AuthorType);

    public description:string= "List of all Authors";

    public resolve = async function () {

        return [{
            id:1,
            firstName:"bilal",
            lastName:"zaidi"
        }]

    }    

}