import {
    GraphQLType,
    GraphQLList,
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLString
} from 'graphql';

import { AuthorType } from '../types'


export interface GraphQLMutation {
    type: GraphQLType;
    description: string;
    resolve: Function;
}

interface Arguments {
    firstName: string
    lastName: string
}

export class SaveAuthorMutation implements GraphQLMutation {

    public type = AuthorType;
    public description:string= "Authors";

    public args = {
        firstName: {
            type: new GraphQLNonNull(GraphQLString)
        },
        lastName: {
            type: new GraphQLNonNull(GraphQLString)
        }
    };

    public resolve = async function (root:any, args: Arguments|any) {

        return {}

    }

}