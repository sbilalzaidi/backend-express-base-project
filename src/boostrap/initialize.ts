import {  createAdmin } from "../modules/v1/admins/services/admins";
import { adminDao } from "../modules/v1/admins/dao/admins";
import logger from "../utils/logger";
import { baseDao } from "../modules/v1/common/dao/baseDao";
import {admin,stores,catagories,subCategory,products,regions } from "./data"
const delay = (ms: number) => new Promise((res) => setTimeout(res, ms));
const createAdminUser=async()=> {
  try {
    let isAdminExist=await adminDao.checkAdminExists({email:admin.email,phoneNo:admin.phoneNo})
    if(!isAdminExist){
        const doc = await createAdmin(admin);
        await doc.setPassword(admin.password);
        await doc.save();
        await baseDao.insertMany(Models.ADMIN, stores,{}); 
        await baseDao.insertMany(Models.CATALOGUES, catagories,{}); 
        await baseDao.insertMany(Models.CATALOGUES, subCategory,{});
        await baseDao.insertMany(Models.CATALOGUES, products,{}); 
        await baseDao.insertMany(Models.REGIONS, regions,{}); 
        console.log('Admin user created successfully.');
    }else {
        console.log('Admin already exists.');
    }
  } catch (error) {
    console.log(JSON.stringify(error))
    logger.warn("Error creating admin user.", JSON.stringify(error));
  }
}

export {
    createAdminUser
}