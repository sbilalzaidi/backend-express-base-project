import * as mongoose from "mongoose";
export const admin: any = {
  _id: new mongoose.Types.ObjectId("6594006442b0113b274b9143"),
  firstName: "demo",
  lastName: "demo",
  email: "demo@yopmail.com",
  password: "demo@sss",
  phoneNo: "7088124456",
  countryCode: "+91",
  isEmailVerified:1,
  isPhoneNoVerified:1,
  profilePic: {
    original:
      "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
    thumbNail:
      "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
  },
  addresses: [
    {
      addressType: "Home",
      country: "India",
      state: "U.P",
      city: "Aligarh",
      address: "Aligarh x",
      latitude: 0,
      longitude: 0,
      isDefault: true,
    },
  ],
  adminType: 1,
  status: 2,
};

export const stores: any = [
  {
    _id: new mongoose.Types.ObjectId("6594006542b0113b274b9171"),
    firstName: "store1",
    lastName: "store1",
    email: "store1@yopmail.com",
    password: "store1@123",
    phoneNo: "7088124457",
    countryCode: "+91",
    profilePic: {
      original:
        "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
      thumbNail:
        "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
    },
    addresses: [
      {
        addressType: "Home",
        country: "India",
        state: "U.P",
        city: "Aligarh",
        address: "Aligarh x",
        latitude: 0,
        longitude: 0,
        isDefault: true,
      },
    ],
    store: {
      regionIds: [
        new mongoose.Types.ObjectId("6594006542b0113b274b9171"),
        new mongoose.Types.ObjectId("6594006542b0113b274b9172"),
      ],
      shopName: { name: "Fake Shop Name 1" },
      country: "India",
      state: "UP",
      city: "ALigarh",
      address: "AMU hospital",
      latitude: 40.7128, // Fake latitude value
      longitude: -74.006, // Fake longitude value
      icons: ["icon1.jpg", "icon2.jpg"],
      images: ["image1.jpg", "image2.jpg"],
      banners: ["banner1.jpg", "banner2.jpg"],
      enableRegions: 2, // Replace with appropriate enum value
      serviceRadius: 10, // Fake service radius value
      enableServiceRadius: 2, // Replace with appropriate enum value
      location: {
        type: "Point",
        coordinates: [27.91772462397669, 78.09088024667028], // Fake coordinates
      },
      autoApprovedEnabled: 3, // Replace with appropriate enum value
      enableInventory: 2, // Replace with appropriate enum value
      enableGoogleMerchantCenter: 3, // Replace with appropriate enum value
      enableFacebookMerchantCenter: 3, // Replace with appropriate enum value
      enableShift: 2, // Replace with appropriate enum value
      shift: [
        {
          day: 1,
          open: 8,
          closed: 17,
        },
        {
          day: 2,
          open: 9,
          closed: 18,
        },
      ],
      ranking: 1, // Fake ranking value
      rating: 4.5, // Fake rating value
    },
    adminType: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("6594006442b0113b274b9161"),
    firstName: "store2",
    lastName: "store2",
    email: "store2@yopmail.com",
    password: "store2@123",
    phoneNo: "7088124457",
    countryCode: "+91",
    profilePic: {
      original:
        "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
      thumbNail:
        "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2",
    },
    addresses: [
      {
        addressType: "Home",
        country: "India",
        state: "U.P",
        city: "Aligarh",
        address: "Aligarh x",
        latitude: 0,
        longitude: 0,
        isDefault: true,
      },
    ],
    store: {
      regionIds: [new mongoose.Types.ObjectId("6594006542b0113b274b9171")],
      shopName: { name: "Fake Shop Name 2" },
      country: "India",
      state: "UP",
      city: "ALigarh",
      address: "Naqvi park",
      latitude: 40.7128, // Fake latitude value
      longitude: -74.006, // Fake longitude value
      icons: ["icon1.jpg", "icon2.jpg"],
      images: ["image1.jpg", "image2.jpg"],
      banners: ["banner1.jpg", "banner2.jpg"],
      enableRegions: 2, // Replace with appropriate enum value
      serviceRadius: 1, // Fake service radius value
      enableServiceRadius: 2, // Replace with appropriate enum value
      location: {
        type: "Point",
        coordinates: [27.902950581379702, 78.07344496247968], // Fake coordinates
      },
      autoApprovedEnabled: 3, // Replace with appropriate enum value
      enableInventory: 2, // Replace with appropriate enum value
      enableGoogleMerchantCenter: 3, // Replace with appropriate enum value
      enableFacebookMerchantCenter: 3, // Replace with appropriate enum value
      enableShift: 2, // Replace with appropriate enum value
      shift: [
        {
          day: 1,
          open: 8,
          closed: 17,
        },
        {
          day: 2,
          open: 9,
          closed: 18,
        },
      ],
      ranking: 1, // Fake ranking value
      rating: 4.5, // Fake rating value
    },
    adminType: 1,
    status: 2,
  },
];

export const catagories = [
  {
    _id: new mongoose.Types.ObjectId("6594006442b0113b274b9165"),
    name: {
      en: "Break fast",
      ar: "Break fast",
    },
    description: {
      en: "Break fast",
      ar: "Break fast",
    },
    catalogueType: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("6594006442b0113b274b9166"),
    name: {
      en: "Dairy product",
      ar: "Dairy product",
    },
    description: {
      en: "Dairy product",
      ar: "Dairy product",
    },
    catalogueType: 1,
    status: 2,
  },
];

export const subCategory = [
  {
    _id: new mongoose.Types.ObjectId("6594006442b0113b274b916a"),
    parentId: new mongoose.Types.ObjectId("6594006442b0113b274b9165"),
    name: {
      en: "Break fast sub ",
      ar: "Break fast sub",
    },
    description: {
      en: "Break fast sub",
      ar: "Break fast sub",
    },
    catalogueType: 1,
    status: 2,
  },
];
export const products = [
  {
    catalogueNo: 1,
    parentId: new mongoose.Types.ObjectId("6594006442b0113b274b916a"),
    regionIds: [
      new mongoose.Types.ObjectId("6594006542b0113b274b9171"),
      new mongoose.Types.ObjectId("6594006542b0113b274b9172"),
    ],
    name: {
      en: "Break fast",
      ar: "Break fast",
    },
    description: {
      en: "Break fast",
      ar: "Break fast",
    },
    catalogueType: 1,
    variants: [
      {
        _id: new mongoose.Types.ObjectId("6594006542b0113b274b9171"),
        regionIds: [
          new mongoose.Types.ObjectId("6594006542b0113b274b9171"),
          new mongoose.Types.ObjectId("6594006542b0113b274b9172"),
        ],
        adminId: new mongoose.Types.ObjectId("6594006542b0113b274b9171"),
        variantNo: 2,
        enableRegion: 2,
        quantity: 3,
      },
    ],
    status: 3,
  },
  {
    catalogueNo: 3,
    parentId: new mongoose.Types.ObjectId("6594006442b0113b274b9166"),
    regionIds: [
      new mongoose.Types.ObjectId("6594006542b0113b274b9171"),
      new mongoose.Types.ObjectId("6594006542b0113b274b9172"),
    ],
    name: {
      en: "Break fast",
      ar: "Break fast",
    },
    description: {
      en: "Break fast",
      ar: "Break fast",
    },
    catalogueType: 1,
    variants: [
      {
        _id: new mongoose.Types.ObjectId("6594006542b0113b274b9172"),
        regionIds: [new mongoose.Types.ObjectId("6594006542b0113b274b9171")],
        adminId: new mongoose.Types.ObjectId("6594006442b0113b274b9161"),
        variantNo: 4,
        enableRegion: 2,
        quantity: 4,
      },
    ],
    status: 3,
  },
];
export const regions = [
  {
    _id: new mongoose.Types.ObjectId("6594006542b0113b274b9171"),
    name: "aligarh nadeem nagla malha",
    discription: "aligarh nadeem nagla malha",
    boundaries: {
      type: "Polygon",
      coordinates: [
        [27.916161492269744, 78.0887043593384],
        [27.90846316589529, 78.08531404714357],
        [27.904670685458708, 78.08454157094728],
        [27.9064152429719, 78.0948841689087],
        [27.91456878004609, 78.09651495198976],
        [27.91858843721763, 78.09501291494142],
        [27.916161492269744, 78.0887043593384],
      ],
    },
  },
  {
    _id: new mongoose.Types.ObjectId("6594006542b0113b274b9172"),
    name: "zakaullah rb sibli rd amu aligarh public school",
    discription: "zakaullah rb sibli rd amu aligarh public school",
    boundaries: {
      type: "Polygon",
      coordinates: [
        [27.91531595934877, 78.07443500737917],
        [27.918728861808543, 78.08301807622682],
        [27.90261143140676, 78.0845630286194],
        [27.902535578998318, 78.07507873754274],
        [27.91531595934877, 78.07443500737917],
      ],
    },
  },
];
export const modules: any = [
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a99a"),
    name: "Dashboard",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a99b"),
    name: "Admin",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a99c"),
    name: "Admin Users",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a99d"),
    name: "Company",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a99e"),
    name: "Company users",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a99f"),
    name: "Roles",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a0"),
    name: "Modules",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a1"),
    name: "Permissions",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a2"),
    name: "Regions",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a3"),
    name: "Settings",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a4"),
    name: "Transactions",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a5"),
    name: "Users",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a6"),
    name: "Catalogues",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a7"),
    name: "Variants",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a8"),
    name: "Payment Cards",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a9"),
    name: "Orders",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9aa"),
    name: "Transactions",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9ab"),
    name: "Promo Codes",
    add: true,
    edit: true,
    view: true,
    delete: true,
    visibilityFor: 1,
    status: 2,
  },
];

export const permissions: any = [
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d85"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a99a"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d86"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a99b"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d87"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a99c"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d88"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a99d"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d89"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a99e"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d8a"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a99f"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d8b"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a0"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d8c"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a1"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d8d"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a2"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d8e"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a3"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d8f"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a4"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d90"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a5"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d91"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a6"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d92"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a7"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d93"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a8"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d94"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9a9"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d95"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9aa"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
  {
    _id: new mongoose.Types.ObjectId("66091fdba487461b483c1d96"),
    moduleId: new mongoose.Types.ObjectId("66091c6bdb025d70b440a9ab"),
    addApis: [],
    editApis: [],
    viewApis: [],
    deleteApis: [],
    status: 2,
  },
];
