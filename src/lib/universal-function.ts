import { Request, Response } from "express";
import * as jwt from 'jsonwebtoken'
import bcrypt from "bcryptjs";
import * as fs from "fs";
import path from "path";
import crypto from "crypto";
import { compile } from "handlebars";
import SERVER from "../config/environment";
import CONSTANT from "../constant/constant";
import MESSAGES from "../messages/messages";
import codes from "../codes/status_codes";
import {CustomError} from "../utils/customError"
import { loadLanguage } from '../langs/index';
import { createCanvas, registerFont }  from 'canvas';
const sendResponse = async (
  req: any,
  res: any,
  code?: number,
  message?: string | any,
  data?: any,
  messageData?:any
) => {
  const lang:string = req.headers?.["content-language"] || CONSTANT.LANGUAGE_TYPE.ENGLISH;
  const language = await loadLanguage(lang);
  message=language?.[message] ||'';
  if(messageData && message)
  message=renderMessage(message,messageData)
  return res.status(200).send({
    statusCode: code || codes.SUCCESS,
    message: message,
    data: data || {},
  });
};
const sendErrorResponse = async (
  req: any,
  res: any,
  error: ICustomErrorMessage
) => {
  let { code= codes.FORBIDDEN,data={},message=''} =error;
  const lang:string = req.headers?.["content-language"] || CONSTANT.LANGUAGE_TYPE.ENGLISH;
  const language = await loadLanguage(lang);
  let errorMessage:string=language?.[message] ||'';
  if(data && errorMessage)
  errorMessage=renderMessage(errorMessage,data)
  return res.status(code).send({
    statusCode:code, 
    error: errorMessage
  });
};

const validateSchema = async (inputs: any, schema: any) => {
  try {
    const { error, value } = schema.validate(inputs);
    if (error) throw error.details ? error.details[0].message : "";
    else return false;
  } catch (error) {
    throw error;
  }
};

const renderMessage =  (templateData: any, variablesData: any) => {
  return compile(templateData)(variablesData);
};
const generateNumber = async () => {
  return Math.floor(1000 + Math.random() * 9000).toString();
};
const filterSpacesFromArray = async (arr: any[]) => {
  return arr.filter((item) => {
    return item && /\S/.test(item);
  });
};
const removeNewLineCharacters = async (value: string) => {
  if (!value) {
    return value;
  }
  return value.toString().replace(/\n|\r/g, "");
};
const dynamicPrecision = async (value: string, precision: number) => {
  precision = precision || 2;
  let float_val = parseFloat(value);
  if (isNaN(float_val)) {
    return value;
  }
  return +float_val.toFixed(precision);
};
const convertArrayToObject = async (array: []) => {
  let arrayLength = array.length;
  let returnObj = {};
  for (let count = 0; count < arrayLength; count++) {
    returnObj[array[count]] = array[count];
  }
  return returnObj;
};
const addingSecurePrefixToURL = async (domainURL: string) => {
  if (domainURL.indexOf("https://") < 0 && domainURL.indexOf("http://") < 0) {
    domainURL = "https://" + domainURL;
  }
  return domainURL;
};
const delay = (ms: number) => new Promise((res) => setTimeout(res, ms));

const jwtSign = async (payload: any) => {
  try {
    return jwt.sign(payload, SERVER.JWT.SECRET || "", {
      expiresIn:SERVER.JWT.EXPIRESIN?(parseInt(SERVER.JWT.EXPIRESIN)* 1000):0 ,
    });
  } catch (error) {
    throw new CustomError(MESSAGES.INVALID_SECRET_KEY,{code:codes.BAD_REQUEST})
  }
};

const jwtVerify = async (token: string) => {
  try {
    return jwt.verify(token, SERVER.JWT.SECRET || "");
  } catch (error) {
    throw new CustomError(MESSAGES.UNAUTHORIZED,{code:codes.UNAUTHORIZED})
  }
};
const hashPasswordUsingBcrypt = async (plainTextPassword: string) => {
  return bcrypt.hashSync(plainTextPassword, 10);
};

const comparePasswordUsingBcrypt = async (pass: string, hash: string) => {
  return bcrypt.compareSync(pass, hash);
};
const generateComplexPassword=async(n:number) =>{
  const uppercaseChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const lowercaseChars = 'abcdefghijklmnopqrstuvwxyz';
  const digits = '0123456789';
  const specialChars = '!@#$%^&*()_+-=[]{}|;:,.<>?';
  let password:any = '';
  // First character is a capital letter
  password += uppercaseChars[Math.floor(Math.random() * uppercaseChars.length)];
  // At least one digit
  password += digits[Math.floor(Math.random() * digits.length)];
  // Generate the remaining characters
  for (let i = 0; i < n - 2; i++) {
    const randomType = Math.floor(Math.random() * 3);
    switch (randomType) {
      case 0: // lowercase
        password += lowercaseChars[Math.floor(Math.random() * lowercaseChars.length)];
        break;
      case 1: // uppercase
        password += uppercaseChars[Math.floor(Math.random() * uppercaseChars.length)];
        break;
      case 2: // special characters
        password += specialChars[Math.floor(Math.random() * specialChars.length)];
        break;
    }
  }
  // Shuffle the password to make sure the first two characters are not predictable
  password = password.split('');
  for (let i = password.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [password[i], password[j]] = [password[j], password[i]];
  }
  return password.join('');
}

const readFile = async (filePath: string) => {
  return fs.readFileSync(
    path.join(process.cwd()) + "/dist/views/" + filePath,
    "utf8"
  );
};
const randomString = async (len?: number) => {
  len = len || 20;
  return crypto.randomBytes(20).toString("hex") + "-" + new Date().getTime();
};
const dateDifference = async (startDate: Date, endDate: Date) => {
  return endDate.getTime() - startDate.getTime();
};
const isEmpty = (value:any )=>{
  return (value === undefined || value === null  || Number.isNaN(value) || 
  (typeof value === 'object' && Object.keys(value).length === 0) || 
  (typeof value === 'string' && (value.trim()).length === 0));
}
const isJson = async (params: any) => {
  try {
    return JSON.parse(params);
  } catch (error) {
    return String(params);
  }
}
const findReverseHierarchy=async(data:any, key:any, level = 20) =>{
  const result = [];
  // Find the user with the provided ID
  const currentData = data.find((el:any) => el.id === key);
  // If the user is found, recursively find the reverse hierarchy
  if (currentData) {
    const { id,level, reporting_to } = currentData;
    result.push({id});
    // If the current level is greater than 0, continue the reverse hierarchy
    if (level > 0 && reporting_to !== null) {
      const subHierarchy:any = await findReverseHierarchy(data, reporting_to, level - 1);
      result.push(...subHierarchy);
    }
  }
  return result;
}
const tree = (data:any, id =  null, link = 'reporting_to') =>
data
    .filter((item:any) => item[link] === id)
    .map((item:any) => ({ ...item, children: tree(data, item.id) }));

const getWhatsAppDate =(timestamp:number) =>{
      const today = new Date();
      const date = new Date(timestamp);
    
      // Check if the date is today
      if (date.toDateString() === today.toDateString()) {
        return 'today';
      }
    
      // Check if the date is yesterday
      const yesterday = new Date(today);
      yesterday.setDate(today.getDate() - 1);
      if (date.toDateString() === yesterday.toDateString()) {
        return 'yesterday';
      }
    
      // Return the actual date
      const options:any = { year: 'numeric', month: 'long', day: 'numeric' };
      return date.toLocaleDateString(undefined, options);
}
const generateSKU=async(prefix:string, length:number)=> {
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  let sku = prefix || ''; // You can provide a prefix if needed
  sku +=  new Date().getTime();
  while (sku.length < length) {
    const randomIndex = Math.floor(Math.random() * chars.length);
    sku += chars[randomIndex];
  }
  return sku;
}    
const genrateCaptcha= async ()=> {
  const text =await generateComplexPassword(4);
  const canvas = createCanvas(400, 200);
  const ctx = canvas.getContext('2d');
  // Set background color
  ctx.fillStyle = '#FFFFFF';
  ctx.fillRect(0, 0, canvas.width, canvas.height);

  // Set text properties
  ctx.font = '30px Arial';
  ctx.fillStyle = '#000000';

  // Split text into multiple lines if needed
  const maxWidth = canvas.width - 20;
  let y = 50;
  const lines = text.split('\n');
  for (const line of lines) {
      let x = (canvas.width - ctx.measureText(line).width) / 2;
      ctx.fillText(line, x, y, maxWidth);
      y += 40; // Move to next line
  }

  // Convert canvas to Data URL
  const dataURL = canvas.toDataURL(); // Default format is PNG
  return {text,dataURL};
}

export {
  sendResponse,
  sendErrorResponse,
  validateSchema,
  renderMessage,
  generateNumber,
  filterSpacesFromArray,
  removeNewLineCharacters,
  dynamicPrecision,
  convertArrayToObject,
  addingSecurePrefixToURL,
  delay,
  jwtVerify,
  hashPasswordUsingBcrypt,
  comparePasswordUsingBcrypt,
  generateComplexPassword,
  readFile,
  jwtSign,
  randomString,
  dateDifference,
  isEmpty,
  isJson,
  findReverseHierarchy,
  tree,
  getWhatsAppDate,
  generateSKU,
  genrateCaptcha
};
