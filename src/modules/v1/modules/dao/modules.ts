import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import {
  ICheckModuleExists,
  ICreateModule,
  IUpdateModule,
  IModuleDetails,
  IModules,
  ISelectedModules,
  IWritePermissions,
  IReadPermissions,
} from "../interfaces/modules";
export class ModulesDao<T>
  extends BaseDao
  implements IReadPermissions<T>, IWritePermissions<T>
{
  async checkModuleExists(params: ICheckModuleExists): Promise<any> {
    let { name, moduleId } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (name) query.name = name;
    if (moduleId) query._id = { $ne: moduleId };
    return await this.findOne(Models.MODULES, query, {}, {}, {});
  }
  async getModuleDetails(params: IModuleDetails): Promise<any> {
    let { moduleId } = params;
    let query = {
      _id: moduleId,
      status: { $ne: STATUS.DELETED },
    };
    return await this.findOne(Models.MODULES, query, {}, {}, {});
  }
  async getAllModules(params: IModules): Promise<any> {
    let query = {
      status: { $ne: STATUS.DELETED },
    };
    let modules = await this.find(Models.MODULES, query, {}, {},{_id:-1},{...params}, {});
    let count = await this.count(Models.MODULES, query);
    return { modules, count };
  }
  async createModule(params: ICreateModule): Promise<any> {
    return await this.save(Models.MODULES, params);
  }
  async updateModule(params: IUpdateModule): Promise<any> {
    let { moduleId } = params;
    let query = {
      _id: moduleId,
    };
    let update = {
      $set: params,
    };
    return await this.updateOne(Models.MODULES, query, update, {});
  }
  async getSelectedModules(params: ISelectedModules): Promise<any> {
    let query:any = {
      status: { $ne: STATUS.DELETED },
    };
    if(params?.moduleIds && params?.moduleIds?.length){
      query["_id"]={$in:params.moduleIds}
    }
    let role= await this.find(Models.MODULES, query, {}, {},{},{limit:100}, {});
    return JSON.parse(JSON.stringify(role))
  }
}
export const modulesDao = new ModulesDao();
