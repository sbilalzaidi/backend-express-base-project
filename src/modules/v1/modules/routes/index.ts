import { Router } from "express";
const router = Router();
import {
    addModule,
    editModule,
    moduleDetails,
    modules
} from "../controllers/modules";
import {
    validateAddModule,
    validateUpdateModule,
    validateModule,
    validateModules
} from "../validaters/modules";
import { adminAuthorization } from "../../common/services/authorization";
/*
PERMISSIONS API'S
*/
router.post(
  "/",
  validateAddModule,
  adminAuthorization,
  addModule
);
router.put(
  "/",
  validateUpdateModule,
  adminAuthorization,
  editModule
);
router.get(
  "/:moduleId",
  validateModule,
  adminAuthorization,
  moduleDetails
);
router.get(
  "/",
  validateModules,
  adminAuthorization,
  modules
);

export default router;