import { Request, Response, NextFunction } from "express";
import joi from "joi";
import { STATUS } from "../../../../enums/enums";
import { validateSchema } from "../../../../lib/universal-function";
/**
 *
 * @param {*} params
 * @description for create and update setting
 * @returns
 */
const validateCreateAndUpdateSetting = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      paymentConfig: joi.any().optional(),
      smsConfig: joi.any().optional(),
      emailConfig: joi.any().optional(),
      webhookConfig: joi.any().optional(),
      vat: joi.number().min(0).max(100).optional(),
      deliveryCharge:joi.number().min(0).max(100).optional(),
      status: joi
        .number()
        .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED, STATUS.DELETED)
        .required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description  for get setting details
 * @returns
 */
const validateSettingDetails = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({});
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};
export { validateCreateAndUpdateSetting, validateSettingDetails };
