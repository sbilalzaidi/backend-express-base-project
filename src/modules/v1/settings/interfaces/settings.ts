import { ObjectId } from "mongoose";
import { STATUS } from "../../../../enums/enums";

export interface ICreateAndUpdateSetting {
  adminId: ObjectId;
  paymentConfig?: any;
  smsConfig?: any;
  emailConfig?: any;
  webhookConfig?: any;
  vat?: number;
  deliveryCharge?:number;
  status?: STATUS.ACTIVE | STATUS.IN_ACTIVATED | STATUS.DELETED;
}
export interface IReadSettings<T> {
  getSettingDetails(): Promise<any>;
}
export interface IWriteSettings<T> {
  createAndUpdateSetting(settingId:string | null,params: ICreateAndUpdateSetting): Promise<any>;
}
