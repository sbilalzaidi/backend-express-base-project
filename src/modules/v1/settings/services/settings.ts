import { settingDao } from "../dao/settings";
import { ICreateAndUpdateSetting } from "../interfaces/settings";

/**
 *
 * @param {*} params
 * @description for create and update setting
 * @returns
 */
const createAndUpdateSetting = async (settingId:string | null,params: ICreateAndUpdateSetting) => {
  return await settingDao.createAndUpdateSetting(settingId,params);
};
/**
 *
 * @param {*} params
 * @description for get setting details
 * @returns
 */
const getSettingDetails = async () => {
  return await settingDao.getSettingDetails();
};
export { createAndUpdateSetting, getSettingDetails };
