import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse } from "../../../../lib/universal-function";
import * as settingSrv from "../services/settings";
/**
 *
 * @param {*} params
 * @description for create and update setting
 * @returns
 */
const createAndUpdateSetting = asyncHandler(
  async (req: Request, res: Response) => {
    let setting = await settingSrv.getSettingDetails();
    await settingSrv.createAndUpdateSetting(setting?._id,{ ...req.body });
    setting = await settingSrv.getSettingDetails();
    return sendResponse(
      req,
      res,
      codes.SUCCESS,
      MESSAGES.SETTING_UPDATE_SUCCESSFULLY,
      setting
    );
  }
);
/**
 *
 * @param {*} params
 * @description for get setting details
 * @returns
 */
const getSettingDetails = asyncHandler(async (req: Request, res: Response) => {
  let setting = await settingSrv.getSettingDetails();
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, setting);
});
export { createAndUpdateSetting, getSettingDetails };
