import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import {
  IReadSettings,
  IWriteSettings,
  ICreateAndUpdateSetting,
} from "../interfaces/settings";
export class SettingDao<T>
  extends BaseDao
  implements IReadSettings<T>, IWriteSettings<T>
{
  async getSettingDetails(): Promise<any> {
    let query = {
      status: { $ne: STATUS.DELETED },
    };
    return await this.findOne(Models.SETTINGS, query, {}, {}, {});
  }
  async createAndUpdateSetting(settingId:string | null,params: ICreateAndUpdateSetting): Promise<any> {
    const query={
      ...(settingId && {_id:settingId}),
      status: { $ne: STATUS.DELETED }
    }
    const update = {
      $set: params
    };
    return await this.updateOne(Models.SETTINGS,query, update,{upsert: true,});
  }
}
export const settingDao = new SettingDao();
