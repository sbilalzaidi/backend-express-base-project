import { Router } from "express";
const router = Router();
import {
  createAndUpdateSetting,
  getSettingDetails,
} from "../controllers/settings";
import {
  validateCreateAndUpdateSetting,
  validateSettingDetails,
} from "../validaters/settings";
import { adminAuthorization } from "../../common/services/authorization";

/*
SETTINGS API'S
*/
router.post(
  "/",
  validateCreateAndUpdateSetting,
  adminAuthorization,
  createAndUpdateSetting
);
router.get("/", validateSettingDetails, adminAuthorization, getSettingDetails);

export default router;
