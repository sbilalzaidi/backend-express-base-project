import { ClientSession } from "mongoose";
import { ObjectId } from "mongoose";
import { STATUS ,DELIVERY_MODE} from "../../../../enums/enums";

export interface IProduct{
  productId:ObjectId;
  variantId:ObjectId;
  quantity:number;
}
export interface IStore{
  storeId:ObjectId;
  products:IProduct[]
}
export interface ICreateOrder{
  stores:IStore[];
  latitude?:number;
  longitude?:number;
  deliveryMode?:DELIVERY_MODE.DELIVERY | DELIVERY_MODE.PICK_UP;
}
export interface IReadOrders<T> {
    orders(params: any): Promise<any>;
    order(params: any,session?: ClientSession): Promise<any>;
}
export interface IWriteOrders<T> {
  createOrder(params: ICreateOrder,session?: ClientSession): Promise<any>;
  updateOrder(params: any,session?: ClientSession): Promise<any>;
}
