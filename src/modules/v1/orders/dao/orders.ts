import { ClientSession } from "mongoose";
import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import {
    IReadOrders,
    IWriteOrders
} from "../interfaces/orders";
export class OrderDao<T>
  extends BaseDao
  implements IReadOrders<T>, IWriteOrders<T>
{
 
  async createOrder(params: any,session?: ClientSession): Promise<any> {
    return await this.save(Models.ORDERS, params);
  }
  async updateOrder(params: any,session?: ClientSession): Promise<any> {
    let { orderId,userId,storeId,storeIds=[] } = params;
    let query = {
      _id: orderId,
      ...(userId && {userId:userId}),
      ...(storeId && {"stores.storeId":storeId}),
      ...(storeIds.length && {"stores.storeId":{$in:storeIds}}),
    };
    if(orderId)
    delete params.orderId
    if(userId)
    delete params.userId
    if(storeId)
    delete params.storeId
    let update = {
      $set: params,
    };
    return await this.updateOne(Models.ORDERS, query, update, {});
  }
  async order(params: any,session?: ClientSession): Promise<any> {
    let { orderId,userId,storeId } = params;
    let query = {
      _id: orderId,
      ...(userId && {userId:userId}),
      ...(storeId && {"stores.storeId":storeId}),
    };
    return await this.findOne(Models.ORDERS, query, {},{},{});
  }
  async orders(params: any): Promise<any> {
    let { orderId, orderNo,transactionId, limit = 0, pageNo, userId } = params;
    let query: any = {
      userId: userId,
      status: { $ne: STATUS.DELETED },
      ...(orderId && { orderId: orderId }),
      ...(orderNo && { orderNo: orderNo }),
      ...(transactionId && { transactionId: transactionId }),
    };

    let orders = await this.find(
      Models.ORDERS,
      query,
      {},
      {},
      {},
      { pageNo, limit },
      {
        path: "userId",
        model: "users",
      }
    );
    let count = await this.count(Models.ORDERS, query);
    return { orders, count };
  }
}
export const orderDao = new OrderDao();
