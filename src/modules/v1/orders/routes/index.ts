import { Router } from "express";
const router = Router();
import { userAuthorization,adminAuthorization } from "../../common/services/authorization";
import { validateProductVariants } from "../validaters/catalogues";
import { createOrder,cancelOrder,editOrder,changeOrderStatus, orders,checkKafka } from "../controllers/orders";
import { validateRegions } from "../validaters/regions";
import { validateStores } from "../validaters/store";
import { validateCreateOrder,
  validateCancelOrder,
  validateEditOrder,
  validateChangeOrderStatusByAdmin,
  validateChangeOrderStatusByStore } from "../validaters/orders";
/*
ORDERS API'S
*/
router.post(
  "/",
  validateCreateOrder,
  userAuthorization,
  validateRegions,
  validateStores,
  validateProductVariants,
  createOrder
);
router.patch("/users",validateCancelOrder,userAuthorization,cancelOrder);
router.put("/",validateEditOrder,adminAuthorization,editOrder);
router.patch("/",validateChangeOrderStatusByAdmin,adminAuthorization,changeOrderStatus);
router.patch("/stores",validateChangeOrderStatusByStore,adminAuthorization,changeOrderStatus);
router.get("/", adminAuthorization,orders);
router.get("/users", userAuthorization,orders);
router.get("/kafka",checkKafka);
export default router;
