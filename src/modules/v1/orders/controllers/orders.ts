import { Request, Response } from "express";
import MongoDBConnection from "../../../../connection/mongoose";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse } from "../../../../lib/universal-function";
import * as orderSrv from "../services/orders";
import { IReadOrders } from "../interfaces/orders";
import { CustomError } from "../../../../utils/customError";
import { ADMIN_TYPE, ORDER_STATUS } from "../../../../enums/enums";
import KafkaProducer from "../../common/dao/kafkaProducer";
import KafkaConsumer from "../../common/dao/kafkaConsumer";
import optimisticLock from "../../../v1/common/dao/optimisticLock";
import pessimisticLock from "../../../v1/common/dao/pessimisticLock";
import OptimisticLockDao from "../../../v1/common/dao/optimisticLock";
import PessimisticLockDao from "../../../v1/common/dao/pessimisticLock";
/**
 *
 * @param {*} params
 * @description for creating order
 * @returns
 */
const createOrder = async (req: Request, res: Response) => {
  const { reviewStoreProducts = [], stores = [] } = req.body;
  const { userId }: any = req.user;
  if (Array.isArray(reviewStoreProducts) && reviewStoreProducts.length) {
    return sendResponse(
      req,
      res,
      codes.SUCCESS,
      MESSAGES.REMOVE_OUT_OF_STOCK_ITEMS,
      { reviewStoreProducts, stores }
    );
  }
  try {
    await orderSrv.decreaseVariantQauntity(stores);
    /**
     * Payment charge logic
     */
    await orderSrv.createOrder({ userId, ...req.body });
    return sendResponse(
      req,
      res,
      codes.SUCCESS,
      MESSAGES.ORDER_CREATE_SUCCESSFULLY,
      {}
    );
  } catch (error) {
    /**
     * Payment refund logic
     */
    await orderSrv.increaseVariantQauntity(stores);
    return sendResponse(
      req,
      res,
      codes.BAD_REQUEST,
      MESSAGES.PAYMENT_ISSUE_PLEASE_TRY_AGAIN,
      {}
    );
  }
};
/**
 *
 * @param {*} params
 * @description for cancel whole order/sub order
 * @returns
 */
const cancelWholeOrSubOrderOrder = async (
  orderId: string,
  storeId: string | null,
  cancelOrderStatus: ORDER_STATUS
) => {
  let finalStores: any = [];
  const connection = MongoDBConnection.getInstance();
  const session = await connection.getDb().startSession();
  try {
    await session.withTransaction(async () => {
      const order = await orderSrv.order({ orderId, storeId }, session);
      let { refundAmount, refundedAmount, amount, cancelAmount, stores } =
        await orderSrv.cancelOrderStatusCheck(order, storeId);
      finalStores = stores;
      await orderSrv.updateOrder(
        {
          orderId,
          refundAmount,
          refundedAmount,
          amount,
          cancelAmount,
          stores,
          "stores.$.status": cancelOrderStatus,
        },
        session
      );
      /**
       * add finalOrderStatus update query update by hook
       */
      /**
       * Make refundAmount payment logic here and cron schedule
       * log also make edit log and default
       * timeout add for request
       */
    });
    await session.endSession();
    /**
     * send notification to user/store
     */

    /**
     * increase variant quantity only not canceled order
     */
    if (finalStores.length) orderSrv.increaseVariantQauntity(finalStores);
  } catch (error) {
    await session.abortTransaction(); // Rollback the transaction in case of an error
    await session.endSession();
    throw error;
  }
};
/**
 *
 * @param {*} params
 * @description for cancel order
 * @returns
 */
const cancelOrder = asyncHandler(async (req: Request, res: Response) => {
  const { orderId } = req.body;
  const { userId }: any = req.user;
  await cancelWholeOrSubOrderOrder(orderId, null, ORDER_STATUS.CANCEL);
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {});
});
/**
 *
 * @param {*} params
 * @description for edit order
 * @returns
 */
const editOrder = asyncHandler(async (req: Request, res: Response) => {
  const { orderId, products } = req.body;
  const { parentId, adminId, adminType }: any = req.user;
  let storeId: string = "";
  if (ADMIN_TYPE.COMPANY == adminType) storeId = adminId;
  if (ADMIN_TYPE.COMPANY_USER == adminType) storeId = parentId;
  const connection = MongoDBConnection.getInstance();
  const session = await connection.getDb().startSession();
  try {
    await session.withTransaction(async () => {
      const order = await orderSrv.order({ orderId, storeId }, session);
      let { refundAmount, refundedAmount, amount, cancelAmount, stores } =
        await orderSrv.editOrderQuantity(order, products);
      await orderSrv.updateOrder(
        {
          orderId,
          refundedAmount,
          amount,
          cancelAmount,
          stores,
        },
        session
      );
      /**
       * Make refundAmount payment logic here and cron schedule
       * log also make edit log and default
       * timeout add for request
       */
    });
    await session.endSession();
    /**
     * send notification to user/store
     */
    return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {});
  } catch (error) {
    await session.abortTransaction(); // Rollback the transaction in case of an error
    await session.endSession();
    throw error;
  }
});

/**
 *
 * @param {*} params
 * @description for change orders status
 * @returns
 */
const changeOrderStatus = asyncHandler(async (req: Request, res: Response) => {
  const { orderId, storeId, orderStatus, finalOrderStatus } = req.body;
  const { adminId, adminType }: any = req.user;
  let cancelOrderStatus: ORDER_STATUS = ORDER_STATUS.CANCEL_BY_STORE;
  if ([ADMIN_TYPE.ADMIN, ADMIN_TYPE.ADMIN_USER].includes(adminType))
    [(cancelOrderStatus = ORDER_STATUS.CANCEL_BY_ADMIN)];
  if (
    storeId &&
    [ORDER_STATUS.CANCEL_BY_STORE, ORDER_STATUS.CANCEL_BY_ADMIN].includes(
      orderStatus
    )
  ) {
    await cancelWholeOrSubOrderOrder(orderId, storeId, cancelOrderStatus);
    return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {});
  } else if (
    !storeId &&
    adminId &&
    finalOrderStatus == ORDER_STATUS.CANCEL_BY_ADMIN
  ) {
    await cancelWholeOrSubOrderOrder(orderId, null, cancelOrderStatus);
    return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {});
  } else {
    /**
     * send notification to user
     */
    await orderSrv.updateOrder({
      orderId,
      storeId,
      orderStatus,
      finalOrderStatus,
    });
    return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {});
  }
});
/**
 *
 * @param {*} params
 * @description for orders
 * @returns
 */
const orders = asyncHandler(async (req: Request, res: Response) => {
  let { search, limit, pageNo } = req.query;
  let orders = await orderSrv.orders({ search, limit, pageNo });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, orders);
});
async function genericUpdateOperation(
  resourceId: string,
  newData: string
): Promise<void> {
  // Simulate some generic resource update logic
  console.log(`Updating resource ${resourceId} with data: ${newData}`);
}

async function performGenericLockedOperation(
  resourceId: string,
  newData: string,
  lockManager:any,
): Promise<void> {
  await lockManager.performLockedOperation(resourceId, async () => {
    await genericUpdateOperation(resourceId, newData);
  });
}
/**
 *
 * @param {*} params
 * @description for check Kafka
 * @returns
 */
const checkKafka = asyncHandler(async (req: Request, res: Response) => {
  const topic = "test1";
  let messages = [
    {
      value: "demo",
    },
  ];
  console.log("#######1", new Date().getTime());
  // optimisticLock pessimisticLock
  // const lock = new OptimisticLockDao("the_locak", 10000, 500);

  // try {
  //   const lockAcquired = await lock.acquireLockWithRetry();

  //   if (lockAcquired) {
  //     // Perform operations on the resource
  //     await lock.updateResource({ example:req?.query|| "data" });
  //     console.log("Lock acquired and resource updated successfully.");
  //   } else {
  //     console.log("Failed to acquire lock within the specified timeout.");
  //   }
  // } finally {
  //   // Release the lock when done
  //   await lock.releaseLock();
  // }

  // const lockManager = new PessimisticLockDao();
  // const resourceId = "example_resource_id";
  // const newData = "updated_data";
  // performGenericLockedOperation(resourceId, newData,lockManager)
  //   .then(() => console.log("Operation successful"))
  //   .catch((error) =>
  //     console.error(`Error performing operation: ${error.message}`)
  //   );
  // const kafkaProducer = KafkaProducer.getInstance();
  // let data1=await kafkaProducer.produceMessage(topic,messages);
  // console.log("#######2",(new Date()).getTime())
  // const kafkaConsumer = KafkaConsumer.getInstance();
  // let data2=await kafkaConsumer.consumeMessages([topic])
  // console.log("#######3",(new Date()).getTime())
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {});
});
export {
  createOrder,
  cancelOrder,
  editOrder,
  changeOrderStatus,
  orders,
  checkKafka,
};
