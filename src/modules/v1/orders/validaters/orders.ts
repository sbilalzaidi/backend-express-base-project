import { Request, Response, NextFunction } from "express";
import joi from "joi";
import CONSTANT from "../../../../constant/constant";
import {
  DELIVERY_MODE,
  ORDER_STATUS
} from "../../../../enums/enums";
import { validateSchema } from "../../../../lib/universal-function";

/**
 *
 * @param {*} params
 * @description for create order
 * @returns
 */

const validateCreateOrder = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      stores: joi
        .array()
        .items(
          joi
            .object()
            .keys({
              strorId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
              products: joi
                .array()
                .items(
                  joi
                    .object()
                    .keys({
                      productId: joi
                        .string()
                        .regex(CONSTANT.REGEX.MONGO_ID)
                        .required(),
                      variantId: joi
                        .string()
                        .regex(CONSTANT.REGEX.MONGO_ID)
                        .required(),
                      quantity: joi.number().min(1).max(10000).required(),
                    })
                    .required()
                )
                .required(),
            })
            .required()
        )
        .required(),
      latitude: joi.number().min(-90).max(90).required(),
      longitude: joi.number().min(-180).max(180).required(),
      address: joi.string().required(),
      deliveryOrderDate: joi.date().required(),
      deliveryMode: joi
        .number()
        .valid([DELIVERY_MODE.DELIVERY, DELIVERY_MODE.PICK_UP])
        .default(DELIVERY_MODE.DELIVERY)
        .optional(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};

/**
 *
 * @param {*} params
 * @description for cancel order
 * @returns
 */
const validateCancelOrder = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      orderId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
      finalOrderStatus: joi
        .number()
        .valid(
          ORDER_STATUS.CANCEL)
        .required(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for edit order by admin
 * @returns
 */

const validateEditOrder = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      orderId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
      products:joi
      .array().items(
        joi.object().keys({
          variantId: joi
          .string()
          .regex(CONSTANT.REGEX.MONGO_ID)
          .required(),
          quantity: joi.number().min(1).max(10000).required(),
        }).required()
      ).required(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for change order status by admin
 * @returns
 */

const validateChangeOrderStatusByAdmin = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      orderId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
      finalOrderStatus:joi
      .number()
      .valid(
        ORDER_STATUS.PENDING,
        ORDER_STATUS.CANCEL,
        ORDER_STATUS.CANCEL_BY_ADMIN,
        ORDER_STATUS.PROGRESS,
        ORDER_STATUS.COMPLETED)
      .required(),
    });
    let schemaForStore = joi.object().keys({
      orderId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
      storeId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
      orderStatus: joi
        .number()
        .valid(
          ORDER_STATUS.PENDING,
          ORDER_STATUS.ACCEPT,
          ORDER_STATUS.CANCEL,
          ORDER_STATUS.CANCEL_BY_ADMIN,
          ORDER_STATUS.PAKING,
          ORDER_STATUS.PICKED_UP,
          ORDER_STATUS.ON_THE_WAY,
          ORDER_STATUS.PROGRESS,)
        .required(),
    });
   const alternatingSchema = joi.alternatives().conditional('storeId', {
      is: joi.exist(),
      then: schemaForStore,
      otherwise: schema,
    });
    await validateSchema(req.query, alternatingSchema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for change order status by store
 * @returns
 */
const validateChangeOrderStatusByStore = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      orderId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
      orderStatus: joi
        .number()
        .valid(
          ORDER_STATUS.PENDING,
          ORDER_STATUS.ACCEPT,
          ORDER_STATUS.CANCEL,
          ORDER_STATUS.CANCEL_BY_ADMIN,
          ORDER_STATUS.PAKING,
          ORDER_STATUS.PICKED_UP,
          ORDER_STATUS.ON_THE_WAY,
          ORDER_STATUS.PROGRESS)
        .required(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};
export { 
  validateCreateOrder,
  validateCancelOrder,
  validateEditOrder,
  validateChangeOrderStatusByAdmin,
  validateChangeOrderStatusByStore };
