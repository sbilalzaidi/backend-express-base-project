import { Request, Response, NextFunction } from "express";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { CustomError } from "../../../../utils/customError";
import * as orderSrv from "../services/orders";

/**
 * Setp 2 to check stores
 * @param req
 * @param res
 * @param next
 */
const validateStores = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const {
    regionIds = [],
    stores = [],
    longitude = 0,
    latitude = 0,
    deliveryOrderDate = new Date(),
    address = "",
  } = req.body;
  let storeIds: string[] = Array.from(
    new Set(stores.map((store: any) => store.storeId))
  );
  const storesData = await orderSrv.stores(
    storeIds,
    regionIds,
    longitude,
    latitude,
    deliveryOrderDate
  );
  if (storesData.length != storeIds.length) {
    /**
     * ya ha item delete ka popup ayega then delete item huga cart sai
     */
    throw new CustomError(MESSAGES.STORE_NOT_SERVE_THIS_DELIVERY_ADDRESS, {
      code: codes.BAD_REQUEST,
      data: {
        address: address,
      },
    });
  } else {
    req.body.storesDict = storesData.reduce((acc: any, store: any) => {
      acc[store.storeId] = store;
      return acc;
    }, {});
    next();
  }
};

export { validateStores };
