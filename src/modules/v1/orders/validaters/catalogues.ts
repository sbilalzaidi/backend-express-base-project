import { Request, Response, NextFunction } from "express";
import MESSAGES from "../../../../messages/messages";
import CONSTANT from "../../../../constant/constant";
import codes from "../../../../codes/status_codes";
import {renderMessage} from "../../../../lib/universal-function";
import { CustomError } from "../../../../utils/customError";
import * as orderSrv from '../services/orders'
import { loadLanguage } from '../../../../langs/index';

/**
 *
 * @param {*} params
 * @description for check variant qauntity
 * @returns
 */
const checkVariantQauntity = async (stores:any,storesDict:any,productVariantDict:any,lang:string) => {
  let reviewStoreProducts:any={
    stores:[]
  };
  let finalStoreProducts:any={
    stores:[]
  }
  let totalAmount:number=0;
  for(let store of stores){
    const {products=[]} = store;
    let reviewProducts:any=[];
    let finalProducts:any=[];
    let amount:number=0;
    for(let product of products){
      let productVarentId=(product.productId).toString()+"-"+(product.variantId).toString();
      if (productVariantDict[productVarentId]?.variant?.enableInventory &&
        productVariantDict[productVarentId]?.variant?.inventoryOn
      ) {
        if(!productVariantDict[productVarentId]?.variant?.quantity){
           /**
           * Send Notification to store for quantity write logic
           * `${product.name[language]} is out of stock`
           */
          const language = await loadLanguage(lang);
          let message=language?.[MESSAGES.PRODUCT_IS_OUT_OF_STOCK] ||'';
          let data={ name: productVariantDict[productVarentId]?.product?.name }
          if(message)
          message=renderMessage(message,data)
          reviewProducts.push({
            ...product,
            message:message
          })
        }
        else if (product.qauntity >= productVariantDict[productVarentId]?.variant?.quantity) {
           /**
           * Send Notification to store for quantity write logic
           * `${product.name[language]} is out of stock`
           */
          let finalQantity=product.qauntity-productVariantDict[productVarentId]?.variant?.quantity;
          amount+=parseFloat(((productVariantDict[productVarentId]?.variant?.spWithVat)*product.qauntity).toFixed(3));
          totalAmount+=parseFloat((amount).toFixed(3));
          finalProducts.push({
            ...product,
            name:productVariantDict[productVarentId]?.product?.name,
            description:productVariantDict[productVarentId]?.product?.description,
            images:productVariantDict[productVarentId]?.product?.images,
            icons:productVariantDict[productVarentId]?.product?.icons,
            brand:productVariantDict[productVarentId]?.variant?.brand,
            label:productVariantDict[productVarentId]?.variant?.label,
            variantImages:productVariantDict[productVarentId]?.variant?.variantImages,
            variantIcons:productVariantDict[productVarentId]?.variant?.variantIcons,
            cp:productVariantDict[productVarentId]?.variant?.cp,
            sp:productVariantDict[productVarentId]?.variant?.sp,
            spAfterDiscount:productVariantDict[productVarentId]?.variant?.spAfterDiscount,
            spWithVat:productVariantDict[productVarentId]?.variant?.spWithVat,
            vat:productVariantDict[productVarentId]?.variant?.vat,
            discount:productVariantDict[productVarentId]?.variant?.discount,
            discountType:productVariantDict[productVarentId]?.variant?.discountType
          })
          if (finalQantity){
            const language = await loadLanguage(lang);
            let message=language?.[MESSAGES.PRODUCT_HAS_LIMITED_QUANTITY] ||'';
            let data={ name: productVariantDict[productVarentId]?.product?.name, 
              quantity: productVariantDict[productVarentId]?.variant?.quantity }
            if(message)
            message=renderMessage(message,data)
            reviewProducts.push({
              ...product,
              qauntity:finalQantity,
              message:message
            })
          }
        }
      }
      else if(!productVariantDict[productVarentId]){
        /**
         * ya ha item delete ka popup ayega then delete item huga cart sai
         */
        throw new CustomError(MESSAGES.ITEM_UNAVAILABLE, {
          code: codes.BAD_REQUEST,
          data: { quantity: product.quantity },
        });
      }
    }
    if(finalProducts.length){
      finalStoreProducts.stores.push({
        ...store,
        amount:amount,
        shopName:storesDict?.[store.storeId]?.shopName ||'',
        products:finalProducts
      })
    }
    if(reviewProducts.length){
      reviewStoreProducts.stores.push({
        ...store,
        amount:totalAmount,
        paidAmount:totalAmount,
        products:reviewProducts
      })
    }
    amount=0;
    finalProducts=[];
    reviewProducts=[];
  }
  return {reviewStoreProducts,finalStoreProducts};
};
const createProductVariantDict=async(products:any)=>{
  let productVariantDict:any={};
  for (let product of products) {
    let variants= product?.variants || [];
    if(product?.variants)
    delete product?.variants;
    for (let variant of variants) { 
        let productVarentId=(product._id).toString()+"-"+(variant._id).toString();
        productVariantDict[productVarentId]=Object.assign({},{product,variant})
    }
  }
  return productVariantDict;
}

/**
 * Setp 3 to check product and variant quantity 
 * @param req 
 * @param res 
 * @param next 
 */
const validateProductVariants = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const lang:string = req.headers?.["content-language"] || CONSTANT.LANGUAGE_TYPE.ENGLISH;
  const { regionIds = [],stores=[],storesDict={}} = req.body;
  let productIds: string[]=[]
  let variantIds: string[]=[];
  let demandOfVariantQauntityDict:any={}
    for(let store of stores){
      const {products=[]} = store;
      for(let product of products){
        productIds.push(product.productId);
        variantIds.push(product.variantId);
        demandOfVariantQauntityDict[product.variantId]=product.quantity || 0;
      }
    }
    productIds=Array.from(new Set(...productIds))
    variantIds=Array.from(new Set(...variantIds))
    let productsData = await orderSrv.productWithVariants(
      regionIds,
      productIds,
      variantIds
    );
    const productVariantDict=await createProductVariantDict(productsData);
    let productFilter=await checkVariantQauntity(stores,storesDict,productVariantDict,lang);
    req.body.reviewStoreProducts=productFilter?.reviewStoreProducts?.stores || [];
    req.body.stores=productFilter?.finalStoreProducts?.stores || [];
    if(Array.isArray(req.body.stores) && stores.length==0){
      throw new CustomError(MESSAGES.ALL_PRODUCT_IS_OUT_OF_STOCK, {
        code: codes.BAD_REQUEST
      });
    }
    else next();
};

export{
  validateProductVariants
}