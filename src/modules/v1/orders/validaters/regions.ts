import { Request, Response, NextFunction } from "express";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { CustomError } from "../../../../utils/customError";
import * as orderSrv from '../services/orders';

/**
 * Setp 1 to check regions
 * @param req 
 * @param res 
 * @param next 
 */
const validateRegions = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { longitude = 0 ,latitude = 0,address=''} = req.body;
  let regionIds: string[] = [];
  if (latitude && longitude)
    regionIds = await orderSrv.regionIdsThroughLocation(longitude,latitude);
  req.body.regionIds = regionIds;
  if(!regionIds.length){
    /**
     * ya ha item delete ka popup ayega then delete item huga cart sai
    */
    throw new CustomError(MESSAGES.NOT_SERVE_THIS_DELIVERY_ADDRESS, {
      code: codes.BAD_REQUEST,
      data:{
        address:address
      }
    });
  }
  else{
    next();
  }
};


export{
  validateRegions,
}