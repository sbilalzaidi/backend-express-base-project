import moment from "moment";
import { ClientSession } from "mongoose";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { CustomError } from "../../../../utils/customError";
import { adminDao } from "../../admins/dao/admins";
import { catalogueDao } from "../../catalogues/dao/catalogues";
import { regionDao } from "../../regions/dao/regions";
import { variantDao } from "../../variants/dao/variants";
import { orderDao } from "../dao/orders";
import { IReadOrders } from "../interfaces/orders";
import {
  DISCOUNT_TYPE,
  ORDER_STATUS,
  DELIVERY_MODE,
  PAYMENT_MODE,
  PAYMENT_STATUS,
} from "../../../../enums/enums";
/**
 *
 * @param {*} params
 * @description for get regions
 * @returns
 */
const regionIdsThroughLocation = async (
  longitude: number,
  latitude: number
) => {
  let regions = await regionDao.regionsThroughLocation({ latitude, longitude });
  return regions.map((region: any) => region.regionId);
};
/**
 *
 * @param {*} params
 * @description for get stores
 * @returns
 */
const stores = async (
  storeIds: string[],
  regionIds: string[] | [],
  longitude: number,
  latitude: number,
  date: Date | null
) => {
  let dayTime: string = "";
  if (date) dayTime = moment(new Date(date)).format("e HHmm");
  else dayTime = moment(new Date()).format("e HHmm");
  const day = dayTime.split(" ")[0];
  const time = dayTime.split(" ")[1];
  return await adminDao.stores(
    storeIds,
    regionIds,
    longitude,
    latitude,
    +day,
    +time
  );
};
/**
 *
 * @param {*} params
 * @description for products with varients
 * @returns
 */
const productWithVariants = async (
  regionIds: string[] | [],
  productIds: string[],
  variantIds: string[]
) => {
  return catalogueDao.productsWithVariants(regionIds, productIds, variantIds);
};
/**
 *
 * @param {*} params
 * @description for increase variant qauntity
 * @returns
 */
const increaseVariantQauntity = async (stores: any) => {
  const { products = [] } = stores;
  for (let product of products) {
    await variantDao.increaseVariantQauntity(
      product.productId,
      product.variantId,
      product.quantity
    );
  }
  return true;
};
/**
 *
 * @param {*} params
 * @description for decrease variant qauntity
 * @returns
 */
const decreaseVariantQauntity = async (stores: any) => {
  const { products = [] } = stores;
  for (let product of products) {
    await variantDao.decreaseVariantQauntity(
      product.productId,
      product.variantId,
      product.quantity
    );
  }
  return true;
};

/**
 *
 * @param {*} params
 * @description for create order
 * @returns
 */
const createOrder = async (params: any, session?: ClientSession) => {
  return await orderDao.createOrder(params, session);
};
const calculateRefundedAmount = async (
  refundedAmount: number,
  discountedAmount: number,
  totalAmount: number,
  totalCancelAmount: number
) => {
  if (discountedAmount && totalAmount >= totalCancelAmount)
    return (
      parseFloat(((refundedAmount / (totalAmount - totalCancelAmount)) * discountedAmount).toFixed(3))
    );
  else return refundedAmount;
};
/**
 *
 * @param {*} params
 * @description for edit order
 * @returns
 */
const cancelOrderStatusCheck = async (params: any, storeId: string | null) => {
  let {
    stores = [],
    discountedAmount = 0,
    cancelAmount,
    finalOrderStatus,
    amount,
    paidAmount,
  } = params;
  let finalStores: any = [];
  let totalAmount: number = 0;
  let refundedAmount: number = 0;
  let totalCancelAmount: number = 0;
  for (let store of stores) {
    let { orderStatus } = store;
    if (
      [
        ORDER_STATUS.PAKING,
        ORDER_STATUS.PICKED_UP,
        ORDER_STATUS.ON_THE_WAY,
        ORDER_STATUS.COMPLETED,
      ].includes(orderStatus)
    ) {
      throw new CustomError(MESSAGES.CAN_NOT_CANCEL_ORDER, {
        code: codes.BAD_REQUEST,
      });
    } else if (
      storeId &&
      [
        ORDER_STATUS.CANCEL,
        ORDER_STATUS.CANCEL_BY_ADMIN,
        ORDER_STATUS.CANCEL_BY_STORE,
      ].includes(orderStatus)
    ) {
      throw new CustomError(MESSAGES.ORDER_ALREADY_CANCELED, {
        code: codes.BAD_REQUEST,
      });
    } else if ([ORDER_STATUS.COMPLETED].includes(finalOrderStatus)) {
      throw new CustomError(MESSAGES.ORDER_ALREADY_COMPLETED, {
        code: codes.BAD_REQUEST,
      });
    } else if (
      [ORDER_STATUS.CANCEL, ORDER_STATUS.CANCEL_BY_ADMIN].includes(
        finalOrderStatus
      )
    ) {
      throw new CustomError(MESSAGES.ORDER_ALREADY_CANCELED, {
        code: codes.BAD_REQUEST,
      });
    } else {
      if (storeId && store.storeId.toString() == storeId) {
        totalAmount = amount;
        totalCancelAmount = cancelAmount + store.amount;
        refundedAmount = store.amount;
        finalStores.push(store);
        break;
      } else if (storeId) {
        throw new CustomError(MESSAGES.CAN_NOT_CANCEL_ORDER, {
          code: codes.BAD_REQUEST,
        });
      } else {
        totalAmount = amount;
        totalCancelAmount = amount;
        refundedAmount = paidAmount - params.refundedAmount;
        finalStores.push(store);
      }
    }
  }
  let refundAmount = await calculateRefundedAmount(
    refundedAmount,
    discountedAmount,
    totalAmount,
    totalCancelAmount
  );

  return {
    refundAmount: refundAmount,
    refundedAmount: params?.refundedAmount + refundedAmount,
    amount: totalAmount,
    cancelAmount: totalCancelAmount,
    stores: finalStores || [],
  };
};
/**
 *
 * @param {*} params
 * @description for edit order
 * @returns
 */
const editOrderQuantity = async (params: any, products: any) => {
  let { stores = [], discountedAmount = 0 } = params;
  let finalStoreProducts: any = {
    stores: [],
  };
  let totalAmount: number = 0;
  let refundedAmount: number = 0;
  let totalCancelAmount: number = 0;
  let productsDitc: any = products.reduce((acc: any, product: any) => {
    acc[product.variantId] = product?.quantity || 0;
    return acc;
  }, {});
  for (let store of stores) {
    let orderedProducts = store?.products || [];
    let finalProducts: any = [];
    let amount: number = 0;
    if (
      ![
        ORDER_STATUS.CANCEL,
        ORDER_STATUS.CANCEL_BY_ADMIN,
        ORDER_STATUS.CANCEL_BY_STORE,
      ].includes(store.orderStatus) &&
      Array.isArray(orderedProducts) &&
      orderedProducts.length
    ) {
      for (let orderedProduct of orderedProducts) {
        if (productsDitc[orderedProduct.variantId]) {
          if (
            productsDitc[orderedProduct.variantId]?.qauntity >
            orderedProduct.qauntity
          ) {
            //quantity should be less then ordered quantity
            throw new CustomError(
              MESSAGES.ADD_MODULE_PERMISSIONS_CREATE_SUCCESSFULLY
            );
          }
          let finalQantity =
            orderedProduct.qauntity -
            productsDitc?.[orderedProduct.variantId]?.qauntity;
          amount += parseFloat(
            (orderedProduct?.spWithVat * finalQantity).toFixed(3)
          );
          refundedAmount += parseFloat(
            (
              orderedProduct?.spWithVat *
              productsDitc[orderedProduct.variantId]?.qauntity
            ).toFixed(3)
          );
          if (finalQantity) {
            finalProducts.push({
              ...orderedProduct,
              qauntity: finalQantity,
            });
          }
        } else {
          amount += parseFloat(
            (orderedProduct?.spWithVat * orderedProduct.qauntity).toFixed(3)
          );
          finalProducts.push({
            ...orderedProduct,
          });
        }
      }
      if (finalProducts.length) {
        totalAmount += parseFloat(amount.toFixed(3));
        finalStoreProducts.stores.push({
          ...store,
          amount: amount,
          products: finalProducts,
        });
      } else {
        // throw error if remove all product a a given store you can cancel the order atleast one product in order
      }
    } else if (store) {
      totalAmount += parseFloat(store.amount.toFixed(3));
      totalCancelAmount += parseFloat(store.amount.toFixed(3));
      finalStoreProducts.stores.push({
        ...store,
      });
    }
  }

  let refundAmount = await calculateRefundedAmount(
    refundedAmount,
    discountedAmount,
    totalAmount,
    totalCancelAmount
  );

  return {
    refundAmount: refundAmount,
    refundedAmount: params?.refundedAmount + refundedAmount,
    amount: totalAmount,
    cancelAmount: totalCancelAmount,
    stores: finalStoreProducts?.stores || [],
  };
};

/**
 *
 * @param {*} params
 * @description for update order
 * @returns
 */
const updateOrder = async (params: any, session?: ClientSession) => {
  return await orderDao.updateOrder(params, session);
};
/**
 *
 * @param {*} params
 * @description for order
 * @returns
 */
const order = async (params: any, session?: ClientSession) => {
  return await orderDao.order(params, session);
};
/**
 *
 * @param {*} params
 * @description for orders
 * @returns
 */
const orders = async (params: any) => {
  return await orderDao.orders(params);
};
export {
  regionIdsThroughLocation,
  stores,
  productWithVariants,
  increaseVariantQauntity,
  decreaseVariantQauntity,
  createOrder,
  cancelOrderStatusCheck,
  editOrderQuantity,
  updateOrder,
  order,
  orders,
};
