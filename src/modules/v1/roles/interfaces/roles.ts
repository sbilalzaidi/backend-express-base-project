export interface ICheckRoleExists {
    role?:string;
    roleId?:string
}
export interface IRoleDetails {
    roleId:string;
}
export interface IDeleteModuleFromRole {
    roleId:string;
    moduleId:string;
}
export interface ICheckModuleInRole {
    roleId:string;
    moduleId?:string;
}

interface IModules{
    moduleId:string;
    add?:Boolean | null;
    edit?:Boolean | null;
    view?:Boolean | null;
    delete?:Boolean | null;
}
export interface ICreateRole{
    role:string;
    description?:string;
    modules?:IModules[]
}
export interface IUpdateRole{
    roleId:string;
    role?:string;
    description?:string;
    moduleId:string;
    add?:Boolean | null;
    edit?:Boolean | null;
    view?:Boolean | null;
    delete?:Boolean | null;
}
export interface IGetAllRoles{
    search?:string;
    limit:number;
    pageNo:number;
}
export interface IReadRole<T> {
    checkRoleExists(params:any): Promise<any>;
    roleDetails(params:any): Promise<any>;
    getAllRoles(params:any): Promise<any>;
}
export interface IWriteRole<T> {
    createRole(params:any): Promise<any>;
    updateRole(params:any): Promise<any>;
    deleteRole(params:any): Promise<any>;
}

