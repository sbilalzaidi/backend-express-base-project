import { Router } from "express";
const router = Router();
import { addRole, editRole,deleteRole, roleDetails, roles } from "../controllers/roles";
import {
  validateAddRole,
  validateUpdateRole,
  validateDeleteRole,
  validateRole,
  validateRoles,
} from "../validaters/roles";
import { adminAuthorization } from "../../common/services/authorization";
/*
ROLES API'S
*/
router.post("/", validateAddRole, adminAuthorization, addRole);
router.put("/", validateUpdateRole, adminAuthorization, editRole);
router.delete("/", validateDeleteRole, adminAuthorization, deleteRole);
router.get("/:roleId", validateRole, adminAuthorization, roleDetails);
router.get("/", validateRoles, adminAuthorization, roles);

export default router;
