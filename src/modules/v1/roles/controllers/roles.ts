import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse } from "../../../../lib/universal-function";
import * as roleSrv from "../services/roles";
import { IGetAllRoles } from "../interfaces/roles";
/**
 *
 * @param {*} params
 * @description for creating role
 * @returns
 */
const addRole = asyncHandler(async (req: Request, res: Response) => {
  let { role, description, modules } = req.body;
  await roleSrv.checkRoleExists({ ...req.body });
  await roleSrv.createRole({ role, description, modules });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.ROLE_CREATE_SUCCESSFULLY,
    {}
  );
});

/**
 *
 * @param {*} params
 * @description for edit role
 * @returns
 */
const editRole = asyncHandler(async (req: Request, res: Response) => {
  let {role=''} =req.body;
  if(role)
  await roleSrv.checkRoleExists({ ...req.body });
  await roleSrv.updateRole({ ...req.body });

  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.ROLE_UPDATED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for delete role
 * @returns
 */
const deleteRole = asyncHandler(async (req: Request, res: Response) => {
  let { roleId ,moduleId} = req.body;
  await roleSrv.deleteRole({roleId,moduleId});

  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.ROLE_DELETED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for role details
 * @returns
 */
const roleDetails = asyncHandler(async (req: Request, res: Response) => {
  let { roleId } = req.params;
  let role:any = await roleSrv.getRoleDetails({ roleId });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, role);
});
/**
 *
 * @param {*} params
 * @description for roles
 * @returns
 */
const roles = asyncHandler(async (req: Request, res: Response) => {
  let { search, limit, pageNo } = req.query as unknown as IGetAllRoles;
  let roles = await roleSrv.getAllRoles({ search, limit, pageNo });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, roles);
});

export { addRole, editRole,deleteRole, roleDetails, roles };
