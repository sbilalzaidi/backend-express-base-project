import MESSAGES from "../../../../messages/messages";
import { rolesDao } from "../dao/roles";
import { modulesDao } from "../../modules/dao/modules";
import { 
  ICheckRoleExists,
  IRoleDetails,
  ICreateRole,
  IUpdateRole,
  IDeleteModuleFromRole,
  IGetAllRoles
} from "../interfaces/roles";
/**
 *
 * @param {*} params
 * @description for cheking role exists or not
 * @returns
 */
const checkRoleExists = async (params: ICheckRoleExists) => {
  let roleExists = await rolesDao.checkRoleExists(params);
  if (roleExists) throw new Error(MESSAGES.ROLE_ALREADY_EXISTS);
  return true;
};

/**
 *
 * @param {*} params
 * @description for create role
 * @returns
 */
const createRole = async (params: ICreateRole) => {
  return await rolesDao.createRole(params);
};
/**
 *
 * @param {*} params
 * @description for delete role
 * @returns
 */
const deleteRole = async (params: IDeleteModuleFromRole) => {
  let {roleId,moduleId}=params;
  if(moduleId)
  return await rolesDao.deleteModuleFromRole(params);
  else
  return await rolesDao.deleteRole({roleId})
};

/**
 *
 * @param {*} params
 * @description for update role
 * @returns
 */
const updateRole = async (params: IUpdateRole) => {
  const {roleId,moduleId}=params;
  const isRoleExists= await rolesDao.checkModuleInRoleOrRole({roleId})
  if(!isRoleExists)
  throw new Error(MESSAGES.ROLE_DOES_NOT_EXISTS);
  if(!moduleId)
  await rolesDao.updateRole(params);
  else{
    let role:any=await rolesDao.checkModuleInRoleOrRole({roleId,moduleId});
    if(role)
    await rolesDao.updateRole(params);
    else{
      await rolesDao.addModuleInRole(params);
    }
  }
  return await rolesDao.updateRole(params);
};
/**
 *
 * @param {*} params
 * @description for role
 * @returns
 */
const getRoleDetails = async (params: IRoleDetails) => {
  let role:any=await rolesDao.roleDetails(params);
  role=JSON.parse(JSON.stringify(role))
  let selctedModules=role?.modules || [];
  let moduleDict:any={};
  for(let module of selctedModules){
    module={...module,name:module?.moduleId?.name ||'',
    visibilityFor:module?.moduleId?.visibilityFor || 0}
    moduleDict[module?.moduleId?._id?.toString()]=module;
    if(module?.moduleId)
    delete module.moduleId;
  }
  let allModules:any=await modulesDao.getSelectedModules({})
  role.modules=[];
  for(let module of allModules){
    let isSelected=false;
    if(moduleDict[module?._id?.toString()])
    isSelected=true;
    role.modules.push({
      moduleId:module?._id,
      isSelected,
      ...(isSelected ?(moduleDict[module?._id?.toString()]):module),  
    })
  }
  return role
};
/**
 *
 * @param {*} params
 * @description for roles
 * @returns
 */
const getAllRoles=async (params:IGetAllRoles)=>{
 return await rolesDao.getAllRoles(params);
}
export {
  checkRoleExists,
  createRole,
  updateRole,
  deleteRole,
  getRoleDetails,
  getAllRoles
};
