import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import { 
  ICheckRoleExists,
  IRoleDetails,
  ICreateRole,
  IUpdateRole,
  IDeleteModuleFromRole,
  ICheckModuleInRole,
  IGetAllRoles,
  IWriteRole, 
  IReadRole } from "../interfaces/roles";
import mongoose from "mongoose";
export class RolesDao<T>
  extends BaseDao
  implements IReadRole<T>, IWriteRole<T>
{
  async checkRoleExists(params: ICheckRoleExists): Promise<any> {
    let { role, roleId } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (role) query.role = role;
    if (roleId) query._id = { $ne: roleId };
    return await this.findOne(Models.ROLES,query, {}, {}, {});
  }
  async createRole(params: ICreateRole): Promise<any> {
    return await this.save(Models.ROLES, params);
  }
  async updateRole(params: IUpdateRole): Promise<any> {
    let { roleId,role,description,moduleId,add,edit,view } = params;
    let query = {
      _id: roleId,
      ...(moduleId && {'modules.moduleId':moduleId})
    };
    let update = {
      $set: { 
        ...(role && {role:role}),
        ...(description && {description:description}),
       ...(moduleId && {"modules.$.add": add }),
       ...(moduleId && {"modules.$.edit": edit }),
       ...(moduleId && {"modules.$.view": view }),
       ...(moduleId && {"modules.$.delete": params?.delete}),
      },
    };
    return await this.findOneAndUpdate(Models.ROLES, query, update, { upsert: true, new: true });
  }
  async addModuleInRole(params: IUpdateRole): Promise<any> {
    let { roleId,role,description,moduleId,add,edit,view } = params;
    let query = {
      _id: roleId,
    };
    let update = {
      $addToSet: { 
        ...(role && {role:role}),
        ...(description && {description:description}),
        modules: {
          moduleId: new mongoose.Types.ObjectId(moduleId),
          add: add,
          edit: edit,
          view: view,
          delete:  params?.delete
        }
      },
    };
    return await this.findOneAndUpdate(Models.ROLES, query, update, { });
  }
  async deleteModuleFromRole(params: IDeleteModuleFromRole): Promise<any> {
    let { roleId,moduleId } = params;
    let query = {
      _id: roleId
    };
    let update={ $pull: { modules: { moduleId: moduleId} } }
    return await this.findOneAndUpdate(Models.ROLES, query, update, {});
  }
  async checkModuleInRoleOrRole(params: ICheckModuleInRole): Promise<any> {
    let { roleId,moduleId } = params;
    let query = {
      _id: roleId,
      ...(moduleId && {'modules.moduleId':moduleId})
    };
    return await this.findOne(Models.ROLES, query, {},{}, {});
  }
  async deleteRole(params: IRoleDetails): Promise<any> {
    let { roleId } = params;
    let query = {
      _id: roleId
    };
    return await this.delete(Models.ROLES, query);
  }
  async roleDetails(params: IRoleDetails): Promise<any> {
    let { roleId } = params;
    let query = {
      _id: roleId,
      status: { $ne: STATUS.DELETED },
    };
    return await this.findOne(Models.ROLES, query, {}, {},{
      path: 'modules.moduleId',
      model: 'modules'
    });
  }
  async getAllRoles(params: IGetAllRoles): Promise<any> {
    let {search,limit=0,pageNo}=params;
    let query:any={
        status:{$ne:STATUS.DELETED}
    }
    if(search)
    query["$or"]= [
        { role: search }, 
        { description:search }
    ];

    let roles = await this.find(Models.ROLES, query, {}, {},{},{pageNo,limit},{
      path: 'modules.moduleId',
      model: 'modules'
    });
    let count = await this.count(Models.ROLES, query);
    return { roles, count };
  }
}
export const rolesDao = new RolesDao();
