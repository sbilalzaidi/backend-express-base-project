import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import {
  ICheckPromoCodeExists,
  IPromoCodeDetails,
  ICreatePromoCode,
  IUpdatePromoCode,
  IPromoCodes,
  IWritePromoCodes,
  IReadPromoCodes,
} from "../interfaces/promoCodes";
export class PromoCodeDao<T>
  extends BaseDao
  implements IReadPromoCodes<T>, IWritePromoCodes<T>
{
  async checkPromoCodeExists(params: ICheckPromoCodeExists): Promise<any> {
    let { promoCode, promoCodeId } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (promoCode) query.promoCode = promoCode;
    if (promoCodeId) query._id = { $ne: promoCodeId };
    return await this.findOne(Models.PROMO_CODES, query, {}, {}, {});
  }
  async createPromoCode(params: ICreatePromoCode): Promise<any> {
    return await this.save(Models.PROMO_CODES, params);
  }
  async updatePromoCode(params: IUpdatePromoCode): Promise<any> {
    let { promoCodeId } = params;
    let query = {
      _id: promoCodeId,
    };
    let update = {
      $set: params,
    };
    return await this.updateOne(Models.PROMO_CODES, query, update, {});
  }
  async promoCodeDetails(params: IPromoCodeDetails): Promise<any> {
    let { promoCodeId } = params;
    let query = {
      _id: promoCodeId,
      status: { $ne: STATUS.DELETED },
    };
    return await this.findOne(
      Models.PROMO_CODES,
      query,
      {},
      {},
      {
        path: "userIds",
        model: "users",
      }
    );
  }
  async promoCodes(params: IPromoCodes): Promise<any> {
    let { search, limit = 0, pageNo } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (search) query["$or"] = [{ promoCode: search }];

    let promoCodes = await this.find(
      Models.PROMO_CODES,
      query,
      {},
      {},
      {},
      { pageNo, limit },
      {
        path: "userIds",
        model: "users",
      }
    );
    let count = await this.count(Models.PROMO_CODES, query);
    return { promoCodes, count };
  }
}
export const promoCodeDao = new PromoCodeDao();
