import { ObjectId } from "mongoose";
import { STATUS } from "../../../../enums/enums";
export interface ICheckPromoCodeExists {
    promoCode?:string;
    promoCodeId?:ObjectId
}
export interface IPromoCodeDetails {
    promoCodeId:string
}
export interface ICreatePromoCode{
    adminId: ObjectId;
    userIds?: ObjectId[];
    promoCode:string;
    perUserFrequency?: number;
    totalFrequency?: number;
    startDate?: Date;
    expiryDate?: Date;
    status: STATUS.ACTIVE | STATUS.IN_ACTIVATED | STATUS.DELETED;
}
export interface IUpdatePromoCode{
    promoCodeId:ObjectId;
    adminId: ObjectId;
    userIds?: ObjectId[];
    promoCode:string;
    perUserFrequency?: number;
    totalFrequency?: number;
    startDate?: Date;
    expiryDate?: Date;
    status: STATUS.ACTIVE | STATUS.IN_ACTIVATED | STATUS.DELETED;
}
export interface IPromoCodes{
    search?:string;
    limit:number;
    pageNo:number;
}
export interface IReadPromoCodes<T> {
    checkPromoCodeExists(params:ICheckPromoCodeExists): Promise<any>;
    promoCodeDetails(params:IPromoCodeDetails): Promise<any>;
    promoCodes(params:IPromoCodes): Promise<any>;
}
export interface IWritePromoCodes<T> {
    createPromoCode(params:ICreatePromoCode): Promise<any>;
    updatePromoCode(params:IUpdatePromoCode): Promise<any>;
}

