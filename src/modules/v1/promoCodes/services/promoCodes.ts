import MESSAGES from "../../../../messages/messages";
import { promoCodeDao } from "../dao/promoCodes";
import {
  ICheckPromoCodeExists,
  IPromoCodeDetails,
  ICreatePromoCode,
  IUpdatePromoCode,
  IPromoCodes,
} from "../interfaces/promoCodes";
/**
 *
 * @param {*} params
 * @description for cheking promo code exists or not
 * @returns
 */
const checkPromoCodeExists = async (params: ICheckPromoCodeExists) => {
  let promoCodeExists = await promoCodeDao.checkPromoCodeExists(params);
  if (promoCodeExists) throw new Error(MESSAGES.PROMO_CODE_ALREADY_EXISTS);
  return true;
};

/**
 *
 * @param {*} params
 * @description for create promo code
 * @returns
 */
const createPromoCode = async (params: ICreatePromoCode) => {
  return await promoCodeDao.createPromoCode(params);
};
/**
 *
 * @param {*} params
 * @description for update promo code
 * @returns
 */
const updatePromoCode = async (params: IUpdatePromoCode) => {
  return await promoCodeDao.updatePromoCode(params);
};
/**
 *
 * @param {*} params
 * @description for promo code
 * @returns
 */
const promoCodeDetails = async (params: IPromoCodeDetails) => {
  return await promoCodeDao.promoCodeDetails(params);
};
/**
 *
 * @param {*} params
 * @description for promo code
 * @returns
 */
const getAllPromoCode = async (params: IPromoCodes) => {
  return await promoCodeDao.promoCodes(params);
};
export {
  checkPromoCodeExists,
  createPromoCode,
  updatePromoCode,
  promoCodeDetails,
  getAllPromoCode,
};
