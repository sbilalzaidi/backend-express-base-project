import { Router } from "express";
const router = Router();
import { adminAuthorization } from "../../common/services/authorization";
import { sanitizeRequest } from "../../../../middlewares/sanitizeRequest";
import {
  validateAddPromoCode,
  validateUpdatePromoCode,
  validatePromoCode,
  validatePromoCodes,
} from "../validaters/promoCodes";
import {
  addPromoCode,
  editPromoCode,
  getPromoCodeDetails,
  getPromoCodes,
} from "../controllers/promoCodes";
/*
PROMO CODES API'S
*/
router.post(
  "/",
  validateAddPromoCode,
  adminAuthorization,
  sanitizeRequest,
  addPromoCode
);
router.put(
  "/",
  validateUpdatePromoCode,
  adminAuthorization,
  sanitizeRequest,
  editPromoCode
);
router.get(
  "/:promoCodeId",
  validatePromoCode,
  adminAuthorization,
  getPromoCodeDetails
);
router.get("/", validatePromoCodes, adminAuthorization, getPromoCodes);
export default router;
