import { Request, Response, NextFunction } from "express";
import joi from "joi";
import CONSTANT from "../../../../constant/constant";
import { STATUS } from "../../../../enums/enums";
import { validateSchema } from "../../../../lib/universal-function";
/**
 *
 * @param {*} params
 * @description for create promo code
 * @returns
 */
const validateAddPromoCode = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      promoCode: joi.string().required(),
      userIds: joi
        .array()
        .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
        .optional(),
      perUserFrequency: joi.number().min(0).max(1000).required(),
      totalFrequency: joi.number().min(0).max(1000).required(),
      startDate: joi.date().optional(),
      expiryDate: joi.date().optional(),
      status: joi
        .string()
        .valid(STATUS.DELETED, STATUS.ACTIVE, STATUS.IN_ACTIVATED)
        .optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};

/**
 *
 * @param {*} params
 * @description for update promo code
 * @returns
 */
const validateUpdatePromoCode = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      promoCodeId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
      promoCode: joi.string().optional(),
      userIds: joi
        .array()
        .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
        .optional(),
      perUserFrequency: joi.number().min(0).max(1000).optional(),
      totalFrequency: joi.number().min(0).max(1000).optional(),
      startDate: joi.date().optional(),
      expiryDate: joi.date().optional(),
      status: joi
        .string()
        .valid(STATUS.DELETED, STATUS.ACTIVE, STATUS.IN_ACTIVATED)
        .optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for get promo code
 * @returns
 */
const validatePromoCode = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      promoCodeId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
    });
    await validateSchema(req.params, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for get promo codes
 * @returns
 */
const validatePromoCodes = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      search: joi.string().optional(),
      limit: joi.number().min(1).max(100).required(),
      pageNo: joi.number().min(1).required(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};

export {
  validateAddPromoCode,
  validateUpdatePromoCode,
  validatePromoCode,
  validatePromoCodes,
};
