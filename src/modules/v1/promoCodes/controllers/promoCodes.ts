import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse } from "../../../../lib/universal-function";
import {
  checkPromoCodeExists,
  createPromoCode,
  updatePromoCode,
  promoCodeDetails,
  getAllPromoCode,
} from "../services/promoCodes";
import { IPromoCodes } from "../interfaces/promoCodes";
/**
 *
 * @param {*} params
 * @description for creating promo code
 * @returns
 */
const addPromoCode = asyncHandler(async (req: Request, res: Response) => {
  await checkPromoCodeExists({ ...req.body });
  await createPromoCode({ ...req.body });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.PROMO_CODE_CREATE_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for edit promo code
 * @returns
 */
const editPromoCode = asyncHandler(async (req: Request, res: Response) => {
  await checkPromoCodeExists({ ...req.body });
  await updatePromoCode({ ...req.body });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.PROMO_CODE_UPDATED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for promo code details
 * @returns
 */
const getPromoCodeDetails = asyncHandler(
  async (req: Request, res: Response) => {
    let { promoCodeId } = req.params;
    let promoCode = await promoCodeDetails({promoCodeId });
    return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, promoCode);
  }
);
/**
 *
 * @param {*} params
 * @description for promo codes
 * @returns
 */
const getPromoCodes = asyncHandler(async (req: Request, res: Response) => {
  let { search, limit, pageNo } = req.query as unknown as IPromoCodes;
  let promoCodes = await getAllPromoCode({ search, limit, pageNo });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, promoCodes);
});

export { addPromoCode, editPromoCode, getPromoCodeDetails, getPromoCodes };
