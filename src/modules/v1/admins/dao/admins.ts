import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import {
  IWriteAdmin,
  IReadAdmin,
  ICheckAdminExists,
  IProfiles,
} from "../interfaces/admins";
export class AdminDao<T>
  extends BaseDao
  implements IReadAdmin<T>, IWriteAdmin<T>
{
  async checkAdminExists(params: ICheckAdminExists): Promise<any> {
    let { email, phoneNo, adminId } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    query["$or"] = [];
    if (email) query["$or"].push({ email: email });
    if (phoneNo) query["$or"].push({ phoneNo: phoneNo });
    if (adminId) query._id = { $ne: Object(adminId) };
    return await this.findOne(Models.ADMIN, query, {}, {}, {});
  }
  async adminDetails(params: any): Promise<any> {
    let { adminId,adminType } = params;
    let query:any = {
      _id: adminId,
      status: { $ne: STATUS.DELETED },
    };
    if(adminType)
    query.adminType=adminType;
    return await this.findOne(Models.ADMIN, query, {}, {}, {});
  }
  async createAdminUser(params: any): Promise<any> {
    return await this.save(Models.ADMIN, params);
  }
  async updateAdminDetails(params: any): Promise<any> {
    let { adminId, parentId, adminType } = params;
    let query = {
      _id: adminId,
      ...(parentId && { parentId: parentId }),
      ...(adminType && { adminType: adminType }),
      status: { $ne: STATUS.DELETED },
    };
    let update = {
      $set: params,
    };
    return await this.updateOne(Models.ADMIN, query, update, {});
  }
  async profiles(params: IProfiles): Promise<any> {
    let { search, limit = 0, pageNo=1,startDate,endDate,adminType } = params;
    let pageSize=limit+1;
    let nextPage=0;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (search)
      query["$or"] = [
        { firstName: search },
        { email: search },
        { phoneNo: search },
      ];

      if(startDate){
        query.createdAt={
          $gt:new Date(startDate)
        }
      }
      if(endDate){
        query.createdAt={
          $lte:new Date(endDate)
        }
      }
      if(adminType)
      query.adminType=adminType;

    let profilesData = await this.find(
      Models.ADMIN,
      query,
      {},
      {},
      {},
      { pageNo, pageSize },
      {}
    );
    if(profilesData.length>limit){
      profilesData.pop();
      nextPage=1;
    }
   
    return { profiles: profilesData, nextPage };
  }
  async stores(
    storeIds: string[],
    regionIds: string[] | [],
    longitude: number | 0,
    latitude: number | 0,
    day:number | 0,
    time:number | 0
  ): Promise<any> {
    let pipeline = [
      {
        $geoNear: {
          near: {
            type: "Point",
            coordinates: [longitude, latitude],
          },
          maxDistance: 10000, // 10km
          spherical: true,
          distanceField: "calcDistance",
        },
      },
      {
        $match: {
          _id: { $in: storeIds },
          status: { $ne: STATUS.DELETED },
        },
      },
      {
        $match: {
          $or: [
            {
              "store.enableServiceRadius": STATUS.ACTIVE,
              calcDistance: { $lte: "$store.serviceRadius" },
            },
            {
              "store.enableRegions": STATUS.ACTIVE,
              "store.regionIds": { $in: regionIds },
            },
            {
              "store.enableShift": STATUS.ACTIVE,
              "store.shift": { 
                day:day, 
                open:{
                  $gte:time
                },
                closed:{
                  $lte:time
                }

              },
            }
          ],
        },
      },
      {
        $project:{
          storeId:'$_id',
          store:1
        }
      }
    ];
    return await this.aggregate(Models.ADMIN, pipeline, {});
  }
}
export const adminDao = new AdminDao();
