import { Router } from "express";
const router = Router();
import {uploads} from '../../../../services/multer'
import {
  captcha,
  genrateRefreshToken,
  login,
  logout,
  profile,
  updateProfile,
  changePassword,
  sendOtp,
  verifyOtp,
  sendVerifyEmail,
  verifyEmail,
  forgotPassword,
  resetPassword,
  uploadDoc,
  uploadProfileImage,
  addAdminUser,
  updateAdminUserProfile,
  adminUserProfile,
  adminUsers,
  addCompany,
  signUpCompany,
  updateCompany,
  company,
  companies,
  addCompanyUser,
  updateCompanyUser,
  companyUser,
  companyUsers,
  updateStoreInfo
} from "../controllers/admins";
import {
  adminDashboard,
  adminMessages,
  adminContacts,
  adminDiscussions,
  adminNotifications,
  adminLocations,
} from "../controllers/config";
import {
  validateLogin,
  validateLogout,
  validateProfile,
  validateUpdateProfile,
  validateChangePassword,
  validateSendOtp,
  validateVerifyOtp,
  validateSendVerifyEmail,
  validateVerifyEmail,
  validateForgotPassword,
  validateResetPassword,
  validateAdminIdProfile,

  validateAddAdminUser,
  validateUpdateAdminUserProfile,
  validateAdminUsers,
  validateAddComany,
  validateSiginUpComany,
  validateUpdateCompany,
  validateCompanies,
  validateAddCompanyUser,
  validateUpdateCompanyUser,
  validateCompanyUsers,
  validateUpdateStoreInfo

} from "../validaters/admins";
import { adminAuthorization } from "../../common/services/authorization";

router.get("/config", adminDashboard);
router.get("/messages", adminMessages);
router.get("/discussions", adminDiscussions);
router.get("/notifications", adminNotifications);
router.get("/contacts", adminContacts);
router.get("/locations", adminLocations);

router.get("/captcha", captcha);
router.get("/refreshToken",adminAuthorization,genrateRefreshToken)
router.put("/login", validateLogin, login);
router.put("/logout", validateLogout, adminAuthorization, logout);
router.get("/profile", validateProfile, adminAuthorization, profile);
router.put("/", validateUpdateProfile, adminAuthorization, updateProfile);
router.put(
  "/changePassword",
  validateChangePassword,
  adminAuthorization,
  changePassword
);
router.put(
  "/changePhoneNo/sendOtp",
  validateSendOtp,
  adminAuthorization,
  sendOtp
);
router.put("/changePhoneNo/verifyOtp", validateVerifyOtp, verifyOtp);
router.put(
  "/changeEmail/sendVerifyEmail",
  validateSendVerifyEmail,
  adminAuthorization,
  sendVerifyEmail
);
router.put("/changeEmail/verifyEmail", validateVerifyEmail, verifyEmail);
router.put("/forgotPassword", validateForgotPassword, forgotPassword);
router.put("/resetPassword", validateResetPassword, resetPassword);

/*
FILE UPLOAD API'S
*/
router.post("/upload/doc", uploads.user.single('image'), adminAuthorization, uploadDoc);
router.post(
  "/upload/profile",
  uploads.user.single('image'),
  adminAuthorization,
  uploadProfileImage
);

/**
 * ADMIN USERS
 */
router.post("/users", validateAddAdminUser, adminAuthorization, addAdminUser);
router.put(
  "/users/:adminId",
  validateAdminIdProfile,
  validateUpdateAdminUserProfile,
  adminAuthorization,
  updateAdminUserProfile
);
router.get(
  "/users/:adminId",
  validateAdminIdProfile,
  adminAuthorization,
  adminUserProfile
);
router.get("/users", validateAdminUsers, adminAuthorization, adminUsers);
/**
 * ADMIN COMPANIES
 */
router.post("/comanies", validateAddComany, adminAuthorization,addCompany);
router.post("/comanies/signUp", validateSiginUpComany, adminAuthorization,signUpCompany);
router.put(
  "/comanies/:adminId",
  validateAdminIdProfile,
  validateUpdateCompany,
  adminAuthorization,
  updateCompany
);
router.get(
  "/comanies/:adminId",
  validateAdminIdProfile,
  adminAuthorization,
  company
);
router.get("/comanies", validateCompanies, adminAuthorization, companies);
/**
 * ADMIN COMPANY USERS
 */
router.post(
  "/comanies/users",
  validateAddCompanyUser,
  adminAuthorization,
  addCompanyUser
);
router.put(
  "/comanies/users/:adminId",
  validateAdminIdProfile,
  validateUpdateCompanyUser,
  adminAuthorization,
  updateCompanyUser
);
router.get(
  "/comanies/users/:adminId",
  validateAdminIdProfile,
  adminAuthorization,
  companyUser
);
router.get("/comanies/users", validateCompanyUsers, adminAuthorization, companyUsers);
/**
 * ADMIN STORE
 */
router.put(
  "/store:adminid",
  validateUpdateStoreInfo,
  adminAuthorization,
  updateStoreInfo
);
export default router;
