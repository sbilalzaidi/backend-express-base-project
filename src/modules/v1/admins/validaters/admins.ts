import { Request, Response, NextFunction } from "express";
import joi from "joi";
import { validateSchema } from "../../../../lib/universal-function";
import CONSTANT from "../../../../constant/constant";
import { STATUS, DISCOUNT_TYPE } from "../../../../enums/enums";
import {
  translationSchemaFn
} from "../../common/validators/schema";
import {shiftSchema,locationSchema} from "../../common/validators/schema"
const validateLogin = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      email: joi.string().email().required(),
      password: joi.string().required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateLogout = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      isLogoutAllDevice: joi.boolean().optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateProfile = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({});
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateUpdateProfile = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      roleId: joi.array().items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional()).optional(),
      firstName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      lastName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      profilePic: joi.object().keys({
        original: joi.string().allow("").optional(),
        thumbNail: joi.string().allow("").optional(),
      }),
      addresses: joi
        .array()
        .items(
          joi.object().keys({
            addressType: joi.string().allow("").optional(),
            country: joi.string().allow("").optional(),
            state: joi.string().allow("").optional(),
            city: joi.string().allow("").optional(),
            address: joi.string().allow("").optional(),
            latitude: joi.number().optional(),
            longitude: joi.number().optional(),
            isDefault: joi.boolean().optional(),
          })
        )
        .optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateChangePassword = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      oldPassword: joi.string().required(),
      newPassword: joi.string().required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateSendOtp = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      phoneNo: joi
        .string()
        .regex(/^[0-9]{5,}$/)
        .required(),
      countryCode: joi
        .string()
        .regex(/^[0-9+]{1,}$/)
        .required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateVerifyOtp = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      otpCode: joi.string().required(),
      phoneNo: joi
        .string()
        .regex(/^[0-9]{5,}$/)
        .required(),
      countryCode: joi
        .string()
        .regex(/^[0-9+]{1,}$/)
        .required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateSendVerifyEmail = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({});
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateVerifyEmail = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      verificationEmailToken: joi.string().required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateForgotPassword = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      email: joi.string().email().required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateResetPassword = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      passwordResetToken: joi.string().required(),
      newPassword: joi.string().required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateAdminIdProfile = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      adminId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
    });
    await validateSchema(req.params, schema);
    next();
  } catch (error) {
    next(error);
  }
};

const validateAddAdminUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      roleId: joi.array().items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional()).optional(),
      firstName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      lastName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      email: joi.string().email().required(),
      phoneNo: joi
        .string()
        .regex(/^[0-9]{5,}$/)
        .required(),
      countryCode: joi
        .string()
        .regex(/^[0-9+]{1,}$/)
        .required(),
      password: joi.string().required(),
      profilePic: joi.object().keys({
        original: joi.string().allow("").optional(),
        thumbNail: joi.string().allow("").optional(),
      }),
      addresses: joi
        .array()
        .items(
          joi.object().keys({
            addressType: joi.string().allow("").optional(),
            country: joi.string().allow("").optional(),
            state: joi.string().allow("").optional(),
            city: joi.string().allow("").optional(),
            address: joi.string().allow("").optional(),
            latitude: joi.number().optional(),
            longitude: joi.number().optional(),
            isDefault: joi.boolean().optional(),
          })
        )
        .optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateUpdateAdminUserProfile = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      roleId: joi.array().items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional()).optional(),
      firstName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      lastName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      profilePic: joi.object().keys({
        original: joi.string().allow("").optional(),
        thumbNail: joi.string().allow("").optional(),
      }).optional(),
      addresses: joi
        .array()
        .items(
          joi.object().keys({
            addressType: joi.string().allow("").optional(),
            country: joi.string().allow("").optional(),
            state: joi.string().allow("").optional(),
            city: joi.string().allow("").optional(),
            address: joi.string().allow("").optional(),
            latitude: joi.number().optional(),
            longitude: joi.number().optional(),
            isDefault: joi.boolean().optional(),
          })
        )
        .optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateAdminUsers = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      search: joi.string().optional(),
      startDate:joi.date().optional(),
      endDate:joi.date().optional(),
      limit: joi.number().min(1).max(100).required(),
      pageNo: joi.number().min(1).required(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};

const validateAddComany = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      captcha:joi.string().required(),
      roleId: joi.array().items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional()).optional(),
      firstName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      lastName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      email: joi.string().email().required(),
      phoneNo: joi
        .string()
        .regex(/^[0-9]{5,}$/)
        .required(),
      countryCode: joi
        .string()
        .regex(/^[0-9+]{1,}$/)
        .required(),
      profilePic: joi.object().keys({
        original: joi.string().allow("").optional(),
        thumbNail: joi.string().allow("").optional(),
      }),
      addresses: joi
        .array()
        .items(
          joi.object().keys({
            addressType: joi.string().allow("").optional(),
            country: joi.string().allow("").optional(),
            state: joi.string().allow("").optional(),
            city: joi.string().allow("").optional(),
            address: joi.string().allow("").optional(),
            latitude: joi.number().optional(),
            longitude: joi.number().optional(),
            isDefault: joi.boolean().optional(),
          })
        )
        .optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateSiginUpComany = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      roleId: joi.array().items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional()).optional(),
      firstName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      lastName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      email: joi.string().email().required(),
      phoneNo: joi
        .string()
        .regex(/^[0-9]{5,}$/)
        .required(),
      countryCode: joi
        .string()
        .regex(/^[0-9+]{1,}$/)
        .required(),
      password: joi.string().required(),
      profilePic: joi.object().keys({
        original: joi.string().allow("").optional(),
        thumbNail: joi.string().allow("").optional(),
      }),
      addresses: joi
        .array()
        .items(
          joi.object().keys({
            addressType: joi.string().allow("").optional(),
            country: joi.string().allow("").optional(),
            state: joi.string().allow("").optional(),
            city: joi.string().allow("").optional(),
            address: joi.string().allow("").optional(),
            latitude: joi.number().optional(),
            longitude: joi.number().optional(),
            isDefault: joi.boolean().optional(),
          })
        )
        .optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateUpdateCompany = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      roleId: joi.array().items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional()).optional(),
      firstName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      lastName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      email: joi.string().email().required(),
      phoneNo: joi
        .string()
        .regex(/^[0-9]{5,}$/)
        .required(),
      countryCode: joi
        .string()
        .regex(/^[0-9+]{1,}$/)
        .required(),
      password: joi.string().required(),
      profilePic: joi.object().keys({
        original: joi.string().allow("").optional(),
        thumbNail: joi.string().allow("").optional(),
      }),
      addresses: joi
        .array()
        .items(
          joi.object().keys({
            addressType: joi.string().allow("").optional(),
            country: joi.string().allow("").optional(),
            state: joi.string().allow("").optional(),
            city: joi.string().allow("").optional(),
            address: joi.string().allow("").optional(),
            latitude: joi.number().optional(),
            longitude: joi.number().optional(),
            isDefault: joi.boolean().optional(),
          })
        )
        .optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateCompanies = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      search: joi.string().optional(),
      startDate:joi.date().optional(),
      endDate:joi.date().optional(),
      limit: joi.number().min(1).max(100).required(),
      pageNo: joi.number().min(1).required(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};

const validateAddCompanyUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      parentId:joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
      roleId: joi.array().items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional()).optional(),
      firstName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      lastName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      email: joi.string().email().required(),
      phoneNo: joi
        .string()
        .regex(/^[0-9]{5,}$/)
        .required(),
      countryCode: joi
        .string()
        .regex(/^[0-9+]{1,}$/)
        .required(),
      password: joi.string().required(),
      profilePic: joi.object().keys({
        original: joi.string().allow("").optional(),
        thumbNail: joi.string().allow("").optional(),
      }),
      addresses: joi
        .array()
        .items(
          joi.object().keys({
            addressType: joi.string().allow("").optional(),
            country: joi.string().allow("").optional(),
            state: joi.string().allow("").optional(),
            city: joi.string().allow("").optional(),
            address: joi.string().allow("").optional(),
            latitude: joi.number().optional(),
            longitude: joi.number().optional(),
            isDefault: joi.boolean().optional(),
          })
        )
        .optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateUpdateCompanyUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      roleId: joi.array().items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional()).optional(),
      firstName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      lastName: joi
        .string()
        .regex(/^[a-zA-Z ]+$/)
        .optional(),
      email: joi.string().email().required(),
      phoneNo: joi
        .string()
        .regex(/^[0-9]{5,}$/)
        .required(),
      countryCode: joi
        .string()
        .regex(/^[0-9+]{1,}$/)
        .required(),
      password: joi.string().required(),
      profilePic: joi.object().keys({
        original: joi.string().allow("").optional(),
        thumbNail: joi.string().allow("").optional(),
      }),
      addresses: joi
        .array()
        .items(
          joi.object().keys({
            addressType: joi.string().allow("").optional(),
            country: joi.string().allow("").optional(),
            state: joi.string().allow("").optional(),
            city: joi.string().allow("").optional(),
            address: joi.string().allow("").optional(),
            latitude: joi.number().optional(),
            longitude: joi.number().optional(),
            isDefault: joi.boolean().optional(),
          })
        )
        .optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
const validateCompanyUsers = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      search: joi.string().optional(),
      startDate:joi.date().optional(),
      endDate:joi.date().optional(),
      limit: joi.number().min(1).max(100).required(),
      pageNo: joi.number().min(1).required(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for update store info
 * @returns
 */
const validateUpdateStoreInfo = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      adminId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
      shopName: joi.object(translationSchemaFn()).optional(),
      country: joi.string().allow("").optional(),
      state: joi.string().allow("").optional(),
      city: joi.string().allow("").optional(),
      address: joi.string().allow("").optional(),
      latitude: joi.number().optional(),
      longitude: joi.number().optional(),
      images: joi.array().items(joi.string().required()).optional(),
      icons: joi.array().items(joi.string().required()).optional(),
      banners: joi.array().items(joi.string().required()).optional(),
      enableRegions: joi
        .number()
        .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED)
        .optional(),
      regionIds: joi
        .array()
        .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
        .optional(),
      enableShift: joi
        .string()
        .valid(STATUS.DEFAULT, STATUS.ACTIVE, STATUS.IN_ACTIVATED)
        .optional(),
      shift: joi.array().items(shiftSchema).optional(),
      enableGoogleMerchantCenter: joi
        .number()
        .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED)
        .optional(),
      enableFacebookMerchantCenter: joi
        .number()
        .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED)
        .optional(),
      autoApprovedEnabled: joi
        .number()
        .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED)
        .optional(),
      serviceRadius: joi.number().min(1).max(10000).optional(),
      enableServiceRadius: joi
        .number()
        .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED)
        .optional(),
      location: locationSchema.optional(),
      enableInventory:joi
      .number()
      .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED)
      .optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
export {
  validateLogin,
  validateLogout,
  validateProfile,
  validateUpdateProfile,
  validateChangePassword,
  validateSendOtp,
  validateVerifyOtp,
  validateSendVerifyEmail,
  validateVerifyEmail,
  validateForgotPassword,
  validateResetPassword,
  validateAdminIdProfile,

  validateAddAdminUser,
  validateUpdateAdminUserProfile,
  validateAdminUsers,
  validateAddComany,
  validateSiginUpComany,
  validateUpdateCompany,
  validateCompanies,
  validateAddCompanyUser,
  validateUpdateCompanyUser,
  validateCompanyUsers,
  validateUpdateStoreInfo

};
