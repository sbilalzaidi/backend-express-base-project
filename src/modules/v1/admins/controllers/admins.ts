import { Request, Response } from "express";
import SERVER from "../../../../config/environment";
import asyncHandler from "../../../../middlewares/asyncHandler";
import CONSTANT from "../../../../constant/constant";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { STATUS,ADMIN_TYPE, APPROVAL_STATUS } from "../../../../enums/enums";
import {
  sendResponse,
  genrateCaptcha,
  generateComplexPassword,
} from "../../../../lib/universal-function";
import {
  availabilityCheckEmailPhoneNo,
  checkAdminExists,
  createAdmin,
  genrateAdminAuthToken,
  adminSendOtp,
  adminVerifyOtp,
  adminSendEmailLink,
  adminVerifyVerificationEmailLink,
  profiles,
  sendPasswordEmail,
} from "../services/admins";
import { deleteSessions } from "../../common/services/session";
import { adminDao } from "../dao/admins";
import {
  uploadImageToS3Bucket,
  uploadOriginalImageToS3Bucket,
} from "../../../../services/s3";
import { getData, deleteData,setDataWithExpire } from "../../../../services/redis";
import { IProfiles } from "../interfaces/admins";

const captcha = asyncHandler(async (req: Request, res: Response) => {
  let { deviceid:deviceId } = req.headers;
  if(!deviceId)
  throw new Error(MESSAGES.DEVICE_ID_REQUIRED);
  let captchaDataUrl=await getData(SERVER.CAPTCHA_KEY+'-dataUrl-'+deviceId);
  if(!captchaDataUrl)
  captchaDataUrl=await genrateCaptcha();
  setDataWithExpire(SERVER.CAPTCHA_KEY+'-dataUrl-'+deviceId,captchaDataUrl?.dataURL,3600)
  setDataWithExpire(SERVER.CAPTCHA_KEY+'-text-'+deviceId,captchaDataUrl?.text,3600)
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {captchaDataUrl:captchaDataUrl});
});

const genrateRefreshToken = asyncHandler(async (req: Request, res: Response) => {
  const {adminId} =req.user;
  let { devicetype: deviceType, deviceName: deviceName } = req.headers;
  let ip = req.ip;
  let admin = await adminDao.adminDetails({ adminId});
  let tokenData = await genrateAdminAuthToken({
    ...admin,
    ip,
    deviceType,
    deviceName,
  });
  admin = { ...admin, ...tokenData };
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, admin);
});
/**
 *
 * @param {*} params
 * @description for login
 * @returns
 */
const login = asyncHandler(async (req: Request, res: Response) => {
  let { devicetype: deviceType, deviceName: deviceName } = req.headers;
  let ip = req.ip;
  let { email, password } = req.body;
  let admin = await checkAdminExists({ email });
 // debugger
  await admin.authenticate(password);
  admin = admin.toJSON();
  let tokenData = await genrateAdminAuthToken({
    ...admin,
    ip,
    deviceType,
    deviceName,
  });
  admin = { ...admin, ...tokenData };
  if (admin.isPhoneNoVerified !=APPROVAL_STATUS.APPROVED) {
    adminSendOtp({...admin});
  }
  if (admin.isEmailVerified !=APPROVAL_STATUS.APPROVED) {
    adminSendEmailLink(admin);
  }
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, admin);
});
const logout = asyncHandler(async (req: Request, res: Response) => {
  let { deviceId, adminId }: any = req.user;
  await deleteSessions(Models.ADMIN_SESSIONS, {
    deviceId,
    adminId,
    ...req.body,
  });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.LOGOUT_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for profile
 * @returns
 */
const profile = asyncHandler(async (req: Request, res: Response) => {
  let { adminId }: any = req.user;
  let admin = await adminDao.adminDetails({ adminId });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, admin);
});
/**
 *
 * @param {*} params
 * @description for update profile
 * @returns
 */
const updateProfile = asyncHandler(async (req: Request, res: Response) => {
  let { deviceId, adminId, parentId, ip, deviceType, deviceName,adminType }: any =
  req.user;
  let admin = await adminDao.updateAdminDetails({
    ...req.body,
    adminId,
  });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.PROFILE_UPDATED, admin);
});
/**
 *
 * @param {*} params
 * @description for change password
 * @returns
 */
const changePassword = asyncHandler(async (req: Request, res: Response) => {
  let { deviceId, adminId }: any = req.user;
  let { oldPassword, newPassword } = req.body;
  const admin = await adminDao.adminDetails({ adminId });
  await admin.authenticate(oldPassword);
  await admin.setPassword(newPassword);
  await admin.save();
  await deleteSessions(Models.ADMIN_SESSIONS, {
    adminId: admin.adminId,
    excludeDeviceId:deviceId,
    isLogoutAllDevice: true,
  });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.PASSWORD_CHANGED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for send OTP
 * @returns
 */
const sendOtp = asyncHandler(async (req: Request, res: Response) => {
  let { phoneNo, countryCode }: any = req.body;
  let { adminId }: any = req.user;
  await availabilityCheckEmailPhoneNo({
    adminId,
    phoneNo,
    isSignUpRequest: true,
  });
  await adminSendOtp({ adminId, phoneNo: phoneNo, countryCode: countryCode });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.SEND_OTP_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for verify OTP
 * @returns
 */
const verifyOtp = asyncHandler(async (req: Request, res: Response) => {
  let { otpCode, phoneNo, countryCode } = req.body;
  const admin = await adminVerifyOtp({ otpCode, phoneNo });
  let update: any = {
    isPhoneNoVerified: APPROVAL_STATUS.APPROVED,
    phoneNo,
    countryCode,
  };
  if (admin.isEmailVerified ==APPROVAL_STATUS.APPROVED) update.status = STATUS.ACTIVE;
  await availabilityCheckEmailPhoneNo({
    adminId: admin?.adminId,
    phoneNo,
    isSignUpRequest: true,
  });
  await adminDao.updateAdminDetails({ ...update, adminId: admin?.adminId });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.OTP_VERIFIED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for send verify email
 * @returns
 */
const sendVerifyEmail = asyncHandler(async (req: Request, res: Response) => {
  let { adminId }: any = req.user;
  let admin = await adminDao.adminDetails({ adminId });
  admin = admin.toJSON();
  if (!admin) throw new Error(MESSAGES.USER_NOT_FOUND);
  if (admin.status == STATUS.IN_ACTIVATED)
    throw new Error(MESSAGES.USER_IS_BLOCKED);
  if (admin.isEmailVerified !=APPROVAL_STATUS.APPROVED && admin?.email) {
    adminSendEmailLink({ adminId: adminId, email: admin?.email });
  }
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.SEND_EMAIL_VERIFICATION_LINK,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for verify email
 * @returns
 */
const verifyEmail = asyncHandler(async (req: Request, res: Response) => {
  let { verificationEmailToken } = req.body;
  let emailVerifyData = await adminVerifyVerificationEmailLink({
    verificationEmailToken,
  });
  if (!emailVerifyData || (emailVerifyData && !emailVerifyData.adminId))
    throw new Error(MESSAGES.EXPIRE_EMAIL_VERIFICATION_LINK);
  let admin = await adminDao.adminDetails({
    adminId: emailVerifyData.adminId,
  });
  admin = admin.toJSON();
  if (!admin) throw new Error(MESSAGES.USER_NOT_FOUND);
  let update: any = {
    isEmailVerified: APPROVAL_STATUS.APPROVED,
  };
  if (admin.isPhoneNoVerified ==APPROVAL_STATUS.APPROVED) {
    update.status = STATUS.ACTIVE;
  }
  await adminDao.updateAdminDetails({
    ...update,
    adminId: admin.adminId,
  });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.EMAIL_VERIFIED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for forgot password
 * @returns
 */
const forgotPassword = asyncHandler(async (req: Request, res: Response) => {
  let { email } = req.body;
  let prefix = CONSTANT.PREFIX.FORGOT_PASSWORD;
  let admin = await checkAdminExists({ email });
  admin = admin.toJSON();
  if (!admin) throw new Error(MESSAGES.USER_NOT_FOUND);
  adminSendEmailLink({ ...admin, prefix });

  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.FORGOT_PASSWORD_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for reset password
 * @returns
 */
const resetPassword = asyncHandler(async (req: Request, res: Response) => {
  let { passwordResetToken, newPassword } = req.body;
  let prefix = CONSTANT.PREFIX.FORGOT_PASSWORD;
  let key = prefix + passwordResetToken;
  let emailData = await getData(key);
  if (!emailData) throw new Error(MESSAGES.EXPIRE_RESET_PASSWORD_LINK);
  if (!emailData) throw new Error(MESSAGES.EXPIRE_RESET_PASSWORD_LINK);
  await deleteData(key);
  let { adminId } = emailData;
  const admin = await adminDao.adminDetails({ adminId });
  await admin.setPassword(newPassword);
  await admin.save();
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.PASSWORD_CHANGED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for upload Doc
 * @returns
 */
const uploadDoc = asyncHandler(async (req: any, res: Response) => {
  let data: any = {};
  if (
    req.file &&
    req.file &&
    req.file.size &&
    req.file.size < 3072000
  ) {
    console.log("File size exceed",req.file)
    data = await uploadOriginalImageToS3Bucket(
      req.file,
      CONSTANT.S3_FOLDERS.ADMINS
    );
  }
  else
    throw new Error(MESSAGES.FILE_SIZE_EXCEED);
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, data);
});
/**
 *
 * @param {*} params
 * @description for upload profile image
 * @returns
 */
const uploadProfileImage = asyncHandler(async (req: any, res: Response) => {
  let data: any = {};
  if (
    req.file &&
    req.file &&
    req.file.size &&
    req.file.size < 3072000
  ) {
    console.log("File size exceed",req.file)
    data = await uploadImageToS3Bucket(
      req.file,
      CONSTANT.S3_FOLDERS.ADMINS
    );
  }
  else
    throw new Error(MESSAGES.FILE_SIZE_EXCEED);
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, data);
});

const addAdminUser = asyncHandler(async (req: Request, res: Response) => {
  let {adminId,parentId,adminType }: any =
    req.user;
  let params = req.body;
  let { email, phoneNo } = params;
  await availabilityCheckEmailPhoneNo({ email,phoneNo, isSignUpRequest: true });
  if(adminType==ADMIN_TYPE.ADMIN)
  parentId=adminId;
  params.adminType==ADMIN_TYPE.ADMIN_USER;
  params.status = STATUS.PENDING;
  params.password=await generateComplexPassword(8);
  const doc = await createAdmin({...params,parentId});
  await doc.setPassword(params?.password);
  await doc.save();
  await sendPasswordEmail({ email,password:params?.password});
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.ADMIN_USER_ADDED_SUCCESSFULLY,
    {}
  );
});
const updateAdminUserProfile = asyncHandler(async (req: Request, res: Response) => {
  let {  adminId }: any =
  req.params; 
  let payload = req.body;
  let admin = await adminDao.updateAdminDetails({adminId,...payload});
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.ADMIN_USER_UPDATED_SUCCESSFULLY, admin);
});
const adminUserProfile = asyncHandler(async (req: Request, res: Response) => {
  let { adminId } = req.params;
  let admin = await adminDao.adminDetails({ adminId });//adminType:ADMIN_TYPE.ADMIN_USER
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, admin);
});
const adminUsers = asyncHandler(async (req: Request, res: Response) => {
  let { search, limit, pageNo,startDate,endDate } = req.query as unknown as IProfiles;
  let profilesData = await profiles({ search, limit, pageNo,startDate,endDate });//adminType:ADMIN_TYPE.ADMIN_USER
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {
    profiles: profilesData?.profiles || [],
    count: profilesData?.count || 0,
  });
});

const addCompany = asyncHandler(async (req: Request, res: Response) => {
  let params = req.body;
  let { email, phoneNo } = params;
  await availabilityCheckEmailPhoneNo({ email,phoneNo, isSignUpRequest: true });
  params.adminType==ADMIN_TYPE.COMPANY;
  params.status = STATUS.PENDING;
  params.password=await generateComplexPassword(8);
  const doc = await createAdmin({...params});
  await doc.setPassword(params?.password);
  await doc.save();
  await sendPasswordEmail({ email,password:params?.password});
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.COMPANY_ADDED_SUCCESSFULLY,
    {}
  );
});
const signUpCompany = asyncHandler(async (req: Request, res: Response) => {
  let { devicetype: deviceType, deviceName: deviceName,deviceid:deviceId } = req.headers;
  let ip = req.ip;
  let params = req.body;
  let { email, phoneNo ,captcha} = params;
  let captchaData=await getData(SERVER.CAPTCHA_KEY+'-dataUrl-'+deviceId);
  if(captchaData !=captcha){
    throw new Error(MESSAGES.INVALID_CAPTCHA);
  }

  await availabilityCheckEmailPhoneNo({ email,phoneNo, isSignUpRequest: true });
  params.adminType==ADMIN_TYPE.COMPANY;
  params.status = STATUS.PENDING;
  params.password=await generateComplexPassword(8);
  const doc = await createAdmin({...params});
  await doc.setPassword(params?.password);
  await doc.save();
  let admin =doc.toJSON();
  let tokenData = await genrateAdminAuthToken({
    ...admin,
    ip,
    deviceType,
    deviceName,
  });
  admin = { ...admin, ...tokenData };
  adminSendOtp({...admin});
  adminSendEmailLink(admin);
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.COMPANY_ADDED_SUCCESSFULLY,
    {}
  );
});
const updateCompany = asyncHandler(async (req: Request, res: Response) => {
  let {  adminId }: any =
  req.params; 
  let params = req.body;
  let admin = await adminDao.updateAdminDetails({adminId,...params});
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.COMPANY_UPDATED_SUCCESSFULLY, admin);
});
const company = asyncHandler(async (req: Request, res: Response) => {
  let { adminId } = req.params;
  let admin = await adminDao.adminDetails({ adminId,adminType:ADMIN_TYPE.COMPANY });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, admin);
});
const companies = asyncHandler(async (req: Request, res: Response) => {
  let { search, limit, pageNo,startDate,endDate } = req.query as unknown as IProfiles;
  let profilesData = await profiles({ search, limit, pageNo,startDate,endDate ,adminType:ADMIN_TYPE.COMPANY});
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {
    profiles: profilesData?.profiles || [],
    count: profilesData?.count || 0,
  });
});
const addCompanyUser = asyncHandler(async (req: Request, res: Response) => {
  let {adminId,parentId,adminType }: any =
    req.user;
  let params = req.body;
  let { email, phoneNo } = params;
  await availabilityCheckEmailPhoneNo({ email,phoneNo, isSignUpRequest: true });
  if(adminType==ADMIN_TYPE.COMPANY)
  parentId=adminId;
  else if(adminType==ADMIN_TYPE.ADMIN || adminType==ADMIN_TYPE.ADMIN_USER){
    parentId=params.parentId;
    let company= await adminDao.adminDetails({ adminId:parentId,adminType:ADMIN_TYPE.COMPANY });
    if(!params.parentId || !company){
      throw new Error(MESSAGES.SELECT_VALID_COMPANY);
      }
  }
  params.adminType==ADMIN_TYPE.COMPANY_USER;
  params.status = STATUS.PENDING;
  params.password=await generateComplexPassword(8);
  const doc = await createAdmin({...params,parentId});
  await doc.setPassword(params?.password);
  await doc.save();
  await sendPasswordEmail({ email,password:params?.password});
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.COMPANY_USER_ADDED_SUCCESSFULLY,
    {}
  );
});
const updateCompanyUser = asyncHandler(async (req: Request, res: Response) => {
  let {  adminId }: any =
  req.params; 
  let params = req.body;
  let admin = await adminDao.updateAdminDetails({adminId,...params});
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.COMPANY_USER_UPDATED_SUCCESSFULLY, admin);
});
const companyUser = asyncHandler(async (req: Request, res: Response) => {
  let { adminId } = req.params;
  let admin = await adminDao.adminDetails({ adminId,adminType:ADMIN_TYPE.COMPANY_USER });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, admin);
});
const companyUsers = asyncHandler(async (req: Request, res: Response) => {
  let { search, limit, pageNo,startDate,endDate } = req.query as unknown as IProfiles;
  let profilesData = await profiles({ search, limit, pageNo,startDate,endDate ,adminType:ADMIN_TYPE.COMPANY_USER});
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {
    profiles: profilesData?.profiles || [],
    count: profilesData?.count || 0,
  });
});
const updateStoreInfo = asyncHandler(async (req: Request, res: Response) => {
  let {  adminId, parentId,adminType }: any =
  req.user;
  if(![ADMIN_TYPE.COMPANY,ADMIN_TYPE.ADMIN].includes(adminType)){
    throw new Error(MESSAGES.INVALID_STORE_ID);
  }
  let admin = await adminDao.updateAdminDetails({
    ...req.body,
    ...(adminType !=ADMIN_TYPE.ADMIN && {adminId:adminId}),
  });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.STROE_INFO_UPDATED_SUCCESSFULLY, admin);
});
export {
  captcha,
  genrateRefreshToken,
  login,
  logout,
  profile,
  updateProfile,
  changePassword,
  sendOtp,
  verifyOtp,
  sendVerifyEmail,
  verifyEmail,
  forgotPassword,
  resetPassword,
  uploadDoc,
  uploadProfileImage,

  addAdminUser,
  updateAdminUserProfile,
  adminUserProfile,
  adminUsers,
  addCompany,
  signUpCompany,
  updateCompany,
  company,
  companies,
  addCompanyUser,
  updateCompanyUser,
  companyUser,
  companyUsers,
  updateStoreInfo,
};
