import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import {
  sendResponse,
  getWhatsAppDate
} from "../../../../lib/universal-function";

/**
 *
 * @param {*} params
 * @description for admin dashboard
 * @returns
 */
const adminDashboard = asyncHandler(async (req: Request, res: Response) => {
  let data={
    "title":"chat",
    "favIcon":"",
    "button_background":"#2787F5",
    "button_color":"#fff",
    "aside_background":"#1e2126",
    "aside_preference_button_background":"#2787F5",
    "aside_preference_button_color":"#fff",
    "theme_background_color":"#1e2126",
    "theme_background":"#2787F5",
    "theme_color":"#1e2126",
    "theme_border":"#1e2126",
    "theme_border_bottom":"#1e2126",
    "theme_online":"#154734",
    "theme_offline":"#2787F5",
    "theme_bg_color":"#2787F5",
    "theme_brand_color":"#2787F5",
  }
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, data);
});
/**
 *
 * @param {*} params
 * @description for admin discussions
 * @returns
 */
const adminDiscussions = asyncHandler(async (req: Request, res: Response) => {
  let discussions=[
    {
      "isOnline":true,
      "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
      "name":"Bilal",
      "noOfUnreadMessages":0,
      "bgColor":"bg-yellow",
      "day":"Mon",
      "lastMmessage":"A new feature has been updated to your account. Check it out.."
  },
  {
      "isOnline":false,
      "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
      "name":"Bilal",
      "noOfUnreadMessages":1,
      "bgColor":"bg-gray",
      "day":"Mon",
      "lastMmessage":"A new feature has been updated to your account. Check it out.."
  },
  {
      "isOnline":true,
      "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
      "name":"Bilal",
      "noOfUnreadMessages":4,
      "bgColor":"bg-green",
      "day":"Mon",
      "lastMmessage":"A new feature has been updated to your account. Check it out.."
  },
  {
    "isOnline":true,
    "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
    "name":"Bilal",
    "noOfUnreadMessages":4,
    "bgColor":"bg-green",
    "day":"Mon",
    "lastMmessage":"A new feature has been updated to your account. Check it out.."
},
{
  "isOnline":true,
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Bilal",
  "noOfUnreadMessages":4,
  "bgColor":"bg-green",
  "day":"Mon",
  "lastMmessage":"A new feature has been updated to your account. Check it out.."
},
{
  "isOnline":true,
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Bilal",
  "noOfUnreadMessages":4,
  "bgColor":"bg-green",
  "day":"Mon",
  "lastMmessage":"A new feature has been updated to your account. Check it out.."
},
{
  "isOnline":true,
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Bilal",
  "noOfUnreadMessages":4,
  "bgColor":"bg-green",
  "day":"Mon",
  "lastMmessage":"A new feature has been updated to your account. Check it out.."
},
{
  "isOnline":true,
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Bilal",
  "noOfUnreadMessages":4,
  "bgColor":"bg-green",
  "day":"Mon",
  "lastMmessage":"A new feature has been updated to your account. Check it out.."
},
{
  "isOnline":true,
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Bilal",
  "noOfUnreadMessages":4,
  "bgColor":"bg-green",
  "day":"Mon",
  "lastMmessage":"A new feature has been updated to your account. Check it out.."
}
  ]
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {
    discussions:discussions,
    nextHit:1
  });
});
/**
 *
 * @param {*} params
 * @description for admin notifications
 * @returns
 */
const adminNotifications = asyncHandler(async (req: Request, res: Response) => {
  let notifications=[
    {
      "isOnline":true,
      "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
      "name":"Bilal",
      "title":"Janette has accepted your friend request on Swipe.",
      "date":"Oct 17, 2018"
  },
  {
      "isOnline":false,
      "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
      "name":"Bilal",
      "title":"Janette has accepted your friend request on Swipe.",
      "date":"Oct 17, 2018w"
  },
  {
      "isOnline":true,
      "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
      "name":"Bilal",
      "title":"Janette has accepted your friend request on Swipe.",
      "date":"Oct 17, 2018"
  }]
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {
    notifications:notifications,
    nextHit:1
  });
});

/**
 *
 * @param {*} params
 * @description for admin contacts
 * @returns
 */
const adminContacts = asyncHandler(async (req: Request, res: Response) => {
  let contacts=[
    {
      "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
      "name":"Janette Dalton",
      "isOnline":true,
      "location":"Sofia, Bulgaria"
  },
  {
      "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
      "name":"Janette Dalton",
      "isOnline":false,
      "location":"Sofia, Bulgaria"
  },
  {
    "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
    "name":"Janette Dalton",
    "isOnline":true,
    "location":"Sofia, Bulgaria"
},
{
    "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
    "name":"Janette Dalton",
    "isOnline":false,
    "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":true,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":false,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":true,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":false,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":true,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":false,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":true,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":false,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":true,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":false,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":true,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":false,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":true,
  "location":"Sofia, Bulgaria"
},
{
  "image":"../assets/dist/img/avatars/avatar-female-1.jpg",
  "name":"Janette Dalton",
  "isOnline":false,
  "location":"Sofia, Bulgaria"
}
  ]
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {
    contacts:contacts,
    nextHit:1
  });
});

/**
 *
 * @param {*} params
 * @description for admin messages
 * @returns
 */
const adminMessages = asyncHandler(async (req: Request, res: Response) => {
  let messages:any=[
    {
      "isMyMessage":false,
      "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
      "name":"bilal",
      "message":"Well done all. See you all at 2 for the kick-off meeting.",
      "time":"10:23 PM",
      "date":"Today"
    },
  {
    "isMyMessage":true,
    "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
    "name":"bilal",
    "message":"Well done all. See you all at 2 for the kick-off meeting.",
    "time":"10:21 PM",
    "date":"Yestarday"
  },
  {
    "isMyMessage":true,
    "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
    "name":"bilal",
    "message":"Well done all. See you all at 2 for the kick-off meeting.",
    "time":"10:21 PM",
    "date":"Yestarday"
  },
  {
    "isMyMessage":true,
    "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
    "name":"bilal",
    "message":"Well done all. See you all at 2 for the kick-off meeting.",
    "time":"10:21 PM",
    "date":"Yestarday"
  },
  {
    "isMyMessage":true,
    "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
    "name":"bilal",
    "message":"Well done all. See you all at 2 for the kick-off meeting.",
    "time":"10:21 PM",
    "date":"Yestarday"
  },
  {
    "isMyMessage":true,
    "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
    "name":"bilal",
    "message":"Well done all. See you all at 2 for the kick-off meeting.",
    "time":"10:21 PM",
    "date":"Yestarday"
  },
  {
    "isMyMessage":true,
    "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
    "name":"bilal",
    "message":"Well done all. See you all at 2 for the kick-off meeting.",
    "time":"10:21 PM",
    "date":"Yestarday"
  },
  {
    "isMyMessage":true,
    "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
    "name":"bilal",
    "message":"Well done all. See you all at 2 for the kick-off meeting.",
    "time":"10:21 PM",
    "date":"Yestarday"
  },
  {
    "isMyMessage":true,
    "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
    "name":"bilal",
    "message":"Well done all. See you all at 2 for the kick-off meeting.",
    "time":"10:21 PM",
    "date":"Yestarday"
  },
  {
    "isMyMessage":true,
    "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
    "name":"bilal",
    "message":"Well done all. See you all at 2 for the kick-off meeting.",
    "time":"10:21 PM",
    "date":"Yestarday"
  },
  {
    "isMyMessage":true,
    "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
    "name":"bilal",
    "message":"Well done all. See you all at 2 for the kick-off meeting.",
    "time":"10:21 PM",
    "date":"Yestarday"
  },
  {
    "isMyMessage":true,
    "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
    "name":"bilal",
    "message":"Well done all. See you all at 2 for the kick-off meeting.",
    "time":"10:21 PM",
    "date":"Yestarday"
  },
  {
    "isMyMessage":true,
    "image":"../assets/dist/img/avatars/avatar-female-5.jpg",
    "name":"bilal",
    "message":"Well done all. See you all at 2 for the kick-off meeting.",
    "time":"10:21 PM",
    "date":"Yestarday"
  }
  ]
  let {pageNo}=req.query;
  let page=0;
  if(pageNo)
  page=+pageNo; 
  
  messages=messages.map((message:any)=>{
    message.date= getWhatsAppDate(Date.now() - page * 24 * 60 * 60 * 1000)
    return message;
  })
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {
    messages:messages,
    nextHit:1
  });
});
/**
 *
 * @param {*} params
 * @description for admin locations
 * @returns
 */
const adminLocations = asyncHandler(async (req: Request, res: Response) => {
   const locations = [
    {
      lat: -31.56391,
      lng: 147.154312,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -33.718234,
      lng: 150.363181,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -33.727111,
      lng: 150.371124,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -33.848588,
      lng: 151.209834,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -33.851702,
      lng: 151.216968,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -34.671264,
      lng: 150.863657,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -35.304724,
      lng: 148.662905,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -36.817685,
      lng: 175.699196,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -36.828611,
      lng: 175.790222,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -37.75,
      lng: 145.116667,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -37.759859,
      lng: 145.128708,
      title: "L&T ",
      
    },
    {
      lat: -37.765015,
      lng: 145.133858,
      description: "L&T ",
      
    },
    {
      lat: -37.770104,
      lng: 145.143299,
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -37.7737,
      lng: 145.145187,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -37.774785,
      lng: 145.137978,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -37.819616,
      lng: 144.968119,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -38.330766,
      lng: 144.695692,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -39.927193,
      lng: 175.053218,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -41.330162,
      lng: 174.865694,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -42.734358,
      lng: 147.439506,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -42.734358,
      lng: 147.501315,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -42.735258,
      lng: 147.438,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    },
    {
      lat: -43.999792,
      lng: 170.463352,
      title: "L&T ",
      description: "L&T ",
      image: "http://61.95.165.226/streetnewsfiles/1691393931078-1691393931078-331200467.jpg"
    } 
  ]
 
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {
    locations:locations,
    nextHit:1
  });
});

export {
    adminDashboard,
    adminMessages,
    adminContacts,
    adminDiscussions,
    adminNotifications,
    adminLocations
}