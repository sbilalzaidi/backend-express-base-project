import { ADMIN_TYPE } from "enums/enums";

export interface ICheckAdminExists {
    email?: string;
    phoneNo?: string;
    isSignUpRequest?: boolean;
    adminId?: string;
  }
  export interface IVerifyOtp {
    otpCode?: string;
    phoneNo?: string;
    prefix?: string;
  }
  export interface IVerifyVerificationEmailLink {
    verificationEmailToken?: string;
    prefix?: string;
  }
  export interface IProfiles {
    pageNo?: number;
    startDate?:Date;
    endDate?:Date;
    limit?: number;
    search?: string;
    adminType?:ADMIN_TYPE.ADMIN | ADMIN_TYPE.ADMIN_USER|ADMIN_TYPE.COMPANY|ADMIN_TYPE.COMPANY_USER;
  }
  export interface IReadAdmin<T> {
    checkAdminExists(params: any): Promise<any>;
    adminDetails(params: any): Promise<any>;
    profiles(params: IProfiles): Promise<any>;
    stores(
      storeIds: string[],
      regionIds: string[] | [],
      longitude: number | 0,
      latitude: number | 0,
      day:number | 0,
      time:number | 0
    ): Promise<any>;
  }
  export interface IWriteAdmin<T> {
    createAdminUser(params: any): Promise<any>;
    updateAdminDetails(params: any): Promise<any>;
  }
  