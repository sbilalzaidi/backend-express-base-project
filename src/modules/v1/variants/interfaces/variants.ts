import { ObjectId } from "mongoose";
import { STATUS, DISCOUNT_TYPE,APPROVAL_STATUS } from "enums/enums";


export interface IVariant{
    variantId:Object;
}
export interface IVariants{
  variantIds:Object;
}

export interface IAddVariants {
  storeId: ObjectId;
  regionIds?: ObjectId;
  label?:LanguageType;
  cp: number;
  sp: number;
  spAfterDiscount?: number;
  spWithVat?: number;
  vat?:number;
  discount?: number;
  discountType?:
    | DISCOUNT_TYPE.FLAT
    | DISCOUNT_TYPE.NO_DISCOUNT
    | DISCOUNT_TYPE.PERCENTAGE;
  sku?: string;
  rating?: number;
  inventoryOn?:STATUS.ACTIVE | STATUS.IN_ACTIVATED;
  quantity?:number
  approvalStatus?:APPROVAL_STATUS.APPROVED | APPROVAL_STATUS.NOT_APPROVED | 
  APPROVAL_STATUS.PENDING | APPROVAL_STATUS.REVIEW;
  status?:STATUS.ACTIVE | STATUS.IN_ACTIVATED | STATUS.DELETED;
}

export interface IUpdateVariants extends IVariant{
  storeId: ObjectId;
  regionIds?: ObjectId;
  label?:LanguageType;
  cp: number;
  sp: number;
  spAfterDiscount?: number;
  spWithVat?: number;
  vat?:number;
  discount?: number;
  discountType?:
    | DISCOUNT_TYPE.FLAT
    | DISCOUNT_TYPE.NO_DISCOUNT
    | DISCOUNT_TYPE.PERCENTAGE;
  rating?: number;
  inventoryOn?:STATUS.ACTIVE | STATUS.IN_ACTIVATED;
  quantity?:number
  approvalStatus?:APPROVAL_STATUS.APPROVED | APPROVAL_STATUS.NOT_APPROVED | 
  APPROVAL_STATUS.PENDING | APPROVAL_STATUS.REVIEW;
  status?:STATUS.ACTIVE | STATUS.IN_ACTIVATED | STATUS.DELETED;
}
export interface IReadVariant<T> {
}
export interface IWriteVariant<T> {
    createVariant(catalogueId:string,params:any): Promise<any>;
    updateVariant(catalogueId:string,sku:string,params:any): Promise<any>;
    deleteVariant(catalogueId:string,adminId:string,sku:string): Promise<any>;
    changeStatusVariant(catalogueId:string,adminId:string,sku:string,status:number): Promise<any>;
    increaseVariantQauntity(catalogueId:string,adminId:string,quantity:number): Promise<any>;
    decreaseVariantQauntity(catalogueId:string,adminId:string,quantity:number): Promise<any>;
}

