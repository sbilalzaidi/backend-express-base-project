import { Request, Response, NextFunction } from "express";
import joi from "joi";
import CONSTANT from "../../../../constant/constant";
import { STATUS, DISCOUNT_TYPE } from "../../../../enums/enums";
import { validateSchema } from "../../../../lib/universal-function";
import {
  translationSchemaFn
} from "../../common/validators/schema";

const addVariantsSchema = joi.object({
  adminId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
  catalogueId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
  regionIds: joi
    .array()
    .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
    .optional(),
  label: joi.object(translationSchemaFn()).optional(),
  cp: joi.number().min(0).required(),
  sp: joi.number().min(0).required(),
  discount: joi.number().min(0).optional(),
  discountType: joi
    .number()
    .valid(...Object.values(DISCOUNT_TYPE))
    .optional(),
  rating: joi.number().min(0).max(5).optional(),
  inventoryOn: joi
    .number()
    .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED)
    .optional(),
  quantity: joi.number().min(1).optional(),
  status: joi
    .number()
    .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED, STATUS.DELETED)
    .required(),
});
const editVariantsSchema = joi.object({
  adminId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
  catalogueId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
  sku: joi.string().required(),
  regionIds: joi
    .array()
    .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
    .optional(),
  label: joi.object(translationSchemaFn()).optional(),
  cp: joi.number().min(0).required(),
  sp: joi.number().min(0).required(),
  discount: joi.number().min(0).optional(),
  discountType: joi
    .number()
    .valid(...Object.values(DISCOUNT_TYPE))
    .optional(),
  rating: joi.number().min(0).max(5).optional(),
  quantity: joi.number().min(1).optional(),
  inventoryOn: joi
    .number()
    .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED)
    .optional(),
  status: joi
    .number()
    .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED, STATUS.DELETED)
    .optional(),
});
/**
 *
 * @param {*} params
 * @description for create variant
 * @returns
 */
const validateAddVariant = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    await validateSchema(req.body, addVariantsSchema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for create variant
 * @returns
 */
const validateEditVariant = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    await validateSchema(req.body, editVariantsSchema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for delete variant
 * @returns
 */
const validateDeleteVariant = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      adminId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
      catalogueId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
      sku: joi.string().required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for change status variant
 * @returns
 */
const validateChangeStatusOfVariant = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      adminId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
      catalogueId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
      sku: joi.string().required(),
      status: joi.number().valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED).optional(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
export {
  validateAddVariant,
  validateEditVariant,
  validateDeleteVariant,
  validateChangeStatusOfVariant,
};
