import { Router } from "express";
const router = Router();
import { adminAuthorization } from "../../common/services/authorization";
import {sanitizeRequest} from "../../../../middlewares/sanitizeRequest"
import {
  validateAddVariant,
  validateEditVariant,
  validateDeleteVariant,
  validateChangeStatusOfVariant
} from "../validaters/variants";
import {
  addVariant,
  editVariant,
  deleteVariant,
  changeStatusVariant
} from "../controllers/variants"
/*
VARIANTS API'S
*/
router.post("/", validateAddVariant, adminAuthorization, sanitizeRequest,addVariant);
router.put("/", validateEditVariant, adminAuthorization,sanitizeRequest, editVariant);
router.delete(
  "/",
  validateDeleteVariant,
  adminAuthorization,
  deleteVariant
);
router.patch(
  "/",
  validateChangeStatusOfVariant,
  adminAuthorization,
  changeStatusVariant
);
export default router;
