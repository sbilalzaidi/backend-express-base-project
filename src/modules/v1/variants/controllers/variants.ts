import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse } from "../../../../lib/universal-function";
import * as variantSrv from "../services/variants";
import {VARIANT_PREFIX} from "../constants/variants"
import {generateSKU} from "../../../../lib/universal-function"
import { ADMIN_TYPE } from "../../../../enums/enums";
/**
 *
 * @param {*} params
 * @description for creating variant
 * @returns
 */
const addVariant = asyncHandler(async (req: Request, res: Response) => {
  let {catalogueId}=req.body;
  delete req.body.catalogueId;
  let {adminId,parentId,adminType}=req.user;
  if(ADMIN_TYPE.COMPANY_USER==adminType)
  adminId=parentId;
  const sku=await generateSKU(VARIANT_PREFIX.VARIANT,30);
  const variantNo=new Date().getTime();
  await variantSrv.createVariant(catalogueId,{ adminId,...req.body,sku,variantNo });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.VARIANT_CREATE_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for edit variant
 * @returns
 */
const editVariant = asyncHandler(async (req: Request, res: Response) => {
  let {catalogueId,sku}=req.body;
  delete req.body.catalogueId;
  delete req.body.sku;
  let {adminId,parentId,adminType}=req.user;
  if(ADMIN_TYPE.COMPANY_USER==adminType)
  adminId=parentId;
  await variantSrv.updateVariant(catalogueId,sku,{ adminId,...req.body });

  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.VARIANT_UPDATED_SUCCESSFULLY,
    {}
  );
});

/**
 *
 * @param {*} params
 * @description for delete variant
 * @returns
 */
const deleteVariant = asyncHandler(async (req: Request, res: Response) => {
  let {catalogueId,sku}=req.body;
  let {adminId,parentId,adminType}=req.user;
  if(ADMIN_TYPE.COMPANY_USER==adminType)
  adminId=parentId;
  adminId = req?.body?.adminId || adminId;
  let variant = await variantSrv.deleteVariant(catalogueId,adminId,sku);
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, variant);
});
/**
 *
 * @param {*} params
 * @description for change status variant
 * @returns
 */
const changeStatusVariant = asyncHandler(
  async (req: Request, res: Response) => {
    let {catalogueId,sku,status}=req.body;
    let {adminId,parentId,adminType}=req.user;
    if(ADMIN_TYPE.COMPANY_USER==adminType)
    adminId=parentId;
    adminId = req?.body?.adminId || adminId;
    let variant = await variantSrv.changeStatusVariant(catalogueId,adminId,sku,status);
    return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, variant);
  }
);
export {
  addVariant,
  editVariant,
  deleteVariant,
  changeStatusVariant
};
