import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import {
  IAddVariants,
  IUpdateVariants,
  IWriteVariant,
  IReadVariant,
  IVariant,
} from "../interfaces/variants";
export class VariantDao<T>
  extends BaseDao
  implements IReadVariant<T>, IWriteVariant<T>
{
  async createVariant(catalogueId:string,params: IAddVariants): Promise<any> {
    const update={
      $push: {
        variants: params,
      },
    }
    return await this.updateOne(Models.CATALOGUES, {_id: catalogueId},update,{});
  }
  async updateVariant(catalogueId:string,sku:string,params: IUpdateVariants): Promise<any> {
    let query:any = {
      _id: catalogueId,
      status: { $ne: STATUS.DELETED },
      'variants.sku':sku
    };
    let update = {
      $set: {
        variants: params,
      },
    };
    return await this.updateOne(Models.CATALOGUES, query,update,{});
  }
  async deleteVariant(catalogueId:string,adminId:string,sku:string): Promise<any> {
    let query:any = {
      _id: catalogueId,
      status: { $ne: STATUS.DELETED },
      'variants.sku':sku,
      'variants.adminId':adminId
    };
    let update = { $set: { "variants.$.status": STATUS.DELETED } }
    return await this.updateOne(Models.CATALOGUES, query,update,{});
  }
  async changeStatusVariant(catalogueId:string,adminId:string,sku:string,status:number): Promise<any> {
    let query:any = {
      _id: catalogueId,
      status: { $ne: STATUS.DELETED },
      'variants.sku':sku,
      'variants.adminId':adminId
    };
    let update = { $set: { "variants.$.status": status } }
    return await this.updateOne(Models.CATALOGUES, query,update,{});
  }
  async increaseVariantQauntity(catalogueId:string,adminId:string,quantity:number): Promise<any> {
    let query:any = {
      _id: catalogueId,
      status: { $ne: STATUS.DELETED },
      'variants.adminId':adminId
    };
    let update = { $inc: { "variants.$.quantity": quantity } }
    return await this.updateOne(Models.CATALOGUES, query,update,{});
  }
  async decreaseVariantQauntity(catalogueId:string,adminId:string,quantity:number): Promise<any> {
    let query:any = {
      _id: catalogueId,
      status: { $ne: STATUS.DELETED },
      'variants.adminId':adminId
    };
    let update = { $inc: { "variants.$.quantity": -quantity } }
    return await this.updateOne(Models.CATALOGUES, query,update,{});
  }
}
export const variantDao = new VariantDao();
