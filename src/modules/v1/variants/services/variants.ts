import { variantDao } from "../dao/variants";
import {
  IAddVariants,
  IUpdateVariants
} from "../interfaces/variants";


/**
 *
 * @param {*} params
 * @description for create variants
 * @returns
 */
const createVariant = async (catalogueId:string,params: IAddVariants) => {
  return await variantDao.createVariant(catalogueId,params);
};
/**
 *
 * @param {*} params
 * @description for update variants
 * @returns
 */
const updateVariant = async (catalogueId:string,sku:string,params: IUpdateVariants) => {
  return await variantDao.updateVariant(catalogueId,sku,params);
};

/**
 *
 * @param {*} params
 * @description for delete variant
 * @returns
 */
const deleteVariant = async (catalogueId:string,adminId:string,sku:string) => {
  return await variantDao.deleteVariant(catalogueId,adminId,sku);
};
/**
 *
 * @param {*} params
 * @description for change status variant
 * @returns
 */
const changeStatusVariant = async (catalogueId:string,adminId:string,sku:string,status: number) => {
  return await variantDao.changeStatusVariant(catalogueId,adminId,sku,status);
};
export {
  createVariant,
  updateVariant,
  deleteVariant,
  changeStatusVariant
};
