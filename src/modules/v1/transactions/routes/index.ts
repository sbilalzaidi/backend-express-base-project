import { Router } from "express";
const router = Router();
import { userAuthorization } from "../../common/services/authorization";
import {validateTransactions} from "../validaters/transactions"
import {
    transactions
} from "../controllers/transactions";
/*
TRANSACTIONS API'S
*/
router.get("/",validateTransactions, userAuthorization, transactions);
export default router;
