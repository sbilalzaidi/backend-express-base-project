import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import {
  ITransactions,
  IReadTransactions,
  IWriteTransactions,
} from "../interfaces/transactions";
export class TransactionDao<T>
  extends BaseDao
  implements IReadTransactions<T>, IWriteTransactions<T>
{
  async transactions(params: ITransactions): Promise<any> {
    let { orderId, transactionId, limit = 0, pageNo, userId } = params;
    let query: any = {
      userId: userId,
      status: { $ne: STATUS.DELETED },
      ...(orderId && { orderId: orderId }),
      ...(transactionId && { transactionId: transactionId }),
    };

    let transactionData = await this.find(
      Models.TRANSACTIONS,
      query,
      {},
      {},
      {},
      { pageNo, limit },
      {
        path: "userIds",
        model: "users",
      }
    );
    let count = await this.count(Models.TRANSACTIONS, query);
    return { transactions:transactionData, count };
  }
}
export const transactionDao = new TransactionDao();
