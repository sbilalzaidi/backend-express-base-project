import { Request, Response, NextFunction } from "express";
import joi from "joi";
import CONSTANT from "../../../../constant/constant";
import { STATUS } from "../../../../enums/enums";
import { validateSchema } from "../../../../lib/universal-function";
/**
 *
 * @param {*} params
 * @description for transaction
 * @returns
 */
const validateTransactions= async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      orderId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
      transactionId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
      limit: joi.number().min(1).max(100).required(),
      pageNo: joi.number().min(1).required(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};

export {
  validateTransactions,
};
