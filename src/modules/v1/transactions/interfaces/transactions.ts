import { ObjectId } from "mongoose";

export interface ITransactions {
  userId: ObjectId;
  orderId?:string;
  transactionId?:string;
  limit:number;
  pageNo:number;
}
export interface IReadTransactions<T> {
  transactions(params: ITransactions): Promise<any>;
}
export interface IWriteTransactions<T> {
}
