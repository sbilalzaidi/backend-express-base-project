import { transactionDao } from "../dao/transactions";
import { ITransactions } from "../interfaces/transactions";

/**
 *
 * @param {*} params
 * @description for transactions
 * @returns
 */
const transactions = async (params: ITransactions) => {
  return await transactionDao.transactions(params);
};
export { transactions };
