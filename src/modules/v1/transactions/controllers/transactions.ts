import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse } from "../../../../lib/universal-function";
import * as transactionSrv from "../services/transactions";
import { ITransactions } from "../interfaces/transactions";
/**
 *
 * @param {*} params
 * @description for transactions
 * @returns
 */
const transactions = asyncHandler(async (req: Request, res: Response) => {
  let { userId } = req.user;
  let { orderId, transactionId, limit, pageNo } =
    req.query as unknown as ITransactions;
  let promoCodes = await transactionSrv.transactions({
    orderId,
    transactionId,
    userId,
    limit,
    pageNo,
  });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, promoCodes);
});

export { transactions };
