import { ObjectId } from "mongoose";
import { STATUS } from "../../../../enums/enums";

export interface IReadRegions<T> {
    regions(params: any): Promise<any>;
    regionsThroughLocation(params: any): Promise<any>;
}
export interface IWriteRegions<T> {
  createRegions(params: any): Promise<any>;
  updateRegions(params: any): Promise<any>;
}
