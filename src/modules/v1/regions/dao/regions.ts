import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import { IReadRegions, IWriteRegions } from "../interfaces/regions";
export class RegionDao<T>
  extends BaseDao
  implements IReadRegions<T>, IWriteRegions<T>
{
  async createRegions(params: any): Promise<any> {
    return await this.save(Models.ORDERS, params);
  }
  async updateRegions(params: any): Promise<any> {
    let { regionId } = params;
    let query = {
      _id: regionId,
    };
    let update = {
      $set: params,
    };
    return await this.updateOne(Models.REGIONS, query, update, {});
  }
  async regions(params: any): Promise<any> {
    let { search, limit = 0, pageNo, userId } = params;
    let query: any = {
      userId: userId,
      status: { $ne: STATUS.DELETED },
    };
    if (search) query.name = search;
    let regionData = await this.find(
      Models.REGIONS,
      query,
      {},
      {},
      {},
      { pageNo, limit }
    );
    let count = await this.count(Models.REGIONS, query);
    return { regions: regionData, count };
  }
  async regionsThroughLocation(params: any): Promise<any> {
    let { latitude, longitude } = params;
    let query: any = {
      boundaries: {
        $geoWithin: {
          $geometry: { type: "Point", coordinates: [longitude, latitude] },
        },
      },
      status: { $ne: STATUS.DELETED },
    };
    return this.find(
      Models.REGIONS,
      query,
      {
        regionId:"$_id",
        name:1
      },
      {},
      {},
      { pageNo: 1, limit: 500 }
    );
  }
}
export const regionDao = new RegionDao();
