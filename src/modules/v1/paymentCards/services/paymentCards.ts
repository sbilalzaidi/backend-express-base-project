import MESSAGES from "../../../../messages/messages";
import { paymentCardDao } from "../dao/paymentCards";
import {
  ICheckPaymentCardExists,
  ICreatePaymentCard,
  IUpdatePaymentCard,
  IPaymentCards,
} from "../interfaces/paymentCards";
/**
 *
 * @param {*} params
 * @description for cheking payment card exists or not
 * @returns
 */
const checkPaymentCardExists = async (params: ICheckPaymentCardExists) => {
  let paymentCardExists = await paymentCardDao.checkPaymentCardExists(params);
  if (paymentCardExists) throw new Error(MESSAGES.PAYMENT_CARD_ALREADY_EXISTS);
  return true;
};

/**
 *
 * @param {*} params
 * @description for create payment card
 * @returns
 */
const createPaymentCard = async (params: ICreatePaymentCard) => {
  return await paymentCardDao.createPaymentCard(params);
};
/**
 *
 * @param {*} params
 * @description for update payment card
 * @returns
 */
const updatePaymentCard = async (params: IUpdatePaymentCard) => {
  return await paymentCardDao.updatePaymentCard(params);
};

/**
 *
 * @param {*} params
 * @description for payment cards
 * @returns
 */
const paymentCards = async (params: IPaymentCards) => {
  return await paymentCardDao.paymentCards(params);
};
export {
  checkPaymentCardExists,
  createPaymentCard,
  updatePaymentCard,
  paymentCards,
};
