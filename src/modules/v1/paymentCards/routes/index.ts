import { Router } from "express";
const router = Router();
import { userAuthorization } from "../../common/services/authorization";
import {
  addPaymentCard,
  editPaymentCard,
  paymentCards,
} from "../controllers/paymentCards";
/*
PAYMENT CARDS API'S
*/
router.post("/", userAuthorization, addPaymentCard);
router.put("/", userAuthorization, editPaymentCard);
router.get("/", userAuthorization, paymentCards);
export default router;
