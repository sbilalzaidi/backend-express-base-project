import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse } from "../../../../lib/universal-function";
import * as paymentCardSrv from "../services/paymentCards";
/**
 *
 * @param {*} params
 * @description for creating payment card
 * @returns
 */
const addPaymentCard = asyncHandler(async (req: Request, res: Response) => {
  let { userId } = req.user;
  await paymentCardSrv.checkPaymentCardExists({ ...req.body,userId });
  await paymentCardSrv.createPaymentCard({ ...req.body,userId });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.PAYMENT_CARD_CREATE_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for edit payment card
 * @returns
 */
const editPaymentCard = asyncHandler(async (req: Request, res: Response) => {
  let { userId } = req.user;
  await paymentCardSrv.checkPaymentCardExists({ ...req.body,userId });
  await paymentCardSrv.updatePaymentCard({ ...req.body,userId });
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.PAYMENT_CARD_UPDATED_SUCCESSFULLY,
    {}
  );
});

/**
 *
 * @param {*} params
 * @description for payment cards
 * @returns
 */
const paymentCards = asyncHandler(async (req: Request, res: Response) => {
  let { userId } = req.user;
  let promoCodes = await paymentCardSrv.paymentCards({ userId });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, promoCodes);
});

export { addPaymentCard, editPaymentCard, paymentCards };
