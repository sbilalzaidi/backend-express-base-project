import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import {
  ICheckPaymentCardExists,
  ICreatePaymentCard,
  IUpdatePaymentCard,
  IPaymentCards,
  IReadPaymentCards,
  IWritePaymentCards,
} from "../interfaces/paymentCards";
export class PaymentCardDao<T>
  extends BaseDao
  implements IReadPaymentCards<T>, IWritePaymentCards<T>
{
  async checkPaymentCardExists(params: ICheckPaymentCardExists): Promise<any> {
    let { userId, customerId } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (userId) query.userId = userId;
    if (customerId) query._id = { $ne: customerId };
    return await this.findOne(Models.PAYMENT_CARDS, query, {}, {}, {});
  }
  async createPaymentCard(params: ICreatePaymentCard): Promise<any> {
    return await this.save(Models.PAYMENT_CARDS, params);
  }
  async updatePaymentCard(params: IUpdatePaymentCard): Promise<any> {
    let { userId, customerId } = params;
    let query = {
      userId: userId,
      customerId: customerId,
    };
    let update = {
      $set: params,
    };
    return await this.updateOne(Models.PAYMENT_CARDS, query, update, {});
  }
  async paymentCards(params: IPaymentCards): Promise<any> {
    let { userId } = params;
    let query = {
      userId: userId,
      status: { $ne: STATUS.DELETED },
    };
    return await this.find(
      Models.PAYMENT_CARDS,
      query,
      {},
      {},
      {
        path: "userId",
        model: "users",
      }
    );
  }
}
export const paymentCardDao = new PaymentCardDao();
