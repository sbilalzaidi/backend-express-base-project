import { ObjectId } from "mongoose";
import { STATUS } from "../../../../enums/enums";
export interface ICheckPaymentCardExists {
  userId: ObjectId;
  customerId: ObjectId;
}
export interface ICreatePaymentCard {
  userId: ObjectId;
  paymentBrand: string;
  currency: string;
  customerId: string;
  cardNumber: string;
  cardHolderName: string;
  expirationDate: Date;
  cvv: string;
  expiryMonth: string;
  expiryYear: string;
  card: any;
  status: STATUS.ACTIVE | STATUS.DELETED;
}
export interface IUpdatePaymentCard {
  userId: ObjectId;
  customerId: string;
  paymentBrand: string;
  currency: string;
  cardNumber: string;
  cardHolderName: string;
  expirationDate: Date;
  cvv: string;
  expiryMonth: string;
  expiryYear: string;
  card: any;
  status: STATUS.ACTIVE | STATUS.DELETED;
}
export interface IPaymentCards {
  userId: ObjectId;
}
export interface IReadPaymentCards<T> {
  checkPaymentCardExists(params: ICheckPaymentCardExists): Promise<any>;
  paymentCards(params: IPaymentCards): Promise<any>;
}
export interface IWritePaymentCards<T> {
  createPaymentCard(params: ICreatePaymentCard): Promise<any>;
  updatePaymentCard(params: IUpdatePaymentCard): Promise<any>;
}
