export interface ICheckModulePermissionExists {
    moduleId:string;
}
export interface ICreateModulePermissions{
    moduleId:string;
    addApis?:string[];
    editApis?:string[];
    viewApis?:string[];
    deleteApis?:string[];
}
export interface IUpdateModulePermissions{
    moduleId:string;
    addApis?:string[];
    editApis?:string[];
    viewApis?:string[];
    deleteApis?:string[];
}
export interface IModulePermissionsDetails {
    moduleId:string
}
export interface IModulesPermissions{
    limit:number;
    pageNo:number;
}
export interface IReadPermissions<T> {
    checkModulePermissionExists(params:ICheckModulePermissionExists): Promise<any>;
    getModulePermissionsDetails(params:IModulePermissionsDetails): Promise<any>;
    getAllModulesPermissions(params:IModulesPermissions): Promise<any>;
}
export interface IWritePermissions<T> {
    createPermissions(params:ICreateModulePermissions): Promise<any>;
    updatePermissions(params:IUpdateModulePermissions): Promise<any>;
}

