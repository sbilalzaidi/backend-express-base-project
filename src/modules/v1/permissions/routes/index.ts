import { Router } from "express";
const router = Router();
import {
  addModulePermissions,
  editModulePermissions,
  modulePermissionsDetails,
  modulesPermissions,
} from "../controllers/permissions";

import {
  validateAddModulePermissions,
  validateUpdateModulePermissions,
  validateModulePermissions,
  validateModulesPermissions,
} from "../validaters/permissions";
import { adminAuthorization } from "../../common/services/authorization";
/*
PERMISSIONS API'S
*/
router.post(
  "/",
  validateAddModulePermissions,
  adminAuthorization,
  addModulePermissions
);
router.put(
  "/",
  validateUpdateModulePermissions,
  adminAuthorization,
  editModulePermissions
);
router.get(
  "/:moduleId",
  validateModulePermissions,
  adminAuthorization,
  modulePermissionsDetails
);
router.get(
  "/",
  validateModulesPermissions,
  adminAuthorization,
  modulesPermissions
);

export default router;