import {
    Request,
    Response,
    NextFunction
  } from "express";
  import joi from "joi";
  import { validateSchema } from "../../../../lib/universal-function";
/**
 * 
 * @param {*} params 
 * @description for create module permissions
 * @returns 
 */
const validateAddModulePermissions = async(req: Request,
    res: Response,
    next: NextFunction) => {
    try {
        let schema = joi.object().keys({
            moduleId:joi.string().required(),
            addApis: joi.array().items(joi.string().required()).optional(),
            editApis: joi.array().items(joi.string().required()).optional(),
            viewApis: joi.array().items(joi.string().required()).optional(),
            deleteApis: joi.array().items(joi.string().required()).optional()
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
};

/**
 * 
 * @param {*} params 
 * @description for update module permissions
 * @returns 
 */
 const validateUpdateModulePermissions = async(req: Request,
    res: Response,
    next: NextFunction) => {
    try {
        let schema = joi.object().keys({
            moduleId:joi.string().required(),
            addApis: joi.array().items(joi.string().required()).optional(),
            editApis: joi.array().items(joi.string().required()).optional(),
            viewApis: joi.array().items(joi.string().required()).optional(),
            deleteApis: joi.array().items(joi.string().required()).optional()
        });
        await validateSchema(req.body, schema);
        next();
    } catch (error) {
      next(error);   
    }
};
/**
 * 
 * @param {*} params 
 * @description for get module permissions
 * @returns 
 */
 const validateModulePermissions = async(req: Request,
    res: Response,
    next: NextFunction) => {
    try {
        let schema = joi.object().keys({
            moduleId:joi.string().required()
        });
        await validateSchema(req.params, schema);
        next();
    } catch (error) {
      next(error);   
    }
};
/**
 * 
 * @param {*} params 
 * @description for get modules permissions
 * @returns 
 */
 const validateModulesPermissions = async(req: Request,
    res: Response,
    next: NextFunction) => {
    try {
        let schema = joi.object().keys({
            limit: joi.number().min(1).max(100).required(),
            pageNo: joi.number().min(1).required()
        });
        await validateSchema(req.query, schema);
        next();
    } catch (error) {
      next(error);   
    }
};

export {
    validateAddModulePermissions,
    validateUpdateModulePermissions,
    validateModulePermissions,
    validateModulesPermissions
}