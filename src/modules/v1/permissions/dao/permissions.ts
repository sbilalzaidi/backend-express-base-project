import { BaseDao } from "../../common/dao/baseDao";
import { STATUS } from "../../../../enums/enums";
import { 
  ICheckModulePermissionExists,
  ICreateModulePermissions,
  IUpdateModulePermissions,
  IModulePermissionsDetails,
  IModulesPermissions,
  IWritePermissions,
  IReadPermissions 
} from "../interfaces/permissions";
export class PermissionDao<T>
  extends BaseDao
  implements IReadPermissions<T>, IWritePermissions<T>
{
  async checkModulePermissionExists(params: ICheckModulePermissionExists): Promise<any> {
    let { moduleId } = params;
    let query = {
      moduleId: moduleId,
      status: { $ne: STATUS.DELETED },
    };
    return await this.findOne(Models.PERMISSIONS, query, {}, {}, {});
  }
  async getModulePermissionsDetails(params: IModulePermissionsDetails): Promise<any> {
    let { moduleId } = params;
    let query = {
      _id: moduleId,
      status: { $ne: STATUS.DELETED },
    };
    return await this.findOne(Models.PERMISSIONS, query, {}, {}, {
      path: 'moduleId',
      model: 'modules'
    });
  }
  async getAllModulesPermissions(params: IModulesPermissions): Promise<any> {
    let { limit, pageNo } = params;
    let query = {
      status: { $ne: STATUS.DELETED },
    };
    let modules = await this.find(Models.PERMISSIONS, query, {},{}, {_id:-1}, {limit, pageNo },{
      path: 'moduleId',
      model: 'modules'
    });
    let count = await this.count(Models.PERMISSIONS, query);
    return { modules, count };
  }
  async createPermissions(params: ICreateModulePermissions): Promise<any> {
    return await this.save(Models.PERMISSIONS, params);
  }
  async updatePermissions(params: IUpdateModulePermissions): Promise<any> {
    let { moduleId } = params;
    let query = {
      _id: moduleId,
    };
    let update = {
      $set: params,
    };
    return await this.updateOne(Models.PERMISSIONS, query, update, {});
  }
}
export const permissionDao = new PermissionDao();
