import { Request, Response, NextFunction } from "express";
import joi from "joi";
import CONSTANT from "../../../../constant/constant";
import { CATALOGUE_TYPE, STATUS, DAYS } from "../../../../enums/enums";
import { validateSchema } from "../../../../lib/universal-function";
import {
  translationSchemaFn
} from "../../common/validators/schema";

const cataloguesSchema = joi.object({
  excludeAdminIds: joi
    .array()
    .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
    .optional(),
  parentId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
  enableRegion: joi
    .number()
    .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED)
    .optional(),
  regionIds: joi
    .array()
    .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
    .optional(),
  name: joi.object(translationSchemaFn()).required(),
  description: joi.object(translationSchemaFn()).required(),
  tags: joi.array().items(joi.string().required()).optional(),
  searchTags: joi.array().items(joi.string().required()).optional(),
  images: joi.array().items(joi.string().required()).required(),
  icons: joi.array().items(joi.string().required()).required(),
  rating: joi.number().min(0).max(5).optional(),
  catalogueType: joi
    .number()
    .valid(...Object.values(CATALOGUE_TYPE))
    .required(),
  status: joi
    .number()
    .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED, STATUS.DELETED)
    .required(),
});
const editCataloguesSchema = joi.object({
  catalogueId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
  excludeAdminIds: joi
    .array()
    .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
    .optional(),
  parentId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).optional(),
  enableRegion: joi
    .number()
    .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED)
    .optional(),
  regionIds: joi
    .array()
    .items(joi.string().regex(CONSTANT.REGEX.MONGO_ID).required())
    .optional(),
  name: joi.object(translationSchemaFn()).optional(),
  description: joi.object(translationSchemaFn()).optional(),
  tags: joi.array().items(joi.string().required()).optional(),
  searchTags: joi.array().items(joi.string().required()).optional(),
  images: joi.array().items(joi.string().required()).optional(),
  icons: joi.array().items(joi.string().required()).optional(),
  rating: joi.number().min(0).max(5).optional(),
  catalogueType: joi
    .number()
    .valid(...Object.values(CATALOGUE_TYPE))
    .optional(),
  status: joi
    .number()
    .valid(STATUS.ACTIVE, STATUS.IN_ACTIVATED, STATUS.DELETED)
    .optional(),
});
/**
 *
 * @param {*} params
 * @description for create Catalogue
 * @returns
 */
const validateAddCatalogue = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    await validateSchema(req.body, cataloguesSchema);
    next();
  } catch (error) {
    next(error);
  }
};

/**
 *
 * @param {*} params
 * @description for update Catalogue
 * @returns
 */
const validateUpdateCatalogue = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    await validateSchema(req.body, editCataloguesSchema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for get Catalogue
 * @returns
 */
const validateCatalogue = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      catalogueId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
    });
    await validateSchema(req.params, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for get Catalogues
 * @returns
 */
const validateCatalogues = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      search: joi.string().optional(),
      limit: joi.number().min(1).max(100).required(),
      pageNo: joi.number().min(1).required(),
    });
    await validateSchema(req.query, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for delete Catalogue
 * @returns
 */
const validateDeleteCatalogue = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      catalogueId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};
/**
 *
 * @param {*} params
 * @description for change status Catalogue
 * @returns
 */
const validateChangeStatusOfCatalogue = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let schema = joi.object().keys({
      catalogueId: joi.string().regex(CONSTANT.REGEX.MONGO_ID).required(),
    });
    await validateSchema(req.body, schema);
    next();
  } catch (error) {
    next(error);
  }
};

export {
  validateAddCatalogue,
  validateUpdateCatalogue,
  validateCatalogue,
  validateCatalogues,
  validateDeleteCatalogue,
  validateChangeStatusOfCatalogue,
};
