import { Router } from "express";
const router = Router();
import {
  addCatalogue,
  editCatalogue,
  getCatalogueDetails,
  getCatalogues,
  deleteCatalogue,
  changeStatusCatalogue,
} from "../controllers/catalogues";
import {
  validateAddCatalogue,
  validateUpdateCatalogue,
  validateCatalogue,
  validateCatalogues,
  validateDeleteCatalogue,
  validateChangeStatusOfCatalogue
} from "../validaters/catalogues";
import { adminAuthorization } from "../../common/services/authorization";

/*
CATALOGUES API'S
*/
router.post("/", validateAddCatalogue, adminAuthorization, addCatalogue);
router.put("/", validateUpdateCatalogue, adminAuthorization, editCatalogue);
router.delete(
  "/",
  validateDeleteCatalogue,
  adminAuthorization,
  deleteCatalogue
);
router.patch(
  "/",
  validateChangeStatusOfCatalogue,
  adminAuthorization,
  changeStatusCatalogue
);
router.get(
  "/:catalogueId",
  validateCatalogue,
  adminAuthorization,
  getCatalogueDetails
);
router.get("/", validateCatalogues, adminAuthorization, getCatalogues);

export default router;
