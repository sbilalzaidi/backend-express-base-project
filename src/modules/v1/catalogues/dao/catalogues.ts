import { BaseDao } from "../../common/dao/baseDao";
import { STATUS, APPROVAL_STATUS } from "../../../../enums/enums";
import {
  ICreateCatalogue,
  IUpdateCatalogue,
  ICatalogueNameExists,
  ICatalogue,
  ICatalogues,
  IWriteCatalogue,
  IReadCatalogue,
} from "../interfaces/catalogues";
export class CatalogueDao<T>
  extends BaseDao
  implements IReadCatalogue<T>, IWriteCatalogue<T>
{
  async checkCatalogueExists(params: ICatalogueNameExists): Promise<any> {
    let { catalogueId, name } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
      ...(name && { name: name }),
    };
    if (catalogueId) query._id = { $ne: Object(catalogueId) };
    return await this.findOne(Models.CATALOGUES, query, {}, {}, {});
  }
  async catalogueDetails(params: ICatalogue): Promise<any> {
    let { catalogueId } = params;
    let query = {
      _id: catalogueId,
      status: { $ne: STATUS.DELETED },
    };
    return await this.findOne(Models.CATALOGUES, query, {}, {}, [
      {
        path: "parentId",
        model: "catalogues",
        match: {
          status: { $ne: STATUS.DELETED },
        },
      },
      {
        path: "categoryId",
        model: "catalogues",
        match: {
          status: { $ne: STATUS.DELETED },
        },
      },
      {
        path: "serviceId",
        model: "catalogues",
        match: {
          status: { $ne: STATUS.DELETED },
        },
      },
      {
        path: "combo",
        model: "catalogues",
        match: {
          status: { $ne: STATUS.DELETED },
        },
      },
    ]);
  }
  async catalogues(params: ICatalogues): Promise<any> {
    let { search, limit = 0, pageNo } = params;
    let query: any = {
      status: { $ne: STATUS.DELETED },
    };
    if (search) query["$or"] = [{ name: search }, { description: search }];

    let cataloguesData = await this.find(
      Models.CATALOGUES,
      query,
      {},
      {},
      {},
      { pageNo, limit },
      {}
    );
    let count = await this.count(Models.CATALOGUES, query);
    return { catalogues: cataloguesData, count };
  }
  async createCatalogue(params: ICreateCatalogue): Promise<any> {
    return await this.save(Models.CATALOGUES, params);
  }
  async updateCatalogue(params: IUpdateCatalogue): Promise<any> {
    let { catalogueId } = params;
    let query: any = {
      _id: catalogueId,
      status: { $ne: STATUS.DELETED },
    };
    let update = {
      $set: params,
    };
    return await this.update(Models.CATALOGUES, query, update, { multi: true });
  }
  async deleteCatalogue(params: ICatalogue): Promise<any> {
    let { catalogueId } = params;
    let query: any = {
      _id: catalogueId,
      status: { $ne: STATUS.DELETED },
    };
    let update = {
      $set: params,
    };
    return await this.update(Models.CATALOGUES, query, update, { multi: true });
  }
  async changeStatusCatalogue(params: ICatalogue): Promise<any> {
    let { catalogueId } = params;
    let query: any = {
      _id: catalogueId,
      status: { $ne: STATUS.DELETED },
    };
    let update = {
      $set: params,
    };
    return await this.update(Models.CATALOGUES, query, update, { multi: true });
  }
  async productsWithVariants(
    regionIds: string[] | [],
    productIds: string[],
    variantIds: string[]
  ): Promise<any> {
    let query: any = {
      _id: { $in: productIds },
      status: STATUS.ACTIVE,
      ...(regionIds.length && { regionIds: { $in: regionIds } }),
      "variants._id": { $in: variantIds },
      "variants.status": STATUS.ACTIVE,
      "variants.approvalStatus": APPROVAL_STATUS.APPROVED,
      ...(regionIds.length && {
        variants: {
          $elemMatch: {
            $or: [
              {
                $and: [{ enable: 1 }, { regions: { $in: regionIds } }],
              },
              { enable: 0 },
            ],
          },
        },
      }),
    };
    return await this.find(
      Models.CATALOGUES,
      query,
      {
        _id:1,
        sku: 1,
        name: 1,
        description: 1,
        images: 1,
        icons: 1,
        variants:{
          _id:1,
          adminId:1,
          variantImages:1,
          variantIcons:1,
          brand:1,
          label:1,
          cp:1,
          sp:1,
          spAfterDiscount:1,
          spWithVat:1,
          vat:1,
          discount:1,
          discountType:1,
          on:1,
          quantity:1,
          enableInventory:1,
          inventoryOn:1
        }
      },
      {},
      {},
      {}
    );
  }
}
export const catalogueDao = new CatalogueDao();
