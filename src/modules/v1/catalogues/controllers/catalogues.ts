import { Request, Response } from "express";
import asyncHandler from "../../../../middlewares/asyncHandler";
import MESSAGES from "../../../../messages/messages";
import codes from "../../../../codes/status_codes";
import { sendResponse } from "../../../../lib/universal-function";
import  * as catalogueSrv from "../services/catalogues";
import { ICatalogues } from "../interfaces/catalogues";
import {CATALOGUE_PREFIX} from "../constants/catalogues"
import { CATALOGUE_TYPE} from "../../../../enums/enums";
/**
 *
 * @param {*} params
 * @description for creating catalogue
 * @returns
 */
const addCatalogue = asyncHandler(async (req: Request, res: Response) => {
  let {catalogueType} =req.body;
  let prefix=CATALOGUE_PREFIX.CATALOGUE;
  await catalogueSrv.checkCatalogueExists({ ...req.body });
  switch(catalogueType){
    case CATALOGUE_TYPE.PRODUCT:
      prefix=CATALOGUE_PREFIX.CATALOGUE;
      break;
    case CATALOGUE_TYPE.SERVICE:
      prefix=CATALOGUE_PREFIX.SERVICE;
    default:
      break;
  }
  let catalogue=await catalogueSrv.createCatalogue({ ...req.body });
  await catalogue.setSku(prefix);
  await catalogue.save();
  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.CATALOGUE_CREATE_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for edit catalogue
 * @returns
 */
const editCatalogue = asyncHandler(async (req: Request, res: Response) => {
  await catalogueSrv.checkCatalogueExists({ ...req.body });
  await catalogueSrv.updateCatalogue({ ...req.body });

  return sendResponse(
    req,
    res,
    codes.SUCCESS,
    MESSAGES.CATALOGUE_UPDATED_SUCCESSFULLY,
    {}
  );
});
/**
 *
 * @param {*} params
 * @description for catalogue details
 * @returns
 */
const getCatalogueDetails = asyncHandler(
  async (req: Request, res: Response) => {
    let { catalogueId } = req.params;
    let catalogue = await catalogueSrv.catalogueDetails({ catalogueId });
    return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, catalogue);
  }
);
/**
 *
 * @param {*} params
 * @description for catalogues
 * @returns
 */
const getCatalogues = asyncHandler(async (req: Request, res: Response) => {
  let { search, limit, pageNo } = req.query as unknown as ICatalogues;
  let cataloguesData = await catalogueSrv.catalogues({ search, limit, pageNo });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, {
    catalogues: cataloguesData?.catalogues || [],
    count: cataloguesData?.count || 0,
  });
});
/**
 *
 * @param {*} params
 * @description for delete catalogues
 * @returns
 */
const deleteCatalogue = asyncHandler(async (req: Request, res: Response) => {
  let catalogue = await catalogueSrv.deleteCatalogue({ ...req.body });
  return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, catalogue);
});
/**
 *
 * @param {*} params
 * @description for change status catalogue
 * @returns
 */
const changeStatusCatalogue = asyncHandler(
  async (req: Request, res: Response) => {
    let catalogue = await catalogueSrv.changeStatusCatalogue({ ...req.body });
    return sendResponse(req, res, codes.SUCCESS, MESSAGES.SUCCESS, catalogue);
  }
);
export {
  addCatalogue,
  editCatalogue,
  getCatalogueDetails,
  getCatalogues,
  deleteCatalogue,
  changeStatusCatalogue,
};
