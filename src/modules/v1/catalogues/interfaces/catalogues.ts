import { ObjectId } from "mongoose";
import { CATALOGUE_TYPE, STATUS } from "enums/enums";
export interface ICatalogueNameExists{
    catalogueId?:Object;
    name:LanguageType;
}
export interface ICatalogue{
    catalogueId:Object;
}
export interface ICatalogues{
    catalogueId?:Object;
    pageNo?:number;
    limit?:number;
    search?:string;
}

export interface ICreateCatalogue {
  adminId?:ObjectId;
  excludeAdminIds?:Object[];
  parentId?: ObjectId;
  enableRegion?: STATUS.DEFAULT | STATUS.ACTIVE | STATUS.IN_ACTIVATED;
  regionIds?: ObjectId;
  sku: string;
  name: LanguageType;
  description?: LanguageType;
  tags?: string[];
  searchTags?: string[];
  images: string[];
  icons: string[];
  rating?: number;
  catalogueType:
    | CATALOGUE_TYPE.CATEGORY
    | CATALOGUE_TYPE.SERVICE
    | CATALOGUE_TYPE.PRODUCT;
  status:STATUS.ACTIVE | STATUS.IN_ACTIVATED | STATUS.DELETED;
}

export interface IUpdateCatalogue {
  catalogueId:ObjectId
  adminId?:ObjectId;
  excludeAdminIds?:Object[];
  parentId?: ObjectId;
  enableRegion?: STATUS.DEFAULT | STATUS.ACTIVE | STATUS.IN_ACTIVATED;
  regionIds?: ObjectId;
  sku?: string;
  name?: LanguageType;
  description?: LanguageType;
  tags?: string[];
  searchTags?: string[];
  images?: string[];
  icons?: string[];
  rating?: number;
  catalogueType?:
    | CATALOGUE_TYPE.CATEGORY
    | CATALOGUE_TYPE.SERVICE
    | CATALOGUE_TYPE.PRODUCT;
  status?:STATUS.ACTIVE | STATUS.IN_ACTIVATED | STATUS.DELETED;
}
export interface IReadCatalogue<T> {
    checkCatalogueExists(params:any): Promise<any>;
    catalogues(params:any): Promise<any>;
    catalogueDetails(params:any): Promise<any>;
    productsWithVariants(regionIds:string[] | [],productIds:string[],variantIds:string[]): Promise<any>;
}
export interface IWriteCatalogue<T> {
    createCatalogue(params:any): Promise<any>;
    updateCatalogue(params:any): Promise<any>;
    deleteCatalogue(params:any): Promise<any>;
    changeStatusCatalogue(params:any): Promise<any>;
    
}

