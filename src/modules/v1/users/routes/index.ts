import { Router } from "express";
const router = Router();
import {
  singUp,
  login,
  logout,
  profile,
  updateProfile,
  changePassword,
  sendOtp,
  verifyOtp,
  sendVerifyEmail,
  verifyEmail,
  forgotPassword,
  resetPassword,
  uploadDoc,
  uploadProfileImage,
  getProfiles
} from "../controllers/users";
import {
  validateSignUp,
  validateLogin,
  validateLogout,
  validateProfile,
  validateUpdateProfile,
  validateChangePassword,
  validateSendOtp,
  validateVerifyOtp,
  validateSendVerifyEmail,
  validateVerifyEmail,
  validateForgotPassword,
  validateResetPassword,
  validateUploadDoc,
  validateUploadProfileImage,
  validateUsers,
} from "../validaters/users";
import { userAuthorization,adminAuthorization } from "../../common/services/authorization";
router.post("/",validateSignUp, singUp);
router.put("/login", validateLogin, login);
router.put("/logout", validateLogout, userAuthorization, logout);
router.get("/profile", validateProfile, userAuthorization, profile);
router.put("/profile", validateUpdateProfile, userAuthorization, updateProfile);
router.put(
  "/changePassword",
  validateChangePassword,
  userAuthorization,
  changePassword
);
router.put(
  "/changePhoneNo/sendOtp",
  validateSendOtp,
  userAuthorization,
  sendOtp
);
router.put(
  "/changePhoneNo/verifyOtp",
  validateVerifyOtp,
  verifyOtp
);
router.put(
  "/changeEmail/sendVerifyEmail",
  validateSendVerifyEmail,
  userAuthorization,
  sendVerifyEmail
);
router.put("/changeEmail/verifyEmail", validateVerifyEmail, verifyEmail);
router.put(
  "/forgotPassword",
  validateForgotPassword,
  userAuthorization,
  forgotPassword
);
router.put("/resetPassword", validateResetPassword, resetPassword);

/*
FILE UPLOAD API'S
*/
router.post("/upload/doc", validateUploadDoc, userAuthorization, uploadDoc);
router.post(
  "/upload/profile",
  validateUploadProfileImage,
  userAuthorization,
  uploadProfileImage
);
router.get(
  "/",
  validateUsers,
  adminAuthorization,
  getProfiles
);
export default router;
