import { jwtSign } from "../../../../lib/universal-function";
import {
  createSession
} from "../services/session"
/**
 *
 * @param {*(admin/user data)} params
 * @description for genrate auth token
 * @returns
 */
const genrateAuthToken = async (ModelName: ModelNames, params: TokenData) => {
  let session={
    ...(params?.adminId && {adminId:params?.adminId}),
    ...(params?.parentId && {parentId:params?.parentId}),
    ...(params?.userId && {userId:params?.userId}),
    ...(params?.deviceId && {deviceId:params?.deviceId}),
    ...(params?.deviceType && {deviceType:params?.deviceType}),
    ...(params?.deviceName && {deviceName:params?.deviceName}),
    ...(params?.ip && {ip:params?.ip}),
    ...(params?.adminType && {adminType:params?.adminType}),
    ...(params.hasOwnProperty("isPhoneNoVerified") && {isPhoneNoVerified:params?.isPhoneNoVerified}),
    ...(params.hasOwnProperty("isEmailVerified") && {isEmailVerified:params?.isEmailVerified}),
  }
  let accessToken = await jwtSign(session);
  await createSession(ModelName, { ...session, accessToken });
  return accessToken;
}
export {
    genrateAuthToken
};
