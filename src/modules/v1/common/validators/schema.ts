import joi from "joi";
import CONSTANT from "../../../../constant/constant";
import {DAYS} from "../../../../enums/enums"
const locationSchema = joi.object({
  type: joi.string().valid('Point').default('Point'),
  coordinates: joi.array().items(
    joi.number().min(-90).max(90), // Latitude should be between -90 and 90 degrees
    joi.number().min(-180).max(180) // Longitude should be between -180 and 180 degrees
  ).length(2).default([0, 0])
});
const translationSchemaFn = () => {
  let translationObj={}
   Object.values(CONSTANT.LANGUAGE_TYPE).map(( language) => {
    translationObj= {...translationObj,[language]: joi.string().required()};
    return true;
  });
  return translationObj
};
const shiftSchema = joi.object({
  day: joi.number().valid(...Object.values(DAYS)).required(),
  open: joi.number().required(),
  closed: joi.number().required(),
});
export {
  locationSchema,
  translationSchemaFn,
  shiftSchema
};
