// kafkaProducer.ts
import { Producer, Message as KafkaMessage } from 'kafkajs';
import logger from '../../../../utils/logger';
import KafkaConnection from '../../../../connection/kafka';

class KafkaProducer {
  private static kafkaProducerInstance: KafkaProducer;
  private kafkaProducer: Producer;

  // Additional properties
  private isProducing: boolean = false;
  private produceDelay: number = 5000;

  private constructor() {
    const kafkaConnection = KafkaConnection.getInstance();
    this.kafkaProducer = kafkaConnection.getClient().producer({
      // Custom producer configuration
      maxInFlightRequests: 1,
      idempotent: true,
      transactionalId: "uniqueProducerId",
    });
  }

  public static getInstance(): KafkaProducer {
    if (!this.kafkaProducerInstance) {
      this.kafkaProducerInstance = new KafkaProducer();
    }
    return this.kafkaProducerInstance;
  }

  public async produceMessage(topic: string, messages: KafkaMessage[]): Promise<any> {
    try {
      await this.kafkaProducer.connect();
      let data=await this.kafkaProducer.send({
        topic,
        acks:-1,
        messages: messages,
      });
      this.isProducing = true;
      return data;
    } catch (error: any) {
      logger.error(`Error producing message to topic ${topic}: ${error.message}`, error);
      throw error;
    } finally {
      await this.kafkaProducer.disconnect();
      this.scheduleNextProduce();
    }
  }
  public async produceMessageTransaction(topic: string, messages: KafkaMessage[]): Promise<any> {
    await this.kafkaProducer.connect();
    const transaction = await this.kafkaProducer.transaction();
    try {
       let data=await transaction.send({
        topic,
        acks:-1,
        messages: messages,
      });
      await transaction.commit()
      this.isProducing = true;
      return data;
    } catch (error: any) {
      logger.error(`Error producing message with in transaction to this topic ${topic}: ${error.message}`, error);
      throw error;
    } finally {
      await transaction.abort()
      await this.kafkaProducer.disconnect();
      this.scheduleNextProduce();
    }
  }
  public async produceMessageTransactionWithPartionsAndOffset(topic: string, messages: KafkaMessage[],partition:number,offset:string): Promise<any> {
    await this.kafkaProducer.connect();
    const transaction = await this.kafkaProducer.transaction();
    try {
      let data=await transaction.send({
        topic,
        acks:-1,
        messages: messages,
      });
      await transaction.sendOffsets({
        consumerGroupId:'gptest1',
        topics:[{
          topic:topic,
          partitions:[{partition:partition,offset:offset}]
        }],
      });
      await transaction.commit()
      this.isProducing = true;
      return data;
    } catch (error: any) {
      logger.error(`Error producing message with in transaction and partions to this topic ${topic}-${partition}-${offset}: ${error.message}`, error);
      throw error;
    } finally {
      await transaction.abort()
      await this.kafkaProducer.disconnect();
      this.scheduleNextProduce();
    }
  }
  private scheduleNextProduce(): void {
    if (this.isProducing) {
      setTimeout(() => {
        this.isProducing = false;
      }, this.produceDelay);
    }
  }
}

export default KafkaProducer;
