import { EventEmitter } from 'events';

export default class CustomEventEmitter {
  private static instance: CustomEventEmitter;
  private emitter: EventEmitter;

  private constructor() {
    this.emitter = new EventEmitter();
  }

  public static getInstance(): CustomEventEmitter {
    if (!CustomEventEmitter.instance) {
        CustomEventEmitter.instance = new CustomEventEmitter();
    }
    return CustomEventEmitter.instance;
  }

  public emit(event: string, data?: any): boolean {
    return this.emitter.emit(event, data);
  }

  public on(event: string, listener: (...args: any[]) => void): this {
    this.emitter.on(event, listener);
    return this;
  }
}
