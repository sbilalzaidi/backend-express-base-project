import { Redis } from 'ioredis';
import logger from '../../../../utils/logger';
import connectRedisDB from "../../../../connection/redis";
const redisInstance = connectRedisDB.getInstance();
const redisClient = redisInstance.getClient("RedisConcurrencyManager");
/**
 * async function example() {
  const lock = new OptimisticLockDao("the_locak", 10, 10);

  try {
    const lockAcquired = await lock.acquireLockWithRetry();

    if (lockAcquired) {
      // Perform operations on the resource
      await lock.updateResource({ example: 'data' });
      console.log('Lock acquired and resource updated successfully.');
    } else {
      console.log('Failed to acquire lock within the specified timeout.');
    }
  } finally {
    // Release the lock when done
    await lock.releaseLock();
  }
  }
 */
  export default class OptimisticLockDao {
    private redis: Redis;
    private resourceKeys: string[];
    private timeout: number;
    private retryTime: number;
  
    constructor(resourceKeys: string[] = ["the_lock"], timeout: number = 10000, retryTime: number = 500) {
      this.redis = redisClient;
      this.resourceKeys = resourceKeys;
      this.timeout = timeout;
      this.retryTime = retryTime;
    }
  
    async acquireLock(resourceKey: string): Promise<boolean> {
      const lockKey = this.getLockKey(resourceKey);
      const clientId = this.generateClientId();
      const timestamp = Date.now();
  
      // Try to acquire the lock
      const isLockAcquired = await this.redis.set(lockKey, `${clientId}:${timestamp}`, 'EX', 10, 'NX');
  
      return Boolean(isLockAcquired);
    }
  
    async acquireLockWithRetry(resourceKey: string): Promise<boolean> {
      const startTime = Date.now();
      let lockAcquired = false;
  
      while (!lockAcquired && Date.now() - startTime < this.timeout) {
        lockAcquired = await this.acquireLock(resourceKey);
        if (!lockAcquired) {
          await new Promise((resolve) => setTimeout(resolve, this.retryTime));
        }
      }
  
      return lockAcquired;
    }
  
    async releaseLock(resourceKey: string): Promise<void> {
      const lockKey = this.getLockKey(resourceKey);
      await this.redis.del(lockKey);
    }
  
    async updateResource(resourceKey: string, data: any): Promise<void> {
      // Get the current version of the resource
      const currentVersion = await this.getCurrentVersion(resourceKey);
  
      // Update the resource
      const updatedData = {
        ...data,
        version: currentVersion + 1,
      };
  
      // Save the updated resource with the new version
      await this.redis.set(resourceKey, JSON.stringify(updatedData));
    }
  
    async performLockedUpdateOperationOnMultipleResources(dataMap: Record<string, any>): Promise<void> {
      const lockAcquiredMap: Record<string, boolean> = {};
  
      try {
        // Acquire locks for all specified resource keys
        for (const resourceKey of this.resourceKeys) {
          const lockAcquired = await this.acquireLockWithRetry(resourceKey);
          lockAcquiredMap[resourceKey] = lockAcquired;
  
          if (!lockAcquired) {
            // Release all acquired locks in case of failure
            await Promise.all(
              this.resourceKeys.map((key) => this.releaseLock(key))
            );
            throw new Error(`Failed to acquire lock for resource ${resourceKey}`);
          }
        }
  
        // Start a Redis transaction
        await this.redis.multi();
  
        try {
          // Perform the update operation on each resource key within the transaction
          for (const resourceKey of this.resourceKeys) {
            const data = dataMap[resourceKey];
            if (data !== undefined) {
              await this.updateResource(resourceKey, data);
            }
          }
  
          await this.redis.exec(); // Execute the transaction
        } catch (error) {
          // Discard the transaction in case of an error
          await this.redis.discard();
          throw error;
        }
      } finally {
        // Release all acquired locks
        for (const resourceKey of this.resourceKeys) {
          await this.releaseLock(resourceKey);
        }
      }
    }
  
    private async getCurrentVersion(resourceKey: string): Promise<number> {
      const currentData = await this.redis.get(resourceKey);
  
      // If the resource doesn't exist, assume version 0
      if (!currentData) {
        return 0;
      }
  
      const parsedData = JSON.parse(currentData);
      return parsedData.version || 0;
    }
  
    private getLockKey(resourceKey: string): string {
      return `${resourceKey}:lock`;
    }
  
    private generateClientId(): string {
      // Generate a unique client ID (you may use a more sophisticated method)
      return Math.random().toString(36).substring(2);
    }
  }
  
  