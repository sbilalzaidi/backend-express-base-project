import { Consumer, EachMessagePayload } from 'kafkajs';
import logger from '../../../../utils/logger';
import KafkaConnection from '../../../../connection/kafka';

class KafkaConsumer {
  private static kafkaConsumerInstance: KafkaConsumer;
  private kafkaConsumer: Consumer;
  private isConsuming: boolean = false;
  private consumeDelay: number = 5000;

  private constructor() {
    this.kafkaConsumer = KafkaConnection.getInstance().getClient().consumer({
      groupId: 'gptest1',
      minBytes: 5,
	    maxBytes: 1e6,
	    // wait for at most 3 seconds before receiving new data
	    maxWaitTimeInMs: 3000,
      // Other custom consumer configurations
    });
  }

  public static getInstance(): KafkaConsumer {
    if (!this.kafkaConsumerInstance) {
      this.kafkaConsumerInstance = new KafkaConsumer();
    }
    return this.kafkaConsumerInstance;
  }

  public async consumeMessages(topics: string[]): Promise<void> {
    try {
      if (!this.isConsuming) {
        await this.kafkaConsumer.connect();

        await this.kafkaConsumer.subscribe({
          topics,
          fromBeginning: true,
        });

        this.isConsuming = true;

        await this.kafkaConsumer.run({
          autoCommit: false,
         // partitionsConsumedConcurrently: 2,
          eachMessage: async (kafkaEventData: EachMessagePayload): Promise<void> => {
            await this.handleMessage(kafkaEventData);
          }
        });
      }
    } catch (error: any) {
      logger.error(`Consumer error: ${error.message}`, error);
      // Handle the error, possibly retry or log accordingly
    } finally {
      // Set isConsuming to false to allow the next consumption cycle
      this.isConsuming = false;
    }
  }

  private async handleMessage(kafkaEventData: EachMessagePayload): Promise<EachMessagePayload> {
    let { topic, partition, message } = kafkaEventData;
    console.log({
      value: message.value?.toString(),
      offset: message.offset,
      partition,
      topic,
    });

    // Add your message handling logic here
    // Simulate processing time
    await new Promise(resolve => setTimeout(resolve, 2000));
    // Explicit acknowledgment (commit offset) after processing the message
    await this.kafkaConsumer.commitOffsets([{ topic: topic, partition: partition, offset: message.offset + 1 }]);
    return kafkaEventData
  }
}

export default KafkaConsumer;
