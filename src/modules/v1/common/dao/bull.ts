import { Worker, Queue, WorkerOptions } from "bullmq";
import { Job } from "bullmq";
import logger from "../../../../utils/logger";
import connectRedisDB from "../../../../connection/redis";
const redisInstance = connectRedisDB.getInstance();
const redisClient = redisInstance.getClient("BullMQ");
class BullMQ {
  private static instance: BullMQ;
  private queue: Queue;
  private worker?: Worker;
  private constructor() {
    // Initialize BullMQ queue
    this.queue = new Queue("eventqueue", { connection: redisClient });
    this.setupWorker();
  }

  public static getInstance(): BullMQ {
    if (!BullMQ.instance) {
      BullMQ.instance = new BullMQ();
    }
    return BullMQ.instance;
  }
  /**
   *
   * eg..await myQueue.add('test',{ foo: 'bar' },
   * { removeOnComplete: true, removeOnFail: true ,delay: 10000 });
   * delay: Delay job by x ms before processing
   * attempts: Number of times to retry a failed job
   * backoff: Backoff strategy on job failure
   * lifo: Use LIFO order instead of FIFO
   * priority: Numeric priority value. Higher is processed sooner
   * repeat: Repeat job on a cron schedule { cron: '0 0 * * *' } // Daily at midnight
   * removeOnComplete/removeOnFail to true, in this case, all
   * jobs will be removed automatically as soon as they are finalized:
   */
  public async addJob(
    jobType: string,
    jobData: any,
    options: any
  ): Promise<Job> {
    try {
      return await this.queue.add(jobType, jobData, options);
    } catch (error) {
      throw error;
    }
  }
  /**
   * @param jobData [{jobType,jobData,options}]
   */
  public async addBulkJob(jobData: any): Promise<Job[]> {
    try {
      return await this.queue.addBulk(jobData);
    } catch (error) {
      throw error;
    }
  }
  public async emptyQueue(): Promise<void> {
    try {
      logger.info("Empty queue.");
      await this.queue.drain();
    } catch (error) {
      logger.warn("Error in empty in queue", JSON.stringify(error));
      throw error;
    }
  }
  /**
   * Removes jobs in a specific state,
   * but keeps jobs within a certain grace period.
   */

  public async cleanQueue(): Promise<string[]> {
    try {
      logger.info("Clean queue.");
      return await this.queue.clean(
        60000, // 1 minute
        1000, // max number of jobs to clean
        "paused"
      );
    } catch (error) {
      logger.warn("Error in clean in queue", JSON.stringify(error));
      throw error;
    }
  }
  /**
   * Completely obliterates a queue and all of its contents.
   */
  public async eventQueueQueue(): Promise<void> {
    try {
      logger.info("Obliterate queue.");
      await this.queue.obliterate();
    } catch (error) {
      logger.warn("Error in obliterate in queue", JSON.stringify(error));
      throw error;
    }
  }
  public setupWorker(concurrency: number = 1): void {
    const workerOptions: WorkerOptions = {
      concurrency,
      limiter: {
        max: 10,
        duration: 1000 // Per second
      },
      connection: redisClient,
    };

    this.worker = new Worker(
      "eventqueue",
      async job => {
        await this.handleMessage(job)
      },
      workerOptions
    );

    this.worker.on("completed", (job: any) => {
      logger.info(`Job ${job.id} has completed successfully.`);
    });

    this.worker.on("failed", (job: any, error: Error) => {
      logger.error(`Job ${job?.id} has failed:`, error.message);
    });

    logger.info(`BullMQ worker with concurrency ${concurrency} is set up.`);
  }
  private async handleMessage(job:any): Promise<any> {
    const {id,name,token,data}=job
    try {
      logger.info(`Processing job ${id}-${name} with data:`+JSON.stringify(data));
      // Perform job processing logic here
      // ...

      // Example: Simulate asynchronous work
      //await new Promise((resolve) => setTimeout(resolve, 1000));

      logger.info(`Job  ${id}-${name} completed.`);
    } catch (error) {
      logger.error(`Error processing job  ${id}-${name}-${token}:`, error);
      throw error;
    }
  }
  public async handleGracefulTermination(): Promise<void> {
    try {
      // Stop the worker
      if (this.worker) {
        await this.worker.close();
        logger.info('BullMQ worker stopped.');
      }

      // Drain the queue
      await this.queue.drain();
      logger.info('BullMQ queue drained.');

      // Additional cleanup logic can be added here if needed

      logger.info('Graceful termination of BullMQ completed.');
    } catch (error) {
      logger.error('Error during graceful termination of BullMQ:', error);
      throw error;
    }
  }
}
export default BullMQ