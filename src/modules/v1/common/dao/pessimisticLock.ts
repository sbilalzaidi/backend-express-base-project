import { Redis } from 'ioredis';
import logger from '../../../../utils/logger';
import connectRedisDB from "../../../../connection/redis";
const redisInstance = connectRedisDB.getInstance();
const redisClient = redisInstance.getClient("RedisConcurrencyManager");
/**
 * // Example usage
const lockManager = new PessimisticLockDao();

async function genericUpdateOperation(resourceId: string, newData: string): Promise<void> {
  // Simulate some generic resource update logic
  console.log(`Updating resource ${resourceId} with data: ${newData}`);
}

async function performGenericLockedOperation(resourceId: string, newData: string): Promise<void> {
  await lockManager.performLockedOperation(resourceId, async () => {
    await genericUpdateOperation(resourceId, newData);
  });
}

const resourceId = 'example_resource_id';
const newData = 'updated_data';

performGenericLockedOperation(resourceId, newData)
  .then(() => console.log('Operation successful'))
  .catch((error) => console.error(`Error performing operation: ${error.message}`));

 */
export default class PessimisticLockDao {
  private redis: Redis;

  constructor() {
    this.redis = redisClient
  }

  async acquireLock(resourceId: string, timeout: number = 5000): Promise<string | null> {
    const lockKey = `lock:${resourceId}`;
    const identifier = Math.random().toString(36).substr(2, 8); // Unique identifier for this lock

    const result = await this.redis.set(lockKey, identifier, 'PX', timeout, 'NX');

    if (result === 'OK') {
      return identifier;
    } else {
      return null;
    }
  }

  async releaseLock(resourceId: string, identifier: string): Promise<boolean> {
    const lockKey = `lock:${resourceId}`;
    const script = `
      if redis.call("get", KEYS[1]) == ARGV[1] then
        return redis.call("del", KEYS[1])
      else
        return 0
      end
    `;

    const result = await this.redis.eval(script, 1, lockKey, identifier);

    return result === 1;
  }

  async performLockedOperation<T>(
    resourceId: string,
    operation: () => Promise<T>,
    timeout: number = 5000
  ): Promise<T> {
    const identifier = await this.acquireLock(resourceId, timeout);

    if (!identifier) {
      throw new Error(`Failed to acquire lock for resource ${resourceId}`);
    }

    try {
      const result = await operation();
      return result;
    } finally {
      await this.releaseLock(resourceId, identifier);
    }
  }
  async performLockedOperationOnMultipleKeys<T>(
    resourceIds: string,
    operation: () => Promise<T>,
    timeout: number = 5000
  ): Promise<T> {
    const lockKeysMap: Record<string,string> = {};
    try {
      // Acquire locks for all specified hash keys
      for (const resourceId of resourceIds) {
        const identifier = await this.acquireLock(resourceId, timeout);

        if (!identifier) {
          // Release all acquired locks in case of failure
          await Promise.all((Object.keys(lockKeysMap)).map((key) => this.releaseLock(resourceId, lockKeysMap[resourceId])));
          throw new Error(`Failed to acquire lock for resource ${resourceId}`);
        }
        lockKeysMap[resourceId] = identifier;
      }

      // Start a Redis transaction
      await this.redis.multi();

      try {
        // Perform the operation on each hash key within the transaction
        const result = await operation();
        await this.redis.exec(); // Execute the transaction

        return result;
      } catch (error) {
        // Discard the transaction in case of an error
        await this.redis.discard();
        throw error;
      }
    } finally {
      // Release all acquired locks
      for (let resourceId of resourceIds) {
        await this.releaseLock(resourceId,lockKeysMap[resourceId]);
      }
    }
  }
}

