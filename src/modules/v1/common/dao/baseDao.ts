"use strict";

import { QueryOptions, ClientSession } from "mongoose";
import * as models from "../../../../models";
import { isEmpty } from "../../../../lib/universal-function";
export class BaseDao {
  async save(model: ModelNames, data: any, session?: ClientSession) {
    try {
      const ModelName: any = models[model];
      const document = new ModelName(data);

      // If session is provided, use it to perform the save operation within the session
      if (session) {
        await document.save({ session });
      } else {
        // If no session is provided, save the document without a session
        await document.save();
      }
      return document;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async find(
    model: ModelNames,
    query: any,
    projection: any,
    options: QueryOptions,
    sort?: any,
    paginate?: any,
    populateQuery?: any,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      let queryBuilder = ModelName.find(query, projection, options);

      if (session) {
        // If session is provided, use it to start the transaction
        queryBuilder = queryBuilder.session(session);
      }

      if (!isEmpty(sort) && !isEmpty(paginate) && isEmpty(populateQuery)) {
        // sorting with pagination
        return await queryBuilder
          .skip((paginate.pageNo - 1) * paginate.limit)
          .limit(paginate.limit)
          .sort(sort)
          .exec();
      } else if (
        isEmpty(sort) &&
        !isEmpty(paginate) &&
        isEmpty(populateQuery)
      ) {
        // pagination
        return await queryBuilder
          .skip((paginate.pageNo - 1) * paginate.limit)
          .limit(paginate.limit)
          .exec();
      } else if (
        isEmpty(sort) &&
        isEmpty(paginate) &&
        !isEmpty(populateQuery)
      ) {
        // populate
        return await queryBuilder.populate(populateQuery).exec();
      } else if (
        !isEmpty(sort) &&
        !isEmpty(paginate) &&
        !isEmpty(populateQuery)
      ) {
        // pagination with populate
        return await queryBuilder
          .skip((paginate.pageNo - 1) * paginate.limit)
          .limit(paginate.limit)
          .populate(populateQuery)
          .sort(sort)
          .exec();
      } else if (
        !isEmpty(sort) &&
        !isEmpty(paginate) &&
        !isEmpty(populateQuery)
      ) {
        // pagination with populate
        return await queryBuilder
          .skip((paginate.pageNo - 1) * paginate.limit)
          .limit(paginate.limit)
          .sort(sort)
          .populate(populateQuery)
          .exec();
      } else {
        // Default case without sorting, pagination, or population
        return await queryBuilder.exec();
      }
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async distinct(
    model: ModelNames,
    path: string,
    query: any,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      let distinctQuery = ModelName.distinct(path, query);

      if (session) {
        // If session is provided, use it to start the transaction
        distinctQuery = distinctQuery.session(session);
      }

      return await distinctQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findOne(
    model: ModelNames,
    query: any,
    projection: any,
    options: any,
    populateQuery: any,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      let findOneQuery = ModelName.findOne(query, projection, options);

      if (!isEmpty(populateQuery)) {
        // If populateQuery is provided, add populate step to the query
        findOneQuery = findOneQuery.populate(populateQuery);
      }

      if (session) {
        // If session is provided, use it to start the transaction
        findOneQuery = findOneQuery.session(session);
      }

      return await findOneQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findOneAndUpdate(
    model: ModelNames,
    query: any,
    update: any,
    options: QueryOptions,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      let findOneAndUpdateQuery = ModelName.findOneAndUpdate(
        query,
        update,
        options
      );

      if (session) {
        // If session is provided, use it to start the transaction
        findOneAndUpdateQuery = findOneAndUpdateQuery.session(session);
      }

      return await findOneAndUpdateQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findAndRemove(
    model: ModelNames,
    query: any,
    update: any,
    options: QueryOptions,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      let findAndRemoveQuery = ModelName.findOneAndRemove(
        query,
        update,
        options
      );

      if (session) {
        // If session is provided, use it to start the transaction
        findAndRemoveQuery = findAndRemoveQuery.session(session);
      }

      return await findAndRemoveQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async update(
    model: ModelNames,
    query: any,
    update: any,
    options: QueryOptions,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      let updateQuery = ModelName.update(query, update, options);

      if (session) {
        // If session is provided, use it to start the transaction
        updateQuery = updateQuery.session(session);
      }

      return await updateQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateOne(
    model: ModelNames,
    query: any,
    update: any,
    options: QueryOptions,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      let updateQuery = ModelName.updateOne(query, update, options);

      if (session) {
        // If session is provided, use it to start the transaction
        updateQuery = updateQuery.session(session);
      }

      return await updateQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async updateMany(
    model: ModelNames,
    query: any,
    update: any,
    options: QueryOptions,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      let updateQuery = ModelName.updateMany(query, update, options);

      if (session) {
        // If session is provided, use it to start the transaction
        updateQuery = updateQuery.session(session);
      }

      return await updateQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async delete(model: ModelNames, query: any, session?: ClientSession) {
    try {
      const ModelName: any = models[model];
      let removeQuery = ModelName.deleteOne(query);

      if (session) {
        // If session is provided, use it to start the transaction
        removeQuery = removeQuery.session(session);
      }

      return await removeQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async deleteMany(model: ModelNames, query: any, session?: ClientSession) {
    try {
      const ModelName: any = models[model];
      let deleteManyQuery = ModelName.deleteMany(query);

      if (session) {
        // If session is provided, use it to start the transaction
        deleteManyQuery = deleteManyQuery.session(session);
      }

      return await deleteManyQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async count(model: ModelNames, query: any, session?: ClientSession) {
    try {
      const ModelName: any = models[model];
      let countQuery = ModelName.countDocuments(query);

      if (session) {
        // If session is provided, use it to start the transaction
        countQuery = countQuery.session(session);
      }

      return await countQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async countDocuments(model: ModelNames, query: any, session?: ClientSession) {
    try {
      const ModelName: any = models[model];
      let countQuery = ModelName.countDocuments(query);

      if (session) {
        // If session is provided, use it to start the transaction
        countQuery = countQuery.session(session);
      }

      return await countQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async aggregate(
    model: ModelNames,
    aggPipe: any,
    options: QueryOptions,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      let aggregation = ModelName.aggregate(aggPipe);

      if (options) {
        aggregation = aggregation.options(options);
      }

      if (session) {
        // If session is provided, use it to start the transaction
        aggregation = aggregation.session(session);
      }

      return await aggregation.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async insert(
    model: ModelNames,
    data: any,
    options: QueryOptions,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      const obj = new ModelName(data);

      if (session) {
        // If session is provided, use it to start the transaction
        await obj.save({ session });
      } else {
        await obj.save();
      }

      return obj;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async insertMany(
    model: ModelNames,
    data: any,
    options: QueryOptions,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      let insertManyQuery = ModelName.insertMany(data, options);

      if (session) {
        // If session is provided, use it to start the transaction
        insertManyQuery = insertManyQuery.session(session);
      }

      return await insertManyQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async aggregateDataWithPopulate(
    model: ModelNames,
    group: any,
    populateOptions: any,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      const aggregate = await ModelName.aggregate(group);

      if (session) {
        // If session is provided, use it to start the transaction
        aggregate.session(session);
      }

      const populate = await ModelName.populate(aggregate, populateOptions);
      return populate;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async bulkFindAndUpdate(
    bulk: any,
    query: any,
    update: any,
    options: QueryOptions,
    session?: ClientSession
  ) {
    try {
      let bulkUpdateQuery = bulk.find(query).upsert().update(update, options);

      if (session) {
        // If session is provided, use it to start the transaction
        bulkUpdateQuery = bulkUpdateQuery.session(session);
      }

      return await bulkUpdateQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async bulkFindAndUpdateOne(
    bulk: any,
    query: any,
    update: any,
    options: QueryOptions,
    session?: ClientSession
  ) {
    try {
      let bulkUpdateOneQuery = bulk
        .find(query)
        .upsert()
        .updateOne(update, options);

      if (session) {
        // If session is provided, use it to start the transaction
        bulkUpdateOneQuery = bulkUpdateOneQuery.session(session);
      }

      return await bulkUpdateOneQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async findByIdAndUpdate(
    model: ModelNames,
    query: any,
    update: any,
    options: QueryOptions,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      let findByIdAndUpdateQuery = ModelName.findByIdAndUpdate(
        query,
        update,
        options
      );

      if (session) {
        // If session is provided, use it to start the transaction
        findByIdAndUpdateQuery = findByIdAndUpdateQuery.session(session);
      }

      return await findByIdAndUpdateQuery.exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async populate(
    model: ModelNames,
    data: any,
    populateQuery: any,
    session?: ClientSession
  ) {
    try {
      const ModelName: any = models[model];
      let populateQueryResult = ModelName.populate(data, populateQuery);

      if (session) {
        // If session is provided, use it to start the transaction
        populateQueryResult = populateQueryResult.session(session);
      }

      return await populateQueryResult.execPopulate();
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async _addSkipLimit(limit: number, pageNo: number) {
    if (limit) {
      limit = Math.abs(limit);
      // If limit exceeds max limit
      if (limit > 100) {
        limit = 100;
      }
    } else {
      limit = 10;
    }
    if (pageNo && pageNo !== 0) {
      pageNo = Math.abs(pageNo);
    } else {
      pageNo = 1;
    }
    let skip = limit * (pageNo - 1);
    return [{ $skip: skip }, { $limit: limit + 1 }];
  }
  _queryBuilder(
    pipeline?: Array<Object>,
    skip?: number,
    limit?: number,
    pageNo?: number
  ): any[] {
    const query = pipeline || [];

    query.push({
      $facet: {
        data: [{ $skip: skip }, { $limit: limit }],
        metadata: [{ $count: "total" }, { $addFields: { page: pageNo } }],
      },
    });
    return query;
  }
  async _paginate(
    model: ModelNames,
    pipeline?: Array<Object>,
    limit?: number,
    pageNo?: number
  ) {
    try {
      const ModelName = models[model];

      if (limit) {
        limit = Math.abs(limit);

        // If limit exceeds max limit
        if (limit > 300) {
          limit = 300;
        }
      } else {
        limit = 10;
      }
      if (pageNo && pageNo !== 0) {
        pageNo = Math.abs(pageNo);
      } else {
        pageNo = 1;
      }
      const skip: number = limit * (pageNo - 1);
      const result = await ModelName.aggregate(
        this._queryBuilder(pipeline, skip, limit, pageNo)
      ).exec();

      let next_hit = 0;
      let total_page =
        result[0]["data"].length > 0
          ? Math.ceil(result[0].metadata[0].total / limit)
          : 0;
      if (result[0]["data"].length > limit) {
        result[0]["data"].pop();
      }

      if (total_page > pageNo) {
        next_hit = pageNo + 1;
      }

      return {
        total:
          result[0]["metadata"] && result[0]["metadata"][0]
            ? result[0]["metadata"][0]["total"]
            : 0,
        pageNo:
          result[0]["metadata"] && result[0]["metadata"][0]
            ? result[0]["metadata"][0]["page"]
            : pageNo,
        totalPage: total_page,
        nextHit: next_hit,
        limit: limit,
        data: result[0]["data"],
        filterCount: result[0]["data"].length,
      };
    } catch (error) {
      throw new Error(JSON.stringify(error));
    }
  }
  async aggregateStream(model: ModelNames, aggPipe: any) {
    try {
      const ModelName: any = models[model];
      return await ModelName.aggregate(aggPipe)
        .cursor({ batchSize: 100 })
        .exec();
    } catch (error) {
      return Promise.reject(error);
    }
  }
  async findStream(
    model: ModelNames,
    query: any,
    projection: any,
    options: QueryOptions,
    sort: any
  ) {
    try {
      const ModelName: any = models[model];
      return await ModelName.find(query, projection, options)
        .sort(sort)
        .stream();
    } catch (error) {
      return Promise.reject(error);
    }
  }
  async getColl(model: ModelNames) {
    try {
      const ModelName: any = models[model];
      const changeStream = await ModelName.watch({
        fullDocument: "updateLookup",
      });
      changeStream.on("change", (error: any, data: any) => {
        console.log(data);
      });
      return changeStream;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}

export const baseDao = new BaseDao();
