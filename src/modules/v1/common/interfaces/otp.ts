export interface ISendOtp{
    phoneNo:string;
    countryCode:string;
    adminId?:string;
    userId?:string;
    prefix?:string;
    message?:string;
    otpCode?:string;
}
export interface IVerifyOtp{
    phoneNo:string;
    deviceId?:string;
    prefix?:string;
    otpCode?:string;
}