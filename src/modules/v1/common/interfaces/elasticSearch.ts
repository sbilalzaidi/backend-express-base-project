export interface ElasticSearchMapping{
    indexName:string;
    mappingkey:string;
}
export interface ElasticSearchDeleteIndex{
    indexName:string;
}
export interface ElasticSearchCreateIndex{
    indexName:string;
}
	