export default {
    "catalogues":{
        "properties": {
          "id": { "type": "keyword" },
          "adminId": { "type": "keyword" },
          "excludeAdminIds": { "type": "keyword" },
          "parentId": { "type": "keyword" },
          "enableRegion": { "type": "keyword" },
          "regionIds": { "type": "keyword" },
          "sku": { "type": "keyword" },
          "name": { "type": "text" },
          "nameTranslations": { "type": "object" },
          "description": { "type": "text" },
          "descriptionTranslations": { "type": "object" },
          "tags": { "type": "keyword" },
          "searchTags": { "type": "keyword" },
          "images": { "type": "keyword" },
          "icons": { "type": "keyword" },
          "rating": { "type": "float" },
          "enableCalender": { "type": "keyword" },
          "calender":{
            "type": "nested",
            "properties": {
              "day": { "type": "float" },
              "open": { "type": "float" },
              "closed": { "type": "float" }
            }
          },
          "enableLocation": { "type": "keyword" },
          "location": {
            "type": "geo_point",
            "ignore_malformed": true
          },
          "variants": {
            "type": "nested",
            "properties": {
              "brand":{ "type": "keyword" },
              "adminId": { "type": "keyword" },
              "regionIds": { "type": "keyword" },
              "label": { "type": "text" },
              "labelTranslations": { "type": "object" },
              "mrp": { "type": "float" },
              "sp": { "type": "float" },
              "price": { "type": "float" },
              "discount": { "type": "float" },
              "discountType": { "type": "keyword" },
              "sku": { "type": "keyword" },
              "rating": { "type": "float" },
              "enableLocation": { "type": "keyword" },
              "location": {
                "type": "geo_point",
                "ignore_malformed": true
              },
              "on": { "type": "integer" },
              "ranking": { "type": "integer" },
              "enableCalender": { "type": "keyword" },
              "calender":{
                "type": "nested",
                "properties": {
                  "day": { "type": "float" },
                  "open": { "type": "float" },
                  "closed": { "type": "float" }
                }
              },
              "enableInventory": { "type": "keyword" },
              "quantity": { "type": "integer" },
              "enableGoogleMerchantCenter":{ "type": "keyword" },
              "enableFacebookMerchantCenter":{ "type": "keyword" },
              "status": { "type": "keyword" }
            }
          },
          "catalogueType": { "type": "keyword" },
          "status": { "type": "keyword" },
          "createdAt": { "type": "date" },
          "updatedAt": { "type": "date" }
        }
    }    
}