export default {
    DATA_NOT_FOUND: "Data not found",
    DATABASE_ERROR: "DB Error.",
    FORBIDDEN: "Forbidden",
    IMPLEMENTATION_ERROR: "Implementation error.",
    INTERNAL_SERVER_ERROR: "Internal server error.",
    SUCCESS: "Success.",
    UNAUTHORIZED: "Unauthorized.",
    INVALID_SECRET_KEY: "Invalid secret key.",
    SESSION_EXPIRE: "Session expired.",
    DEVICE_ID_REQUIRED:"DEVICE_ID_REQUIRED",
    FILE_SIZE_EXCEED:"FILE_SIZE_EXCEED",
    EMAIL_ALREDAY_EXIT: "EMAIL_ALREDAY_EXIT",
    PHONE_NUMBER_ALREADY_EXISTS: "PHONE_NUMBER_ALREADY_EXISTS",
    SUCCESSFULLY_REGISTERED: "SUCCESSFULLY_REGISTERED",
    SESSION_LIMIT_EXCEEDED: "SESSION_LIMIT_EXCEEDED",
    OTP_LIMIT_EXCEEDED: "OTP_LIMIT_EXCEEDED",
    EMAIL_VERIFICATION_LINK_LIMIT_EXCEEDED:
      "EMAIL_VERIFICATION_LINK_LIMIT_EXCEEDED",
    EXIPRE_OTP_CODE: "EXIPRE_OTP_CODE",
    INVALID_OTP: "INVALID_OTP",
    EXPIRE_EMAIL_VERIFICATION_LINK: "EXPIRE_EMAIL_VERIFICATION_LINK",
    USER_NOT_FOUND: "USER_NOT_FOUND",
    USER_IS_BLOCKED: "USER_IS_BLOCKED",
    LOGOUT_SUCCESSFULLY: "LOGOUT_SUCCESSFULLY",
    PROFILE_UPDATED: "PROFILE_UPDATED",
    PASSWORD_CHANGED_SUCCESSFULLY: "PASSWORD_CHANGED_SUCCESSFULLY",
    EXPIRE_RESET_PASSWORD_LINK: "EXPIRE_RESET_PASSWORD_LINK",
    SEND_OTP_SUCCESSFULLY: "SEND_OTP_SUCCESSFULLY",
    OTP_VERIFIED_SUCCESSFULLY: "OTP_VERIFIED_SUCCESSFULLY",
    SEND_EMAIL_VERIFICATION_LINK: "SEND_EMAIL_VERIFICATION_LINK",
    EMAIL_VERIFIED_SUCCESSFULLY: "EMAIL_VERIFIED_SUCCESSFULLY",
    MISSING_PASSWORD: "MISSING_PASSWORD",
    WRONG_PASSWORD: "WRONG_PASSWORD",
    SAME_AS_OLD_PASSWORD: "SAME_AS_OLD_PASSWORD",
    INVALID_ADMIN_TYPE_SELECT: "INVALID_ADMIN_TYPE_SELECT",
    INVALID_CAPTCHA:"INVALID_CAPTCHA",
    FORGOT_PASSWORD_SUCCESSFULLY: "FORGOT_PASSWORD_SUCCESSFULLY",
  
    ADMIN_USER_ADDED_SUCCESSFULLY: "ADMIN_USER_ADDED_SUCCESSFULLY",
    ADMIN_USER_UPDATED_SUCCESSFULLY: "ADMIN_USER_UPDATED_SUCCESSFULLY",
    COMPANY_ADDED_SUCCESSFULLY:"COMPANY_ADDED_SUCCESSFULLY",
    COMPANY_UPDATED_SUCCESSFULLY:"COMPANY_UPDATED_SUCCESSFULLY",
    COMPANY_USER_ADDED_SUCCESSFULLY:"COMPANY_USER_ADDED_SUCCESSFULLY",
    COMPANY_USER_UPDATED_SUCCESSFULLY:"COMPANY_USER_UPDATED_SUCCESSFULLY",
    SELECT_VALID_COMPANY:"SELECT_VALID_COMPANY",
    
    ROLE_ALREADY_EXISTS: "ROLE_ALREADY_EXISTS",
    ROLE_DOES_NOT_EXISTS:"ROLE_DOES_NOT_EXISTS",
    ROLE_CREATE_SUCCESSFULLY: "ROLE_CREATE_SUCCESSFULLY",
    ROLE_DELETED_SUCCESSFULLY:"ROLE_DELETED_SUCCESSFULLY",
    ROLE_UPDATED_SUCCESSFULLY: "ROLE_UPDATED_SUCCESSFULLY",
  
    

    ADD_MODULE_PERMISSIONS_CREATE_SUCCESSFULLY:
      "ADD_MODULE_PERMISSIONS_CREATE_SUCCESSFULLY",
    MODULE_PERMISSIONS_UPDATED_SUCCESSFULLY:
      "MODULE_PERMISSIONS_UPDATED_SUCCESSFULLY",
    MODULE_PERMISSIONS_ALREADY_EXISTS: "MODULE_PERMISSIONS_ALREADY_EXISTS",
  
    MODULE_CREATE_SUCCESSFULLY: "MODULE_CREATE_SUCCESSFULLY",
    MODULE_ALREADY_EXISTS: "MODULE_ALREADY_EXISTS",
    MODULE_UPDATED_SUCCESSFULLY: "MODULE_UPDATED_SUCCESSFULLY",
  
    CATALOGUE_ALREADY_EXISTS: "CATALOGUE_ALREADY_EXISTS",
    CATALOGUE_CREATE_SUCCESSFULLY: "CATALOGUE_CREATE_SUCCESSFULLY",
    CATALOGUE_UPDATED_SUCCESSFULLY: "CATALOGUE_UPDATED_SUCCESSFULLY",
  
    VARIANT_CREATE_SUCCESSFULLY: "VARIANT_CREATE_SUCCESSFULLY",
    VARIANT_UPDATED_SUCCESSFULLY: "VARIANT_UPDATED_SUCCESSFULLY",
  
    SETTING_UPDATE_SUCCESSFULLY: "SETTING_UPDATE_SUCCESSFULLY",
  
    PROMO_CODE_ALREADY_EXISTS: "PROMO_CODE_ALREADY_EXISTS",
    PROMO_CODE_CREATE_SUCCESSFULLY: "PROMO_CODE_CREATE_SUCCESSFULLY",
    PROMO_CODE_UPDATED_SUCCESSFULLY: "PROMO_CODE_UPDATED_SUCCESSFULLY",
  
    PAYMENT_CARD_ALREADY_EXISTS: "PAYMENT_CARD_ALREADY_EXISTS",
    PAYMENT_CARD_CREATE_SUCCESSFULLY: "PAYMENT_CARD_CREATE_SUCCESSFULLY",
    PAYMENT_CARD_UPDATED_SUCCESSFULLY: "PAYMENT_CARD_UPDATED_SUCCESSFULLY",
  
    ORDER_CREATE_SUCCESSFULLY: "ORDER_CREATE_SUCCESSFULLY",
    ORDER_UPDATED_SUCCESSFULLY: "ORDER_UPDATED_SUCCESSFULLY",
    PAYMENT_ISSUE_PLEASE_TRY_AGAIN:"PAYMENT_ISSUE_PLEASE_TRY_AGAIN",
    REMOVE_OUT_OF_STOCK_ITEMS:"REMOVE_OUT_OF_STOCK_ITEMS",
    
    INVALID_STORE_ID: "INVALID_STORE_ID",
    STROE_INFO_UPDATED_SUCCESSFULLY: "STROE_INFO_UPDATED_SUCCESSFULLY",
  
    PRODUCT_IS_OUT_OF_STOCK: "{{name.en}} is out of stock",
    ALL_PRODUCT_IS_OUT_OF_STOCK:"All products are out of stock",
    PRODUCT_HAS_LIMITED_QUANTITY: "${name.en} is only ${quantity} in stock",
  
    NOT_SERVE_THIS_DELIVERY_ADDRESS:
      "Monha is not available at ${address} at the moment. Please select a different location.",
    STORE_NOT_SERVE_THIS_DELIVERY_ADDRESS:
      "Monha store is not available at ${address} at the moment.Please select a different location/time.",
    ITEM_UNAVAILABLE: "Item unavailable ${quantity} item in your cart is not available in your new location and is removed",

    CAN_NOT_CANCEL_ORDER:"You can not cancel the order because order on the way.",
    ORDER_ALREADY_COMPLETED:"Order already delivered",
    ORDER_ALREADY_CANCELED:"Order already canceled.",
  };
  