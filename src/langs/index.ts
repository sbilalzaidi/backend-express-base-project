export async function loadLanguage(language: string) {
    const langModule = await import(`../langs/${language}`);
    return langModule.default;
}