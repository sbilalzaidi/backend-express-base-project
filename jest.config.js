module.exports = {
    clearMocks: true,
    collectCoverage: true,
    coverageDirectory: "coverage",
    moduleFileExtensions: ['js', 'jsx', 'ts', 'tsx','json', 'node', 'graphql'],
    roots: ['<rootDir>/src'],
    testEnvironment: 'node',
    transform: {
        '^.+\\.tsx?$': 'ts-jest',
        "^.+\\.graphql$": "jest-transform-graphql",
    },
};
